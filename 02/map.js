function init_map (max_zoom, abovepages, layoutpages) {

var cell_width = 320,
    cell_height = 209;

if (abovepages === undefined) { abovepages = [] }
 
var map = L.map('map', {
        maxZoom: max_zoom || 9,
        minZoom: 0,
        zoom: 0,
        crs: L.CRS.Simple,
        center: new L.LatLng(0,0)
});


var markers = [
  {
    "zoom": 6,
    "x": 2.273,
    "y": -16.203,
    "id": "t1"
  },
  {
    "zoom": 8,
    "x": 15.484,
    "y": -18.063,
    "id": "t2"
  },
  {
    "zoom": 6,
    "x": 17.172,
    "y": -9.938,
    "id": "t3"
  },
  {
    "zoom": 6,
    "x": 32.125,
    "y": -9.953,
    "id": "t5"
  },
  {
    "zoom": 6,
    "x": 45.031,
    "y": -16.594,
    "id": "t6"
  },
  {
    "zoom": 6,
    "x": 233.063,
    "y": -16.547,
    "id": "t7"
  },
  {
    "zoom": 6,
    "x": 13.234,
    "y": -16.406,
    "id": "t8"
  },
  {
    "zoom": 6,
    "x": 224.891,
    "y": -9.859,
    "id": "t9"
  },
  {
    "zoom": 6,
    "x": 227.391,
    "y": -10.109,
    "id": "t10"
  },
  {
    "zoom": 6,
    "x": 3.031,
    "y": -16.641,
    "id": "t13"
  },
  {
    "zoom": 6,
    "x": 13.172,
    "y": -16.563,
    "id": "t14"
  },
  {
    "zoom": 6,
    "x": 17.094,
    "y": -16.703,
    "id": "t15"
  },
  {
    "zoom": 7,
    "x": 17.008,
    "y": -12.203,
    "id": "t17"
  },
  {
    "zoom": 6,
    "x": 23.0,
    "y": -16.766,
    "id": "t18"
  },
  {
    "zoom": 6,
    "x": 27.0,
    "y": -16.734,
    "id": "t19"
  },
  {
    "zoom": 6,
    "x": 32.859,
    "y": -16.594,
    "id": "t22"
  },
  {
    "zoom": 6,
    "x": 36.906,
    "y": -10.219,
    "id": "t23"
  },
  {
    "zoom": 8,
    "x": 33.73,
    "y": -9.473,
    "id": "t25"
  },
  {
    "zoom": 6,
    "x": 37.078,
    "y": -16.609,
    "id": "t26"
  },
  {
    "zoom": 6,
    "x": 45.141,
    "y": -16.516,
    "id": "t27"
  },
  {
    "zoom": 6,
    "x": 44.953,
    "y": -9.875,
    "id": "t29"
  },
  {
    "zoom": 6,
    "x": 55.172,
    "y": -16.297,
    "id": "t30"
  },
  {
    "zoom": 6,
    "x": 64.984,
    "y": -16.438,
    "id": "t31"
  },
  {
    "zoom": 6,
    "x": 74.969,
    "y": -16.563,
    "id": "t32"
  },
  {
    "zoom": 6,
    "x": 85.016,
    "y": -16.156,
    "id": "t33"
  },
  {
    "zoom": 6,
    "x": 94.953,
    "y": -16.313,
    "id": "t34"
  },
  {
    "zoom": 6,
    "x": 94.984,
    "y": -9.797,
    "id": "t38"
  },
  {
    "zoom": 6,
    "x": 97.344,
    "y": -9.734,
    "id": "t39"
  },
  {
    "zoom": 6,
    "x": 105.094,
    "y": -16.313,
    "id": "t40"
  },
  {
    "zoom": 6,
    "x": 115.016,
    "y": -16.406,
    "id": "t41"
  },
  {
    "zoom": 6,
    "x": 117.109,
    "y": -16.594,
    "id": "t42"
  },
  {
    "zoom": 6,
    "x": 125.109,
    "y": -16.625,
    "id": "t43"
  },
  {
    "zoom": 6,
    "x": 124.313,
    "y": -9.969,
    "id": "t44"
  },
  {
    "zoom": 6,
    "x": 134.984,
    "y": -16.328,
    "id": "t45"
  },
  {
    "zoom": 6,
    "x": 142.672,
    "y": -16.391,
    "id": "t46"
  },
  {
    "zoom": 6,
    "x": 144.875,
    "y": -9.75,
    "id": "t49"
  },
  {
    "zoom": 6,
    "x": 155.0,
    "y": -16.109,
    "id": "t50"
  },
  {
    "zoom": 6,
    "x": 155.094,
    "y": -9.75,
    "id": "t51"
  },
  {
    "zoom": 6,
    "x": 164.922,
    "y": -16.328,
    "id": "t52"
  },
  {
    "zoom": 6,
    "x": 174.875,
    "y": -16.563,
    "id": "t53"
  },
  {
    "zoom": 6,
    "x": 185.234,
    "y": -16.5,
    "id": "t54"
  },
  {
    "zoom": 6,
    "x": 195.031,
    "y": -16.453,
    "id": "t55"
  },
  {
    "zoom": 6,
    "x": 195.063,
    "y": -9.859,
    "id": "t56"
  },
  {
    "zoom": 6,
    "x": 205.125,
    "y": -16.328,
    "id": "t57"
  },
  {
    "zoom": 6,
    "x": 205.0,
    "y": -9.828,
    "id": "t58"
  },
  {
    "zoom": 6,
    "x": 214.844,
    "y": -16.406,
    "id": "t59"
  },
  {
    "zoom": 6,
    "x": 225.047,
    "y": -16.625,
    "id": "t61"
  },
  {
    "zoom": 6,
    "x": 224.953,
    "y": -9.828,
    "id": "t62"
  }
];
// console.log("[map].markers", markers);
var allmarkers = [];
markers.forEach(function (m) {
    var marker = L.marker([m.y, m.x]).on("click", function (e) {
        //console.log("marker click", m.id, e, this)
        window.parent.postMessage({msg: "mapclick", id: m.id}, "*")
    });
    allmarkers.push(marker);
})
var markers_layer = L.layerGroup(allmarkers);

var request = new XMLHttpRequest();
request.open('GET', 'tiles.json', true);

var hash = new L.Hash(map);

function is_above_page (item) {
    var m = /^_DSC(\d+)\.png/.exec(item.name);
    if (m) {
        var pn = parseInt(m[1]),
            pni = abovepages.indexOf(pn);
        // console.log("AP", pn, pni);
        return (abovepages.indexOf(pn) != -1)
    }
}

request.onload = function() {
  if (request.status >= 200 && request.status < 400) {
    // Success!
    var data = JSON.parse(request.responseText);
    // console.log("data", data);
    var pelts = [];
    var above_spread = false;
    var x = 0,
        y = 0,
        items = data['@graph'];
    for (var i=0, l=items.length; i<l; i++) {
        var item = items[i],
            m = /^_DSC(\d+)\.png/.exec(item.name),
            itemname = m ? m[1] : '';

        if (layoutpages && layoutpages[itemname]) {
            x = layoutpages[itemname].x;
            y = layoutpages[itemname].y;
            // console.log("using layoutpages, placing", itemname, "at", x, y);
            pelts.push({x: x, y: y, item: items[i]});
            continue
        }
        if (is_above_page(items[i])) {
            // above_spread = true;
            y = 0;
            x -= 1;
        } else {
            y = 1;
        }
        // pelts.push({x: i, y: 0, item: items[i]});
        // console.log("placing", items[i].name, "at", x, y);
        pelts.push({x: x, y: y, item: items[i]});
        x += 1;
        // if (i == 1) break;
    }
    var vt = leafygal.layout(pelts, undefined, undefined, undefined, undefined, undefined, cell_width, cell_height),
        layer = leafygal.gridlayer(L, vt, {tileSize: L.point(cell_width, cell_height)});
    map.addLayer(layer);

    map.addLayer(markers_layer);
    L.control.layers(null, { "Show links": markers_layer }).addTo(map);


  } else {
    console.log("server ERROR");
  }
};
request.onerror = function() {
  console.log("connection ERROR");
};
request.send();


}
