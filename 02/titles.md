# Situationist Times #2

## Cover

[0036#t=00:00:12.436]
[st02#6/-16.203/2.273]

So the front side is the tri?, tre?.... this symbol... This symbol was used in Belgium... 

[0036#t=00:00:32.768]
[st02#8/-18.063/15.484]

... by the farmers. (here you see it). In the sort of opposition.

[0036#t=00:00:40.866]
[st02#6/-9.938/17.172]
Is is part of our *dérive* through Belgium. And it goes all through the entire issue of number 2, this *dérive*.

[0036#t=00:00:55.708]
And I had to typographically, cut (the paper) into 2/3rds of the images.

[0036#t=00:01:02.683]
[st02#6/-9.953/32.125]
And here you see, all the time you see this tri... tre...I don't remember what it was. It was a farmers's sign in Wallonie (Wallonia) in Belgium against the government. But I don't remember what it was standing for, probably issues about money and more possibilities.

[0036#t=00:01:24.581]
[st02#6/-16.594/45.031]
Here you see it again.   And I put it on the front page because this was supposed to become **the Belgian issue**. I don't remember why.

[0036#t=00:01:36.321]
[st02#6/-16.547/233.063]

The back side is this by **Hansen Jacobsen**, which was called *Faustroll smelling Christian blood*. A very beautiful, crazy and expressionist-like sculpture in bronze, which in my imagination was standing in Lund, but I'm not sure it's true.

[0036#t=00:01:59.667]
[st02#6/-16.406/13.234]
Then the inside of the cover is a collage by **Jacques Prévert**, the famous poet. He had an exhibition of collages, and I liked it very much. I liked Jacques Prévert as a poet very much and also liked him very much as a person. One anectode is that whenever Jorn and I came to see him, they were living at the back of Moulin Rouge...  And he also was a pataphysician. This is very much the aspects of Noël Arnaud and the pataphysicians, and that is also why the Faustroll...it was of course the Ubu Roi, Faustroll - the icons of the pataphysicians. So I put all this in, and also of course Noël Arnaud. Prévert was very unhappy as you hardly could see his beautiful collage, because it was black on blue, which is a bit awkward. 

[0036#t=00:03:06.180]
[st02#6/-9.859/224.891]
Here you see again this symbol which is on the front page. And the back page, I went on a *dérive*. I have to read because I can't read... Do I see anything? Oh, yeah!

[0036#t=00:03:25.654]
[st02#6/-10.109/227.391]
Here I put all the bookstores where it can be found, where it can be ordered. And I also put when it was made and by whom? Yes, it was made by me, September 1962. You can find it New York at Wittenborn. And I put the address: Madison Avenue. In Paris, Library Le Minotaure. In Brussels at Corman library. And in Amsterdam at Antiquariaat... I can't read it. Collaborators to this issue, they are all here. You have to tell me what this is. It is an atom bomb and then it says...

[0036#t=00:04:39.776]
*"Himmelen beskytter ikke planter, dyr, menneskebørn. Heaven does not protect plants, animals and human children."* 

[0036#t=00:04:52.193]
Okay! And an atom bomb. And here we have England and Fanchon Fröhlich, that's the whole list of collaborators. There's a hell of a lot of collaborators in this issue. I put it in a funny way, because it's like a script I am making... Francois Dufrene, Aline Gagnaire, there are a lot of French: Boris Vian, Jacques Prévert, Noël Arnaud, Aline Gagnaire. From Holland it is Peter Schat and Lodewijk de Boer and Jacqueline de Jong, hey, that's interesting, she's collaborating! Then we have Marcel and Gabriel Piqueray in Belgium, and Serge Vandercam who was a very big collaborator in this issue. Germany: Spur. Sweden:Ansgar Elde. I make it very international, of course. Danmark: Hansen-Jacobsen. England: Fanchon Fröhlich (and Gordon Fazakerley). America: Gail Singer. So I just put everyone in it.   

[0036#t=00:06:26.898]
[st02#6/-16.641/3.031]
Did I say enough about the cover?  
*Maybe you want to say something about the color?*  
The color, which I re-used in number six, it is in a way **Klein blue**. And on the Klein blue I put black and white. There are three colors. You see this is white, and then black.

[0036#t=00:07:01.047]
[st02#6/-16.563/13.172]
And of course Prévert was not happy that I put his collage on the blue because you can hardly see it.

[0036#t=00:07:10.288]
[st02#6/-16.703/17.094]

Now then we decided, and we were... Arnaud and I went to see **Serge Vandercam**, who was an artist, was also a photographer. And as he was Belgian he knew... Actually he had been partcipating in Cobra, I think more as a photographer, I don't remember. So I again went back to the origins of the avant-garde, let's say. And then we decided to make a *dérive* through Belgium with him. Noël was driving the car, Serge was taking the photos, and I was chatting... I don't know. And then we made this *dérive*, literally, through Wallonia and took photos of everything we encountered.

[0036#t=00:08:10.224]
We also went to see some friends in Verviers who made a very nice review. He made also *Phantomas*, here you have one issue. There's lots of *Phantomas* issues here if you want to have a look. That's very Belgian. There was at this moment or a little later an exhibition in Verviers. So we went so see André Blavier because of the exhibition in which I participated with two big paintings. Well, this is all this around, brother and sister magazines, and avant-garde. And André Blavier was at least a point to go to. And Polydore Bouffioux, he was a cyclist. So then we just made pictures. This is Polydore Bouffioux, and texts. This is my handwriting, and it explains our travel. And these are just completely arbitrary pictures that go through all of the magazine.

[0036#t=00:10:31.548]
[st02#7/-12.203/17.008]
And this is called the first illiterate magazine which situation-objects. And the editors are -- itineraire -- the travellers are, this is explaining the map: the road is Noël Arnaud, the river is me and the railway is Serge Vandercam. And then you have forests with trees. So that's the map from one part of our *dérive*. 

[0036#t=00:11:17.676]
[st02#6/-16.766/23.000]
And then we have here *The Shadow* of Hansen-Jacobsen, the same sculptor as on the backside of the cover. That's a very beautiful sculpture.

[0036#t=00:11:30.481]
[st02#6/-16.734/27.000]
Then this again of course comes up with the SPUR process. **Uwe Lausen** was the only one that got convicted. He went to prison. And this is an article by Debord and Vaneigem in favour of Uwe Lausen. Now I must say the funny thing about Uwe Lausen is that he was 20 or 21 years and a student of Prem. I think it was Prem, anyway, of one of the Spur artists. And he was a promising artist but completely on drugs. So that was not so promising.

[0036#t=00:12:23.196]
Now Debord and company, by that I mean Debord and Vaneigem and Kotanyi, as they threw out the Germans and the Scandinavians, they had to have... After all the Internationale Situationniste had to be international. They had to have someone in Germany. So they took Uwe Lausen as the new Bavarian in the IS. Like they did with J.V. Martin in Denmark, to have someone in Denmark. 

[0036#t=00:12:59.890]
At that moment he was completely on drugs so he was not very active. Later on, after his death, he died very early, he is now considered as a very famous artist. There's a book made by Selima Niggle on him, she wrote her thesis on Uwe Lausen. So he became a famous artist after his death, during his life he was not at all. In a way I put this in as a sort of, "here you see how they work in the IS."     

[0036#t=00:13:50.279]
[st02#6/-16.594/32.859]

And then we have a very famous song by **Boris Vian**, it is really ultra-famous (Le Prisonnier), of which I got the original copy. It was very nice, it was his brother who gave it to me.  

*Vian died three years earlier, in 1959.*  

It was his brother who gave it to me. Later on I got to know very well his wife who died only two years ago, I think.

[0036#t=00:14:35.080]
[st02#6/-10.219/36.906]
So you have it here as a manuscript, and here the song in itself. And I actually used the song very recently, in October, I sent it to America when -- what's his name? -- Mr. Trump became president. I sent it to a friend who published it. This goes on, the song is still very famous. 

[0036#t=00:15:11.190]
Then this is a cake. I put ink on it and printed the cake. This is a typical cake from Belgium, from that part of Belgium. 

[0036#t=00:15:24.758]
[st02#8/-9.473/33.730]
This is Blavier, who we talked about. He was famous in Belgium during that period. He was really one of the avant-gardists and a close friend of Arnaud. This actually is quite funny because she is the woman in Asger's atelier in Boulevard de la Gare, the concierge. I don't know why there's a photo of her or what they are saying, you have to read it. That's the *dérive*, it's just whatever and then we put funny texts with it.

[0036#t=00:16:20.372]
[st02#6/-16.609/37.078]
These are grafittis, concerning this... "liberté, egalité, parité"... this fight of the farmers against the government. 

[0036#t=00:16:50.924]
[st02#6/-16.516/45.141]

This is about **SPUR**, of course. The trial was still going on when I was making this. And here it says: "the original genius from Schwabing", the part of Munich where they were from. It is from *Die Zeit*, May 1962. "The Spur group is original at any price". And the trial about blasphemy is talked about here. It was not a trial about liberty and freedom in art. Then we have all sorts of texts from newspapers in Germany about the trial. Of course it got a lot of publicity. They called it a "modell-Prozess", an iconic process about an avant-gardist quartet. Here you see them at the trial itself: Kunzelmann, Sturm, Prem, Zimmer, and no Lausen. While Lausen is convicted. This is a sort of collage of newspaper clippings. Obviously I put this on some horrible paper as a collage and had it printed like that.

[0036#t=00:19:14.533]
Then we get the symbol again, and a phallus, and socks who got very wet -- I am drying them on my hand and making a text. It's all the time these funny images from the spots where we were, and texts.  

[0036#t=00:19:46.708]
[st02#6/-9.875/44.953]
And then we get of course the SPUR trial, for a change, the conviction, "Im Namen des Volkes", in the name of the people.  

*Were you in Munich at the time?* 

No, they just sent me all this. And here it says, "Spur should be more moving."

[0036#t=00:20:17.060]
[st02#6/-16.297/55.172]
And it goes on: trial, trial, trial, the judge. It's all from the original documents.The place where it took place, the Amstgerichts. 

[0036#t=00:20:46.938]
[st02#6/-16.438/64.984]
And there of course again a text on the trial.  

*But isn't this the Vera Brühne case?*  

Yes. Probably I got bored by all this publicity and was using other things and trials and put the Spur process together with other more important things like Eichmann. Vera Brühne I don't remember. No idea! ... That's Vera Brühne. Who the fuck was Vera Brühne? Does anyone know? We don't know. And here there's a diary of the entire process, with all the dates. I just put these actualities in there, I don't remember what it was. We can look it up. The elegant Vera Brühne has apparently done soemthing terrible. But I don't think it's a nazi process. But I am really not sure, perhaps I should read it. But we can't spend time on that.  
*I think there is a note on a following page explaining it.*

[0036#t=00:23:21.112]
[st02#6/-16.563/74.969]
Then **Gasché**, who was a close friend of the Spur people, made this essay about the Spur process, and probably he is comparing it with... Of course as my magazine is quite confusing, I get confused too.

Is this his essay in French translation? No, this is Noël Arnaud. Perhaps it's worth reading.
*And then it's your text, and I think that explains the collage of the news clippings.* 
More or less. All of this is about comparing processes: Eichmann, Spur, Vera Brühne, apparently, in three languages. This is my text. Interesting... Did I write this? It says it's my text.

[0036#t=00:25:05.493]
[st02#6/-16.156/85.016]
People are asking me what texts I ever wrote. That's why I'm mentioned as a collaborator to the magazine on the cover. Perhaps you can find out which texts are mine?  
*I will!*  
Because I don't remember what I wrote. Didn't know I was able to write. I should read it one day.

[0036#t=00:25:40.288]
[st02#6/-16.313/94.953]
This I remember, this is another story. This I am quite proud of. Should I read it loud? Because this is quite funny. I have to explain one thing which has nothing to do with the *dérive* here. There was one thing going on in Holland. Herring was caught and not conserved. The fresh herring was consumed without having been conserved. They just brought to the land and then you were eating Dutch herring like that. But apparently there was a parasite, and that parasite was eating your stomach. They found that out at this moment. Some people had herring and died of the herring parasite. From that moment on herring was conserved on the boats in salt. So this has a little bit to do with that but it goes further. I put it in three languages. What language do you prefer, English?'

[0036#t=00:27:22.440]

**ADDRESS TO U THANT, SECRETARY GENERAL OF THE UNITED NATIONS.
The supposedly "developed" world deplores the continued freedom of existence of the cannibals of New Guinea.**

(That of course was going on as an actuality, entre parenthese)

[0036#t=00:27:51.881]
"The over-developed and under-developed peoples of the world are equally anxious concerned about the problem of increasing overpopulation in the Netherlands.
We propose a solution: the establishment in Holland of a sufficiently large contingent of cannibal master-cooks who can play an effective part, both in restaurants and in the home, in reducing the surplus Dutch population to manageable proportions.
We wish to point out that the recent ban issued by the Dutch government on the sale and consumption of fresh herrings (groene haring) makes this proposed recourse to anthropophagous practices a project of the greatest urgency."

[0036#t=00:28:46.938]

So my suggestion is to give them all this green herring and we get rid of this Dutch overpopulation. And this is the proposal for U Thant of the UN. That's I think the only manifesto I ever did on my own and I did it anonymously. I didn't put my name on it. I did on the article on the pages before, but not here. 

There we go on. There you see the car we were driving, I think a Peugeot, a nice car.

[0036#t=00:29:27.303]
[st02#6/-9.797/94.984]
Then we have here Noël Arnaud, me, don't know, Blavier, don't know. And here we have a clam that is terribly exhausted but it is also a female sexual organ. There it goes on with the same thing.

[0036#t=00:29:50.015]
[st02#6/-9.734/97.344]
Then we have **Henri Salvador** but written in the way of his Carribean language, Henwi Salvado. Made by **François Dufrêne** who was a lettrist, but with whom I was still friendly. And it's a very nice twist, twist was the form of dancing in that period.

[0036#t=00:30:17.454]
[st02#6/-16.313/105.094]

This I considered it a mistake, because it had no sense, really, but I felt very sorry for the woman in question, **Gail Singer**. She was a colleague, a painter, no graphic artist at the Hayter ... at the **Atelier 17**. I liked that she was American. She was much older than we were, and she was insane, in an insane asylum. So I thought I should put some of her work in and she was very proud of that. But it has not any real... I mean, I can't explain any more about it. And of course she ended up very badly, closed in. But at least that got published. 

[0036#t=00:31:09.023]
[st02#6/-16.406/115.016]
Oh yeah, this gets more and more incredibly intellectual, scholarly, we could say. **Fanchon Frölich** was also an American artist, married to a physician who was quite well-known in England. And I asked the physician to make an article for the magazine. And I think he just copied one of his articles. And I wrote here, this is quite funny, that Fanchon Frohlich wrote this article in 1852. Well, that's about a hundred years before she was born. I just made a mistake. But in this case I think the lapsus is quite nice.

"She was studying metaphysica and became only later a painter."

Well, much later... and it never got published, so we have the "original reshaped" article by Mrs. Frohlich. I don't know if it's on purpose or not. Anyway, I had to put something of her in, as I got this article from her husband.

[0036#t=00:32:50.836]
[st02#6/-16.594/117.109]

And this article she wrote about him, I think. But I had to put something of her artwork into it. So I did it in a Whitehead, absurd way. I think it is nice, it is like a comic strip story where you don't understand at all what is going on. 

[0036#t=00:33:15.882]
[st02#6/-16.625/125.109]

And then we get again **Peter Schat** and one of his first compositions, with real explanations. This is real, not like the first page.

[0036#t=00:33:28.867]
[st02#6/-9.969/124.313]
The images are telling what the sounds are and whatever the music should be. And I put it again on a different paper and another form, which is quite different from the things that were being done at that moment. And I like it today too, because it's quite exceptional for a manuscript to have it done like that, even then in the early 1960s.

[0036#t=00:34:12.388]
[st02#6/-16.328/134.984]
"Pubis de mite" is just a poem by **Marcel and Gabriel Piqueray** who are pataphysical friends of Noël Arnaud. And I must say that is the problem with this issue, that Arnaud puts his friends in it.

[0036#t=00:34:34.644]
[st02#6/-16.391/142.672]

This is okay, but then you get this, which I couldn't stand, and I didn't want to put in. Because I thought this is so silly.  

[0036#t=00:34:42.202]
**cut**

[0037#t=00:00:00.000]
Gale Singer doesn't really fit into the magazine. This doesn't fit at all. Neither text, nor image has anything to do with the funny thing, like Fanchon Fröhlich with the date. But she happened to be one of his mistresses. That's how it went.

It also meant, gave me a reason, to finish later on the collaboration, not the friendship, at all, but the collaboration.

[0037#t=00:00:54.804]
[st02#6/-9.750/144.875]
Of course I also put my friends in, the few friends I had. I put **Lodewijk de Boer's** text in it, but I also associated the text with the images of the farmers in Belgium against the... So this is a small theatre play of Lodewijk de Boer.

[0037#t=00:01:24.148]
[st02#6/-16.109/155.000]
This is of course ha ha ha-funny. So you have to turn the magazine, and then you have the text of his play. And there is again a cake of which I made a copy. And the *dérive* goes on and on. 

[0037#t=00:01:44.345]
[st02#6/-9.750/155.094]
Here you ccan see how it goes on. Here we have who?  I don't remember. Who is this? It could be Fazakerley, but I don't know.

[0037#t=00:02:02.828]
[st02#6/-16.328/164.922]

And then I was invited by a friend of my parents and of me too, to a congress about spatial science in Europe. And this was at Eurospace in Paris. And I went there because I wanted to know about this. It is funny, because much later, in the mid-60s, I started making my *private life of cosmonaut* paintings. This was an issue because at that moment European space science wasn't very developed yet. And this congress was the second symposium of the Societé francaise d'austronautique and the British interplanetary society. So it is very early in outer space research in Europe. I went there together with him and I put the whole program into the magazine. I thought it was a good idea to have this as an actuality.

[0037#t=00:03:30.373]
[st02#6/-16.563/174.875]
And then we get to Bucaille again, because we had to have not only space but also topology. And then he made a topological analysis of...  
*Buffon's needle, it's a mathematical problem.*  
No, it's about the Polydor Bouffioux, the cyclist. He was very famous, of course this is a fake name, but this is an analysis of this cyclist. I can look up his name, that's easy. He was called Polydore something. And Bucaille made an analysis of speed and bicycling

[0037#t=00:04:48.608]
[st02#6/-16.500/185.234]
As you can see it's quite mathematical, very difficult to analyse. And there you have probablity, of course, 'Situation and probability', that's very topological.

*What about the figures, the illustrations?*
That's him. As an artist he always has his illustrations, which at that moment I thought exaggerated, but today I think it's marvellous.

[0037#t=00:05:20.420]
[st02#6/-16.453/195.031]
In each issue he did this, in a fantastic way. He really made a piece of art out of the mathematics.

And there we have our friend Polydor. And he's saying: "I am getting smaller and smaller." The statue of course is for the big wheel runner.

[0037#t=00:05:55.875]
[st02#6/-9.859/195.063]
And here it says: "The milk for us; the milk for you." It's again this fighting for the liberty of... It's enormous this article! I only realize now.

[0037#t=00:06:22.434]
[st02#6/-16.328/205.125]
And then there is Asterix. We are driving around and finding Asterix. At that moment you have to have Asterix in it. And then it gets more and more crazy.

[0037#t=00:06:41.707]
[st02#6/-9.828/205.000]

And there we have again a collage by Prévert, which is visible. As I said to him, "you can't say that everything is invisible'" 

[0037#t=00:06:51.865]
[st02#6/-16.406/214.844]

And there we have all these cakes, **Speculoos** it's called. And this is **Gordon Fazakerley**. He was thrown out together with me from the situationists and he stayed a friend. I asked him to make an article. Here it says:

"This declaration was made in August 1962 in Drakabygget, Orkeljunga, and in Stockholm, Sweden.
We consider it as a contribution of the Scandinavian Section to the Situationist Times and print it here as such."

Here already, I am against them.

[0037#t=00:07:32.844]

"The attack on the common market by Gordon Fazakerley is put here as an introduction to the Declaration without further connection to it."

I apparently keep a distance form the article. Allthough it is quite funny. That's what I think I am doing, no? Yes, otherwise I wouldn't have put it like that. It's an article he sent to me, you can see it is signed by Thorsen... I was already in a fight with Thorsen and Nash, so.... Staffan Larsson. Oh, I signed it too... Hey! It's the Stockholm conference, August 1962. I never went to a Stockholm conference in August 1962. Did you?

[0037#t=00:08:36.904]
[st02#6/-16.625/225.047]

And then, while we were driving we heard on the radio that Marilyn Monroe had died, so we made a collage: moon-related, moon, miss universe, and we called it potlatch, in hounour of Marilyn Monroe. 

And there Verlaine is kissing... Well, this is a quite naughty sort of...

[0037#t=00:09:16.129]
[st02#6/-9.828/224.953]
And there we say "Externisme, soulevement de la jeunesse," uplifting of the youth. 

[0037#t=00:09:37.865]
**cut**

[0039#t=00:00:19.24]
I got very angry with the printer because he made the cover too big. You could ask me...

[0039#t=00:00:32.600]
**cut**

[0039#t=00:00:33.735]
Why I made it that size. What do you think is the reason?  
*It's a standard format? To get as much as possible from the paper?*  
Yes, to get the cheapest way, taking into account the size of the sheets of paper. 

[0039#t=00:01:15.883]
And then suddenly I get this cover which is much bigger, and I don't remember the reason. This is cut in a wrong way. This is not equal. You know I am a maniac, so I thought it was very strange. But then probably he just did it and then I thought it's nice. And it protects better, in a way. I don't remember what Arnaud said about the fact that it had changed. He probably didn't mind at all. 

[0039#t=00:02:23.987]
But it is a strange fact, it is the only one that has another format. It is weird, but apparently it was acceptable. So this was done in September, and number one in May. Of course it was done in September, because it was during the summer we went to Belgium, and the whole issue was made in Belgium. In Paris I finished the lay-out and then went to Holland, to have it printed in Hengelo again. 

[0039#t=00:03:39.838]
And then because of this Aline Gagnaire, this rather ridiculous nepotism that went on, I thought - and I think it was also Asger saying something - what are you up to with all this? How will you be going on? Because this is not the way *The Situationist Times* was meant to be. Well, it is international and it has all sorts of artists or disciplines, different disciplines in art and science (outer space and things like that). It has all the aspects I want to, but some things are not ok. This was not okay, and that was Noël Arnaud. And then this is not ok, but that was me. 

[0039#t=00:04:33.634]
**cut**


[0033]: ../video/2017-12-14/MVI_0033.web.mp4 "video"
[0034]: ../video/2017-12-14/MVI_0034.web.mp4 "video"
[0035]: ../video/2017-12-14/MVI_0035.web.mp4 "video"
[0036]: ../video/2017-12-14/MVI_0036.web.mp4 "video"
[0037]: ../video/2017-12-14/MVI_0037.web.mp4 "video"
[0038]: ../video/2017-12-14/MVI_0038.web.mp4 "video"
[0039]: ../video/2017-12-14/MVI_0039.web.mp4 "video"
[0040]: ../video/2017-12-14/MVI_0040.web.mp4 "video"
[0041]: ../video/2017-12-14/MVI_0041.web.mp4 "video"


[st02]: ../02/scans.html "scans"
