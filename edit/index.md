Situationist Times
=====================

Updated 24 jan 2019

* [Situationist Times #1](../01/titles.md)
* [Situationist Times #2](../02/titles.md)
* [Situationist Times #3](../03/titles.md)
* [Situationist Times #4](../04/titles.md)
* [Situationist Times #5](../05/titles.md)
* [Situationist Times #6](../06/titles.md)

Videos
==============

* [1970 film](../1970.md)
* [1970 film](../1970_en.md) English Translation!


Instructions
==================

0. Click on a markdown file above to open it in an editor tab.
1. Click on a video link to open it in a separate frame.
2. Select the radio button to "focus" on the video frame.

## Saving

* Press CTRL-S to save

### Pasting fragment links

Clicking on the fragment (e.g. #t=00:00:00 ) above a frame will paste a link to that fragment in the editor. The control key (Ctrl-Down) will also paste a fragment link.


## Control Keys

Control keys are relvant to the "focused" frame. Click on the round radio button control to focus on a frame.

When focussed on a video, the following keys have an effect in the editor:

* Ctrl-Up to start/stop the video
* Ctrl-Down to paste a (fragment) link
* Ctrl-Left, Ctrl-Right to jump forward/back


## Markdown help

See <http://pandoc.org/MANUAL.html#pandocs-markdown>.