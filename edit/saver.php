<?php

// Only allow saving to existing files (because of realpath)
// that fall within this directory
$allowedpath = "/var/www/vhosts/vandal.ist/httpdocs/thesituationisttimes/";

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

function join_paths() {
    $paths = array();
    foreach (func_get_args() as $arg) {
        if ($arg !== '') { $paths[] = $arg; }
    }
    return preg_replace('#/+#','/',join('/', $paths));
}
function startsWith($haystack, $needle)
{
     $length = strlen($needle);
     return (substr($haystack, 0, $length) === $needle);
}

$docroot = $_SERVER['DOCUMENT_ROOT'];
$path = $_REQUEST['path'];
// $path = join_paths($docroot, $path);
$rpath = realpath($path);

if ($rpath && startsWith($rpath, $allowedpath)) {
    $text = @$_REQUEST['text'];
    if ($text) {
        $result = file_put_contents($rpath, $text);
        if ($result) {
            header('Content-Type: application/json');
            echo json_encode(array(message => "saved (".$result.")", error => 0));    
        } else {
            $err = error_get_last();
            header('Content-Type: application/json');
            echo json_encode(array(message => $err['message'], error => 1));    
        }        
    } else {
        header('Content-Type: application/json');
        echo json_encode(array(message => "no text", error => 1));    
    }   
} else {
    header('Content-Type: application/json');
    echo json_encode(array(message => "bad path ($rpath)", error => 1));    
}

?>