
###############################
# JAVASCRIPT
# cceditor/dist/cceditorapp.js: cceditor/src/*.js cceditor/src/editor.css
# 	cd cceditor && node_modules/.bin/webpack

ccplayer/dist/ccplayer.js: ccplayer/src/*.js
	cd ccplayer && node_modules/.bin/webpack

dist/leafygal.js: src/leafygal.js
	node_modules/.bin/webpack


###############################
# TITLES, MARKERS, MAP

%/titles.html: %/titles.md
	scripts/expand_reference_links.py --input $< \
	| \
	pandoc --from markdown \
		--to html \
		--standalone \
		-o $@

%/titles.srt.html: %/titles.md scripts/html2srt.py
	scripts/expand_reference_links.py --input $< \
	| \
	pandoc --from markdown \
		--to html \
		--standalone \
	| \
	scripts/html2srt.py --ids \
	| \
	scripts/symboliclinks.py \
	| \
	scripts/html5tidy \
		--stylesheet "../lib/srt.css" \
		--output $@ 

# initial version
%/markers.json: %/titles.srt.html
	cat $< | scripts/extractmapmarkers.py > $@

# FINAL version
# %/markers.json: %/edit.srt.html
# 	cat $< | scripts/extractmapmarkers.py > $@

%/map.js: %/markers.json templates/map.js
	scripts/jinjafy.py templates/map.js --data $< > $@


## RULE TO CUT THE VIDEO + finalize titles based on titles.srt.html
%/video.mp4 %/edit.srt.html: %/titles.srt.html
	scripts/edit.py --input $< \
		--output $*/edit.srt.html \
		--outputvideo ../$*/video.mp4 --performedit \
		--cwd edit

# For updating the titles *without* regenerating the video
# this rule is useful
# %/video.mp4 %/edit.srt.html: %/titles.srt.html
# 	scripts/edit.py --input $< \
# 		--output $*/edit.srt.html \
# 		--outputvideo ../$*/video.mp4 \
# 		--cwd edit
# 	touch $*/video.mp4


###############################
# for debugging variables
print-%:
	@echo '$*=$($*)'


1970_en.titles.html: 1970_en.md scripts/html2srt.py
	scripts/expand_reference_links.py --input $< \
	| \
	pandoc --from markdown \
		--to html \
		--standalone \
	| \
	scripts/html2srt.py --ids \
	| \
	scripts/symboliclinks.py \
	| \
	scripts/html5tidy \
		--stylesheet "../lib/srt.css" \
		--output $@ 

1970_en.srt: 1970_en.titles.html
	scripts/makesrt.py --input $< --output $@