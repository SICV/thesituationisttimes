function init_map (max_zoom, abovepages, layoutpages) {

var cell_width = 320,
    cell_height = 209;

if (abovepages === undefined) { abovepages = [] }
 
var map = L.map('map', {
        maxZoom: max_zoom || 9,
        minZoom: 0,
        zoom: 0,
        crs: L.CRS.Simple,
        center: new L.LatLng(0,0)
});


var markers = {{markers|json}};
// console.log("[map].markers", markers);
var allmarkers = [];
markers.forEach(function (m) {
    var marker = L.marker([m.y, m.x]).on("click", function (e) {
        //console.log("marker click", m.id, e, this)
        window.parent.postMessage({msg: "mapclick", id: m.id}, "*")
    });
    allmarkers.push(marker);
})
var markers_layer = L.layerGroup(allmarkers);

var request = new XMLHttpRequest();
request.open('GET', 'tiles.json', true);

var hash = new L.Hash(map);

function is_above_page (item) {
    var m = /^_DSC(\d+)\.png/.exec(item.name);
    if (m) {
        var pn = parseInt(m[1]),
            pni = abovepages.indexOf(pn);
        // console.log("AP", pn, pni);
        return (abovepages.indexOf(pn) != -1)
    }
}

request.onload = function() {
  if (request.status >= 200 && request.status < 400) {
    // Success!
    var data = JSON.parse(request.responseText);
    // console.log("data", data);
    var pelts = [];
    var above_spread = false;
    var x = 0,
        y = 0,
        items = data['@graph'];
    for (var i=0, l=items.length; i<l; i++) {
        var item = items[i],
            m = /^_DSC(\d+)\.png/.exec(item.name),
            itemname = m ? m[1] : '';

        if (layoutpages && layoutpages[itemname]) {
            x = layoutpages[itemname].x;
            y = layoutpages[itemname].y;
            // console.log("using layoutpages, placing", itemname, "at", x, y);
            pelts.push({x: x, y: y, item: items[i]});
            continue
        }
        if (is_above_page(items[i])) {
            // above_spread = true;
            y = 0;
            x -= 1;
        } else {
            y = 1;
        }
        // pelts.push({x: i, y: 0, item: items[i]});
        // console.log("placing", items[i].name, "at", x, y);
        pelts.push({x: x, y: y, item: items[i]});
        x += 1;
        // if (i == 1) break;
    }
    var vt = leafygal.layout(pelts, undefined, undefined, undefined, undefined, undefined, cell_width, cell_height),
        layer = leafygal.gridlayer(L, vt, {tileSize: L.point(cell_width, cell_height)});
    map.addLayer(layer);

    map.addLayer(markers_layer);
    L.control.layers(null, { "Show links": markers_layer }).addTo(map);


  } else {
    console.log("server ERROR");
  }
};
request.onerror = function() {
  console.log("connection ERROR");
};
request.send();


}
