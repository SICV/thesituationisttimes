var timecode = require("./timecode.js");

var cc_t_pat = /^t=((?:\d\d\:)?\d\d\:\d\d(?:[,.]\d{1,3})?)(?:,((?:\d\d\:)?\d\d\:\d\d(?:[,.]\d{1,3})?))?$/,
	cc_xywh_pat = /^xywh=((?:pixel\:)|(?:percent\:))?(\-?\d+(?:\.\d+)?),(\-?\d+(?:\.\d+)?),(\-?\d+(?:\.\d+)?),(\-?\d+(?:\.\d+)?)$/;

function cc_parse_fragment_component (c) {
	var m = cc_t_pat.exec(c);
	if (m !== null) {
		return {
			start : timecode.timecode_to_seconds(m[1]),
			end : ((m[2] !== undefined) ? timecode.timecode_to_seconds(m[2]) : undefined)
		}
	}
	m = cc_xywh_pat.exec(c);
	if (m !== null) {
		return {
			x : parseFloat(m[2]),
			y : parseFloat(m[3]),
			width : parseFloat(m[4]),
			height : parseFloat(m[5])
		}
	}
}

function cc_href (h) {
	var that = {},
		fpos = h.indexOf("#");
	that.base = h;
	that.frag;
	if (fpos !== -1) {
		that.base = h.substring(0, fpos);
		that.frag = h.substring(fpos+1);
		that.frag.split("&").forEach(function (nvp) {
			var pc = cc_parse_fragment_component(nvp);
			if (pc !== undefined) {
				for (var key in pc) {
					that[key] = pc[key];
				}
			}
		});
	}
	that.duration = function () {
		if (that.start && that.end) {
			return that.end.value - that.start.value;
		}
	}
	that.unparse = function () {
		// allow href item to be malleable and reconstituted as href via unparse
		// WARNING :: CURRENT IMPLEMENTATION IS MINIMAL AT BEST (with just a poc of xywh)
		var ret = that.base;
		if (that.x !== undefined) {
			ret += "#xywh="+that.x+","+that.y+","+that.width+","+that.height;
		}
		// todo: implement time / also some kind of normalisation of raw values !!
		return ret;
	}
	return that;
}

module.exports.parse_href = cc_href;