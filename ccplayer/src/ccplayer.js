var createIntervalTree = require("interval-tree-1d"),
	srt = require("./sloppysrt.js"),
	updateset = require('./updateset.js'),
	parse_href = require("./href.js").parse_href,
	CCFrameSimple = require("./ccframesimple.js"),
	$ = require("jquery"),
	smoothscroll = require('smoothscroll-polyfill');

// https://github.com/iamdustan/smoothscroll
// http://iamdustan.com/smoothscroll/
// console.log("smoothscroll", smoothscroll);
// console.log("updateset", updateset);
smoothscroll.polyfill();

function ccplayer (opts) {
	var that = {},
		tree_for_source = {},
		frame_for_title = {},
		titles_by_id = {},
		ttid = 0,
		byid = {},
		active = updateset(),
		active_tree,
		titles_iframe,
		ccvideo,
		ttid = 0;

	// return the first enclosing div.title of a given elt
	function enclosing_title (elt) {
		if (elt.nodeName == "DIV" && elt.classList.contains("title")) {
			return elt;
		}
		if (elt.parentNode) { return enclosing_title(elt.parentNode); }
	}

	function parse_leaflet_href (href) {
		return /(.+?)\#(\d+)\/(\-?\d+(?:\.\d+)?)\/(\-?\d+(?:\.\d+)?)/.exec(href);
	}

	function preserve_zoom_level_link_filter (oldhref, newhref) {
		// console.log("preserve_zoom_level_link_filter", oldhref, newhref);
		var oldp = parse_leaflet_href(oldhref),
			old_zoom_level = oldp ? oldp[2] : null,
			old_href = oldp ? oldp[1] : null,
			newp = parse_leaflet_href(newhref),
			new_zoom_level = newp ? newp[2] : null,
			new_href = newp ? newp[1] : null;

		// NB: Considers zoom_level 6 + 7 "neutral" ... ie when the newhref's zoom level is 6
		// hold whatever previous zoom level was present!

		// console.log("old_zoom_level", old_zoom_level, new_zoom_level);
		if (oldp && newp && old_href == new_href && new_zoom_level >= 6 && new_zoom_level <= 8) {
			var ret = newp[1]+"#"+oldp[2]+"/"+newp[3]+"/"+newp[4];
			// console.log("old zoom", oldp[2], "new zoom", newp[2]);
			// console.log("transformed leaflet link to preserve zoom", newhref, ret);
			return ret;
		}
		return newhref;
	}

	function activate_title (title, video_links) {
		// simulate clicking on the links in a title
		// if video_links isn't set, title=video links are skipped
		var links = title.querySelectorAll("a[title]");
		for (var i=0, l=links.length; i<l; i++) {
			var a = links[i],
				a_title = a.getAttribute("title"),
				href = a.getAttribute("href");
			if (a_title != "video" || video_links) {
				if (a_title == "scans") {
					href = preserve_zoom_level_link_filter(frame_for_title[a_title].src, href)
				}
				frame_for_title[a_title].src = href;
			}
		}
	}

	// process clicks on the titles
	function capture_clicks (doc) {
		doc.addEventListener("click", function (e) {
			// console.log("click", e.target);
			var title = enclosing_title (e.target);
			// console.log("enclosing_title", title);
			if (title) {
				e.preventDefault();
				activate_title(title, true);
				// var links = title.querySelectorAll("a[title]");
				// for (var i=0, l=links.length; i<l; i++) {
				// 	var a = links[i],
				// 		a_title = a.getAttribute("title"),
				// 		href = a.getAttribute("href");
				// 	frame_for_title[a_title].src = href;
				// }
			}
			return;
			// if (e.target.nodeName == "A") {
			// 	var title = e.target.getAttribute("title"),
			// 		href = e.target.getAttribute("href");
			// 	frame_for_title[title].src = href;
			// }
		})
	}

	function init_titles () {
		var tt = titles_iframe.contentDocument.querySelectorAll(".title");
		if (tt.length) {
			capture_clicks(titles_iframe.contentDocument);
			// srt.normalizeSRT(tt);
			var hrefs = [];
			for (var i=0, l=tt.length; i<l; i++) {
				hrefs.push(parse_href(tt[i].getAttribute("data-href")));
			}
			for (var i=0, l=tt.length; i<l; i++) {
				// console.log("href", hrefs[i]);
				var href = hrefs[i],
					src = href.base,
					start = href.start ? href.start : 0,
					end;
				if (tree_for_source[href.base] === undefined) {
					// console.log("init tree for src", href.base);
					tree_for_source[href.base] = createIntervalTree();
				}
				if (href.end) {
					end = href.end; 			
				} else if ((i+1 < l) && (hrefs[i+1].base == href.base) && hrefs[i+1].start) {
					end = Math.max(start, hrefs[i+1].start - 0.01);
				} else {
					end = start + 10;
				}
				var intv = [start, end];
				intv.title = tt[i];
				if (tt[i].id == undefined) {
					tt[i].id = "_tid"+(++ttid); // apply unique id, required by updateset !!!
				}
				titles_by_id[tt[i].id] = tt[i];
				//tt[i].style.display = "none";
				// console.log("inserting", intv);
				tree_for_source[href.base].insert(intv);
				// var w = (tt[i].end != undefined) ? tt[i].end - tt[i].start : 10;
				// 
				// byid[tt.id] = document.createElement("div");
				// var intv = [tt[i].start, (tt[i].end != undefined) ? tt[i].end : tt[i].start + 10];
				// intv.title = tt[i];
				// tree.insert(intv);
			}

		}
		// console.log("init_titles", ccvideo.base, active_tree);
		if (ccvideo && ccvideo.base && !active_tree) {
			active_tree = tree_for_source[ccvideo.base];
			// console.log("init_titles: active_tree", active_tree);
		}
	}

	function time_update (t) {
		// console.log("time_update", t);
		var items = [];
		if (!active_tree) { console.log("ccplayer.time_update: no active_tree"); return; }
		active_tree.queryPoint(t, function (x) {
			items.push(x.title);
		});
		active.update(items, {
			exit: function (item) {
				// console.log("exit", item);
				item.classList.remove("active");
			},
			enter: function (item) {
				// console.log("enter", item);
				item.classList.add("active");
				activate_title(item);

			},
			change: function () {
				scroll_to(titles_iframe, ".active");
			}
		});
	}


	function scroll_to (f, sel) {
		var elt = f.contentDocument.querySelector(sel);
		// console.log("scroll_to_active", elt);
		// console.log("contentDocument", f.contentDocument);
		// console.log("contentWIndow", f.contentWindow);
		elt.scrollIntoView({ behavior: 'smooth' });
	}

	/* INIT */

	window.addEventListener("message", function (e) {
		// console.log("window message", e.data);
		if (e.data.msg == "mapclick") {
			var title = titles_by_id[e.data.id];
			if (title) {
				var a = title.querySelector("a[title=video]");
				// console.log("mapclick, title", title, a);
				if (a) { a.click(); } else {
					console.log("mapclick, no a")
				}
			} else {
				console.log("mapclick, no title");
			}
		}
	})

	if (opts.frames) {
		for (var key in opts.frames) {
			var sel = opts.frames[key],
				elt = document.querySelector(sel),
				ccframe = new CCFrameSimple(elt);
			frame_for_title[key] = ccframe;
			if (key == "video") {
				ccvideo = ccframe;		
				ccframe.on("hashchange", function (h, t) {
					// console.log("hashchange", h, t);
					time_update(t);
				}).on("srcchange", function (src) {
					// console.log("srcchange", src);
					active_tree = tree_for_source[src];
					// console.log("active_tree", active_tree);
				});
				// console.log("attempting to set active_tree from ccvideo.src", ccframe.base);
				active_tree = tree_for_source[ccframe.base];
				// console.log("initial active_tree", active_tree);
			}
		}
	}
	if (opts.titles) {
		// console.log("attaching to opts.titles", opts.titles);
		titles_iframe = document.querySelector(opts.titles);
		// console.log("iframe", iframe);
		titles_iframe.addEventListener("load", function () {
			// console.log("[load]", this);
			init_titles();
		})
		init_titles();
	}

	return that;
}

module.exports.ccplayer = ccplayer;

