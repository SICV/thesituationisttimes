// based on examples shown at: http://youmightnotneedjquery.com/

function dispatch_custom_event (elt, event_name, data) {
    if (window.CustomEvent) {
      var event = new CustomEvent(event_name, {detail: data});
    } else {
      var event = document.createEvent('CustomEvent');
      event.initCustomEvent(event_name, true, true, data);
    }
    elt.dispatchEvent(event);
}

function dispatch_native_event (elt, event_type, event_name) {
    // For a full list of event types: https://developer.mozilla.org/en-US/docs/Web/API/document.createEvent
    event_type = event_type || 'MouseEvents'; // 'UIEvents' 'HTMLEvents'
    var event = document.createEvent(event_type);
    event.initEvent(event_name, true, false);
    el.dispatchEvent(event);
}

module.export.dispatch_custom_event = dispatch_custom_event;
module.export.dispatch_native_event = dispatch_native_event;
