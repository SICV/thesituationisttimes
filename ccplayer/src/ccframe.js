var timecode = require("./timecode.js"),
    EventEmitter = require("eventemitter3");

function strip_fragment (url) {
    var p = url.indexOf("#");
    if (p >= 0) { return url.substring(0, p); }
    return url;
}

function parse_fragment (url) {
    // console.log("parse_fragment", url);
    ret = {}
    if (url.indexOf("#") >= 0) {
        var p = url.split("#", 2);
        ret.base = p[0];
        ret.fragment = p[1];
    } else {
        ret.base = url;
        ret.fragment = '';
    }
    return ret;
}
function parseTime (h) {
    var m = h.match("t=([^&,]*)");
    // console.log("parseTime", h, m);
    if (m) { return timecode.timecode_to_seconds(m[1]); }
}

class CCFrame {
    constructor (elt) {
        // console.log("ccframe.init", elt);
        this.elt = elt;
        this.url = null;
        this.video = null;
        this.iframe = null;
        this.interval = null;
        this.pageNumber = null;
        var that = this;
        this.events = new EventEmitter();
        // $(elt).bind("control", function (e, cmd) {
        //     // console.log("ccframe got control", e, cmd);
        //     if (cmd == "toggle") {
        //         that.toggle();
        //     } else if (cmd == "jumpback") {
        //         that.jumpback();
        //     } else if (cmd == "jumpforward") {
        //         that.jumpforward();
        //     }
        // });
    }
    on (event, callback) {
        this.events.on(event, callback);
        return this;
    }
    _clear () {
        if (this.video !== null) {
            this.video.pause();
            this.elt.removeChild(this.video);
            this.video = null;
        }
        if (this.iframe !== null) {
            this.elt.removeChild(this.iframe);
            this.iframe = null;
        }
        if (this.interval !== null) {
            window.clearInterval(this.interval);
            this.interval = null;
        }
        this.pageNumber = null;
        this.elt.innerHTML = "";
    }
    set src (url) {
        // console.log("ccframe.src", url);
        var purl = parse_fragment(url),
            previous_url = this.url ? parse_fragment(this.url) : null;
        // console.log("purl", purl, "previous", previous_url)
        var viewers = {
            'video': {
                pattern: /\.(mp4|webm)$/,
                viewer: function (m) {
                    /* Check if match previous_url */
                    if (this.video && previous_url && previous_url.base == purl.base) {
                        var t = parseTime(purl.fragment);
                        if (t) {
                            this.video.currentTime = t;
                            this.video.play();
                            return;    
                        } else {
                            console.log("ccframe, bad timecode?", purl.fragment);
                        }
                    }
                    this._clear();
                    this.video = document.createElement("video");
                    this.video.setAttribute("autoplay", "");
                    this.video.setAttribute("controls", "");
                    this.elt.appendChild(this.video);
                    // console.log("adding load listeneer")
                    // this.video.addEventListener("loaded", this._load.bind(this)); 
                    this.video.addEventListener("timeupdate", this._timeupdate.bind(this)); 
                    this.video.src = url;
                }
            },
            'local_pdf': {
                pattern: function (url) {
                    // console.log("local_pdf?", url);
                    if (url.match(/\.pdf$/i)) {
                        if (url.match(/^https?:/) == null) {
                            return true;
                        }
                    }
                    return false;
                },
                viewer: function (m) {
                    this._clear();
                    this.iframe = document.createElement("iframe");
                    this.elt.appendChild(this.iframe);
                    this.iframe.src = '/lib/generic/web/viewer.html?file='+url;
                    this.iframe.addEventListener("load", this._load.bind(this));
                }
            }
        }
        var default_viewer = function () {
            if (previous_url && previous_url.base == purl.base) {
                // fragment change only
                // console.log("ccframe: update hash", purl.fragment);
                this.iframe.contentWindow.location.hash = purl.fragment;
                return;
            }
            this._clear();
            this.iframe = document.createElement("iframe");
            this.elt.appendChild(this.iframe);
            this.iframe.src = url;
            this.iframe.addEventListener("load", this._load.bind(this)); 
        }

        //this._clear();
        this.url = url;
        // $(this.elt).trigger("srcchange", this.url);
        if (!previous_url || previous_url.base != purl.base) {
            this.events.emit("srcchange", purl.base);
        }
        // var urlnf = strip_fragment(url);
        // console.log("cceditor", setsrc, url);
        for (var type in viewers) {
            var v = viewers[type],
                m = (v.pattern instanceof RegExp) ? v.pattern.exec(purl.base) : v.pattern.call(this, purl.base);
            if (m) {
                v.viewer.call(this, m);
                return this;
            }
        }
        default_viewer.call(this);
        return this;
    }
    get src () {
        return this.url;
    }
    _load (e) {
        // console.log("ccframe._load", this, e, this.url);
        if (this.iframe.contentDocument) {
            var video = this.iframe.contentDocument.querySelector("video");
            if (video) {
                // console.log("iframe has video", video);
            }
            var pageNumber = this.iframe.contentDocument.querySelector("input#pageNumber");
            this.pageNumberInput = pageNumber;
            this.pageNumber = pageNumber && pageNumber.value;
            if (pageNumber) {
                // console.log("iframe has pageNumber", pageNumber);
                this.interval = function () {
                    if (this.pageNumberInput.value != this.pageNumber) {
                        this.pageNumber = this.pageNumberInput.value;
                        // console.log("page changed", this.pageNumber);
                    }
                };
                window.setInterval(this.interval.bind(this), 1000);
                // The input/change eventListeners only seem to fire on user initiated changes (in Firefox at least)
                // pageNumber.addEventListener("input", function () {
                //     console.log("page changed", pageNumber.value);
                // }, false);
            }
        } else {
            // console.log("unable to access iframe.contentDocument");
        }

        if (this.iframe.contentWindow) {
            var that = this;
            // console.log("watching iframe.contentWindow")
            this.iframe.contentWindow.addEventListener("hashchange", function () {
                // console.log("iframe hashchange", that.iframe.contentWindow.location.hash);
                // $(that.elt).trigger("hashchange", that.iframe.contentWindow.location.hash);
                this.events.emit("hashchange", that.iframe.contentWindow.location.hash);
            })
        } else {
            // console.log("unable to access iframe.contentWindow")
        }
    }
    _timeupdate (e) {
        if (this.video) {
            var h = "#t="+timecode.seconds_to_timecode(this.video.currentTime);
            // $(this.elt).trigger("hashchange", h);
            this.events.emit("hashchange", h, this.video.currentTime);
        }
        // console.log("timeupdate", this.video.currentTime);
    }
    // pause () {
    //     if (this.video !== null) {
    //         this.video.pause();
    //     }
    // }
    // play () {
    //     if (this.video !== null) {
    //         this.video.play();
    //     }
    // }
    /* Functions for Ctrl Keys */
    toggle () {
        if (this.video !== null) {
            this.video.paused ? this.video.play() : this.video.pause();
        }
    }
    jumpforward () {
        if (this.video !== null) {
            this.video.currentTime += 5;
        }
    }
    jumpback () {
        if (this.video !== null) {
            this.video.currentTime -= 5;
        }
    }

    get currentTime () {
        if (this.video !== null) {
            return this.video.currentTime;
        }
    }
    set currentTime (t) {
        if (this.video !== null) {
            this.video.currentTime = t;
        }
    }
    
    get fragment () {
        if (this.pageNumber !== null) {
            return "#page="+this.pageNumber;
        } else if (this.video) {
            return "#t="+timecode.seconds_to_timecode(this.video.currentTime);
        } else {
            return this.iframe.contentWindow.location.hash;
        }
    }
    set fragment (val) {
        
    }
    
}

module.exports = CCFrame;
