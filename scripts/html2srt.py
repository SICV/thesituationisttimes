#!/usr/bin/env python3

import html5lib
import argparse, sys, re
from urllib.parse import urlparse
from xml.etree import ElementTree as ET

"""
Insert title divs for 1 at a time display, wrapping 1 or more p's with video links.
"""

def parentchilditerwithindex (elt):
    for parent in elt.iter():
        for i, child in enumerate(parent):
            yield parent, child, i

def replace_elt (t, elt, tag):
    for p, c, i in parentchilditerwithindex(t):
        if c == elt:
            # print ("replacing {0} with {1}".format(elt.tag, tag), file=sys.stderr)
            newelt = ET.SubElement(p, tag)
            p.remove(elt)
            p.insert(i, newelt)
            return newelt

def wrap_elts (t, elts, tag):
    newelt = None
    count = 0
    for p, c, i in parentchilditerwithindex(t):
        if c in elts:
            # print ("replacing {0} with {1}".format(elt.tag, tag), file=sys.stderr)
            if newelt == None:
                newelt = ET.SubElement(p, tag)
                p.insert(i, newelt)
            p.remove(c)
            newelt.append(c)
            count += 1
            if count == len(elts):
                if c.tail:
                    newelt.tail = c.tail
                    c.tail = None
                return newelt
    # return newelt

def header_level (n):
    m = re.search(r"^h(\d)$", n.tag, flags=re.I)
    if m:
        return int(m.group(1))

def gather_titles (t, use_title="video"):
    """ Group consecutive paragraph tags which contain a timecode link (filtered by title)
    Returns list of form:

    Emit flat document with gathered paragraphs...


    [{'content': [<Element 'p' at 0x7f739d637655>], 'href': None},
     {'content': [<Element 'p' at 0x7f739d637728>, <Element 'p' at 0x7f739d6377c8>],
      'href': '../video/2017-12-13/MVI_0029.web.mp4#t=00:06:32.25'},
     {'content': [<Element 'p' at 0x7f739d637818>],
      'href': '../video/2017-12-13/MVI_0029.web.mp4#t=00:06:44.451'},

    """
    # newdoc = html5lib.parse("", namespaceHTMLElements=False, treebuilder="etree")
    # body = newdoc.find(".//body")
    # title = SubElement(body, "div")

    # Walk through the DOM, flattening to headers + TITLE sequences
    # where a title sequence is a sequence of paragraphs the first of which contains a timed link
    # DOM -> [hx, p ...]
    # compress the p's into div.titles

    seq = None
    titles = []
    lastp = None
    for n in t.iter():
        if header_level(n) != None:
            titles.append(n)
        elif n.tag == "p":
            if seq == None:
                seq = {
                    'href': None,
                    'content': []
                }
                titles.append(seq)
            seq['content'].append(n)
            lastp = n
        elif n.tag == "a":
            href, title = n.attrib.get("href"), n.attrib.get("title")
            if href and title == use_title:
                if seq['href'] == None:
                    seq['href'] = href
                else:
                    # Start a new sequence with the current paragraph
                    seq['content'].remove(lastp)    
                    seq = {
                        'href': href,
                        'content': [lastp]
                    }
                    titles.append(seq)

    return titles

def output (titles, file, ids=False):
    print ("""<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>""", file=file)
    title_count = 0
    for seq in titles:
        if hasattr(seq, "tag"):
            print (ET.tostring(seq, method="html", encoding="unicode"), file=file)
        else:
            if ids:
                title_count += 1
                title_id = "t{0}".format(title_count)
                print ("""<div id="{0}" class="title" data-href="{1}">""".format(title_id, seq['href']))
            else:    
                print ("""<div class="title" data-href="{0}">""".format(seq['href']))
            for x in seq['content']:
                print (ET.tostring(x, method="html", encoding="unicode"), file=file)
            print ("""</div>""", file=file)
    print ("""</body></html>""", file=file)

ap = argparse.ArgumentParser("")
ap.add_argument("--input", type=argparse.FileType('r'), default=sys.stdin)
ap.add_argument("--output", type=argparse.FileType('w'), default=sys.stdout)
ap.add_argument("--ids", action="store_true", default=False, help="add numerical ids to title divs")
args = ap.parse_args()
t = html5lib.parse(args.input, namespaceHTMLElements=False, treebuilder="etree")
titles = gather_titles(t)
# from pprint import pprint
# pprint (titles)
output(titles, args.output, args.ids)
