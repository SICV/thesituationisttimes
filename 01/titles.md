# The Situationist Times #1 

## Origins: the i10

[0029#t=00:06:32.25] [st01#2/-23.0/156.3]
*Maybe you could just start by explaining why you wanted to make this magazine in the first place and how it came into being?*

[0029#t=00:06:39.596]
**cut**

[0029#t=00:06:48.204]
At the *Conseil Central* in Brussels in '61 I suggested that there should be an English-language version of the **International Situationiste**. I suggested that it should be made with Alexander Trocchi, but he was in prison because of drugs. So it couldn't be with him. Everyone agreed because there was a German magazine called **Spur**, and the French bulletin *International Situationiste*, and I hoped others would come with ideas.

[0029#t=00:07:38.671]
My concept of a magazine was actually this, which I admired very very much, still do actually. International Revue **i10**. Made by Arthur Lehning, the anarchist, in the 1920s, in '27.

[0029#t=00:08:05.799]
It was international and it was "multi-culture":  it had all the aspects of culture: Architecture, music, film, photo, art, literature. All the important people at that time participated in this. It was to me an exceptionally beautiful and well done as well in style as in content. And I wanted to do something alike. Philosopy, Bloch. Even the Urlauten of Schwitters, published for the first time. Many many different things were issued for the first time in the *i10*.

[0029#t=00:09:17.550]
Ehrenboerg on Cinema, a paper about Beethoven and jazz. It was amazing what they did.

[0029#t=00:09:24.237]
That was really my example of making a magazine. Imagine that in '27. And it only lasted til number 10... No I think it's 12 issues that came out.

[0029#t=00:09:48.808]
But still in these 12 issues, the whole culture of that time and that moment, internationally, was published and realized. And my dream was to make something alike.

[0029#t=00:10:15.358]
So I suggested this and I don't remember if anyone had seen it.

[0029#t=00:10:27.732]
And also, it had something, but that is not what Debord wanted. He wanted my English magazine to be a sort of translation from the French texts. I was still living in Amsterdam, working at the Stedelijk museum, and **Debord** sent me the concepts, made really pages that I should use in my (well *my*?) in the English magazine called **Situationist Times** (that was already my suggestion to call it that).

[0029#t=00:11:05.694]
And that was not at all my idea, to make a translation of the bulletin of the Situationists, but rather to make a completely new concept somethiing like this (*i10*) with also different people from all over the place. Whoever would participate was free to participate. So it lasted a year, it's incredible. It is absolutely... in typography and in its enormous broadness of culture which is in it makes it quite unique. You could say **Helhesten** had a little bit of that, but that was also very Scandinavian.

[0029#t=00:12:00.700]
But this, it was made in Holland, and it was completely international.

## Situationist Times

[0029#t=00:12:03.785]
[st01#7/-9.938/7.078]
So that was the idea of *The Situationist Times*. I think the title I found was obvious.

[0029#t=00:12:21.288]
So that was a very nice idea, but didn't work because no one was reacting nor particpating apart from these texts (from Debord) to be translated. And then nothing happened, I didn't do anything about it.

[0029#t=00:12:37.232]
Then in February 62, which I publish in the 1st issue, the SPUR group had a trial and was excluded from the Internationale Situationniste.

[0029#t=00:12:55.588]
Nash, Elde and I were solidary with them and declared our confimation of solidarity in the issue. To Debord, Vaneigem, and Kotanyi who had thrown them out. So the result was in a way, that we were also expelled.

[0029#t=00:13:18.848]
It has never really been for sure if it was the consequence of or if I just took the consequence and started *The Situationist Times*.

[0029#t=00:13:32.476]
Then I said OK, I want NOW to start it. And I started in my way very much influenced by the *i10*. So that means much more images, in a structural way, not just in a frivolous way as in the Gruppe SPUR (which I admired very much) but I wanted to make a combination of both magazines (i10 and Spur) but also of *all* magazines.

[0029#t=00:14:05.960]
And I thought, and I talked it over with Jorn of course, that it would be a very good idea to have someone who knew how to make a magazine. And who had made this fabulous magazine, *Le Surréalisme Révolutionnaire* in 1948. There never came anything but a No 1.

[0029#t=00:14:30.016]
That was directeur / redacteur **Noël Arnaud** who I knew through Jorn and who was living in Paris. I said to Jorn, I'm going to ask him if he would participate in my magazine and he said yes.

[0029#t=00:14:47.686]
Because then I thought not only did I do something quite nasty towards the rest of the Situationists, because Noël Arnaud is, or was, a Pataphysician. And of course that was not exactly done to have a pataphysician by the Situationists, they were competitive you could say. And I thought well the way he did with the International Surrealists I didn't agree with but the International part I agreed with very much.

[0029#t=00:15:30.099]
If you look at the participants: There are people like Dotremont, Bille who were in COBRA. You get a combination of all the former groups and international people together in this magazine from 48. 

[0029#t=00:16:03.457]
So when I start in 1962 the real Situationist Times, in my way, we get a sort of continuation of the international idea of the Avant-Garde. That's how I started it.

[0029#t=00:16:20.757]
[st01#6/-10.016/312.891]
And then there was the pamplet made by **Jorn** and Debord called **Mutant** which Jorn said: you have to use that and publish that, otherwise I will not help you with the magazine. And I said I have nothing against using that at all. I entirely agree with this (sort of) anti-United Nations text. It's not *anti* United Nations, but deforming the United Nations.

[0029#t=00:16:52.495]
[st01#6/-9.922/17.156]
And then I started with Noël Arnaud in our shelter. In the cave of the shelter. And used the countdown of a shelter, what does it say: "a Family-sized shelter". As we were all *very* much against nuclear shelters. I thought it was a very nice image, and I just put "Editors" and there they are in this "Galvanized Metal Air Intake Hood For Family Size Shelters".

[0029#t=00:17:32.819]
I don't what it exactly means. But that is the first thing.

[0029#t=00:17:38.635]
[st01#6/-9.891/25.078]
Then I also asked Arnaud to write an article in favor of the Gruppe SPUR. Who has at this moment has this issue because of the magazine (*SPUR*), and has this trial in Germany still based on the Nazi laws.

[0029#t=00:18:06.266]
The law or juruidical situation at that moment of Munich/Bavaria is still not so far from the Nuremberg process. It has a sort of remembrance of what has been happening for many years in Germany.

[0029#t=00:18:36.801]
And Noël Arnaud wrote a beautiful editorial article on the situation of SPUR and on the situation of Germany. And that's the first pages, and also seen from a French point of view.

[0029#t=00:18:55.085]
[st01#6/-10.031/44.969]
And then I just put in parts of the SPUR magazine to show that this ridiculous trial, is non-sensical. It's about blasphemy and pornography.

[0029#t=00:19:09.221]
[st01#6/-10.094/52.719]
If you really want to have a good look you can see that there is a phallus held by the woman and she is jerking him off, perhaps. It's not really very clear. And here is a text "okkult and OBSZOEN", and where there are some words like "fucking" and absolutely innocent erotic and pornographic words, but you can't call them pornographic. And (based) on that is this ridiculous trial.

[0029#t=00:19:54.427]
So I thought, and Arnaud agreed, that we just put the complete trial into print.

[0029#t=00:20:03.158]
And then the fantastic thing was, in this tiny little printers shop where I was doing it, which was making mostly prints for the Protestant church (gereformeerde protestant church, a very strong protestant church) They had papers, and I said may I use any paper I want, so I took out the things I liked. And I was of course also influenced by **Willem Sandberg**, my director at the Stedelijk Museum who was a well known typographer and who made beautiful books and pamplets and catalogs for the Stedelijk. And it was often on wrapping paper.

[0029#t=00:20:55.149]
[st01#6/-9.906/94.922]
So I took the wrapping paper of Sandberg. I took other colors than the usual white of the magazine.
I just put this complete trial of the SPUR *and* their answers in it. And images from their magazine.

[0029#t=00:21:16.344]

So the big thing is the Spur trial in the first issue.

[0029#t=00:21:23.842]
[st01#6/-9.859/117.063]
But then also, there is a pamphlet that the SPUR made against this trial which was on incest and blasphemy and pornography... and here you have the text that was mentioned that was itself considered incestuous.

[0029#t=00:21:55.150]
[st01#6/-9.859/134.938]
Then I had the idea to make modifications of images together with (Theo) **Wolvekamp** and later on with the Belgian **Serge Vandercam**. I just took images from a catalog in Verviers, with sculptures of Serge Vandercam and Wolvecamp was drawing on top of the sculptures, so he was making modifications. Wolvecamp is a Dutch artist who was in Cobra.

[0029#t=00:22:37.865]
[st01#6/-9.859/137.359]
This is (Rodolphe) **Gasché**. And that I don't really remember, he is a Professor of something important, in the US. And he was very close with SPUR. He made an article with a wort of poem with his drawing.

[0029#t=00:23:06.271]
[st01#6/-9.938/145.047]
So, what I always thought necessary (like in *i10*) that whatever someone sent which was within the concept and ideas should be printed as they sent it.

[0029#t=00:23:28.710]
And these are drawings Serge Vandercam made. These are drawing we made together, and by him.

[0029#t=00:23:29.267]
[st01#6/-9.859/155.000]
These are by him and me. Modified by two people.

[0029#t=00:23:39.421]
[st01#6/-9.969/172.578]
This is by Wolvecamp again

[0029#t=00:23:41.432]
[st01#6/-9.953/176.906]
And then I asked my friend **Peter Schat**, my age, then not yet very famous composer, but later one of the most famous Dutch composers, to participate. 

[0029#t=00:23:56.385]
[st01#6/-3.484/174.953]
And he sent me a "EnteLechie". And I just printed it, oh yes I wrote the text myself, the text he gave me but it's handwritten by me. And here's his composition, made already in a way like later on I printed his compositions.

[0029#t=00:24:20.981]
[st01#6/-10.063/185.156]
Then another aspect came into the magazine. And that was **Topology** which Jorn and I were very interested in. The idea of Noël Arnaud was that his friend **Max Bucaille**, who was also a pataphysician, could make texts. Because he was a topologist and actually a professor in Mathematics, and also a poet and a writer and a litle bit surrealist sort of designer. So I asked him to make a text for the first issue.

[0029#t=00:25:06.603]
And that is the first algebraic text in *The Situationist Times*. Talking about Speed and Fluctuation of Speed.... I don't remember exactly... (laughs) *Le problème du point*... So the "problem of the point". Which he explains very well. It's quite a big text. He made it especially for this. He actually continued with all the issues apart from #6.

[0029#t=00:25:50.595]
[st01#6/-11.438/195.953]
I don't know why I put this bicycle where you hardly see the bicycle, it looks like a man sitting on a chair, probably because it has something to do with speed.

[0029#t=00:26:02.923]
[st01#6/-[st01#6/-10.188/204.969]

And then we get to the dadaistic game of "Exquisite Corpse", so turning the pages. Wolvecamp and I made this together, we signed it. And I don't know why I put "poep", which means shit in Dutch.

[0029#t=00:26:37.467]
But they are funny texts that are engimatic to me, by me. So this *corpse* is by Wolvecamp and me. I don't think there's anyone else. Which is a game.

[0029#t=00:27:08.216]
[st01#6/-9.938/217.156]
Then we get to the **Critique of the political practice of détournement**. Which is the text I made against the treatment of the SPUR  by the Internationale Situationniste and the consequences of it, the way we, the others, were treated by the Situationists. 

[0029#t=00:27:38.046]
[st01#6/-9.922/223.094]
And there you get the pamphlet the way we got it. Zimmer or Sturm? Who was at the moment in Paris? Nash, De Jong, Elde. I think one of the SPUR people were there too, but I am not sure. The Conseil Centrale has eight members, but only these three were present. There  the Conseil Centrale was going to be at Blvd. St. Germain. But instead we got **Nicht Hinauslehnen**. *This* was on the table. And no one was there. So *these people* were considering the others so unimportant that they had to do it with this pamplet. So then we made the contra pamplet two or three days later.

[0029#t=00:29:45.207]
[st01#6/-9.984/242.922]
I started making my text... oh the same day, apparently. But it's only the three of us, so Kunzelmann was not present.

[0029#t=00:30:10.509]
[st01#6/-9.984/246.875]
And then I started making this text, which is absolutely unreadable, but luckily the Free Academy in Copenhagen made the transcription. And there I explain my feelings and my anger about what has been happening... It didn't come out of the blue. It all started with the participation of Kotanyi in the IS. You could also say Vaneigem, but he was less stubborn or heavy on it. All that was happening while there was this trial against Gruppe SPUR. So the non-solidarity of the situationists was my really big hang-up.

[0029#t=00:31:19.434]
[st01#6/-9.984/297.094]
And that's the whole text I made about it. And as it was my magazine, I could publish it and I did. And it ends; **I am proud that you call us gangsters. Nevertheless, you are wrong, we  are worse, we are situationists.**

[0029#t=00:32:09.125]
[st01#6/-9.875/302.813]
And then the last page I put ... What have I written there? I don't know... But I don't remember what is written there... I made an embryo out of Guy Debord. So that was sort of revenge, to put him in the situation of an embryo. ...

[0029#t=00:32:45.399]
[st01#6/-9.844/307.094]
And then I have, out of friendship, a copy of an image which given to me by **Pierre Wemaëre**, in part out of friendship with Jorn. .... I think it's the participants and how many copies were made... 

[0029#t=00:33:16.861]
[st01#9/-12.0000/308.0391]
*"The situationist times was printed in 1000 copies plus 90 exemplars with 1 litho, in Hengelo, Holland, May 1962. No. 1." And then it says "coll.", so maybe it's short for collage, Max Bucaille.*

[0029#t=00:33:42.067]
*Is there a collage by him here?*  

I think this must be Max Bucaille, but this I made.

[0029#t=00:33:49.287]
*Then it's says: France Noel Arnaud. Belgium Serge Vandercam, Holland Theo Wolvecamp, Peter Schat, Jacqueline de Jong, and this name I'm not able to read: Gebben?*

[0029#t=00:34:16.701]
Oh, Bert Gebben! It was through him I got the print shop. It was quite important to find a cheap one, and in Hengelo, 

[0030#t=0]
I mean I could stay with my parents, that made it very cheap to stay. And Bert Gebben was the designer of all the publicity of my father's factory. And he was a very nice guy, we became quite friendly. And he found this fantastic printer where I was completely free to work on it. And it took me about I think 3 weeks or months. My text was already made. (it *is* Max Bucaille). My text I had made in Paris. Because I was living in Paris when I made this.

[0030#t=00:00:41.274]
[st01#6/-10.328/313.219]
And then of course on the back is the Mutant manifesto, that Jorn insisted on.  
*So you prepared all the content in Paris, and then you went to Hengelo.* 

I'm not sure...

[0030#t=00:00:53.461]
This part for sure, these (the cadavre exquis) I made in Hengelo, all the Wolvecamp things were made in Hengelo. Serge Vandercam I probably had. And this was sent by post. And all this material of the SPUR process, I took with me and had it done there. 

[0030#t=00:01:38.121]
*Can you tell us about the printing process?* 

It's called **Rota-print**. It was with a roll. 

[0030#t=00:01:54.865]
It's sort of between duplicating and offset. I think it was photographed on a roll and it was hand-turned.

[0030#t=00:02:08.685]
Should I look it up? **cut**

[0030#t=00:02:14.433]
It was a very simple and very cheap way. 

[0030#t=00:02:19.355]
Ellef: Did you make photographic films?

[0030#t=00:02:22.357]
No. You couldn't retouch it.

[0030#t=00:02:26.109]
**cut**

[0029]: ../video/2017-12-13/MVI_0029.web.mp4 "video"
[0030]: ../video/2017-12-13/MVI_0030.web.mp4 "video"

[st01]: ../01/scans.html "scans"









































































































