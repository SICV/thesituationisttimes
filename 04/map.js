function init_map (max_zoom, abovepages, layoutpages) {

var cell_width = 320,
    cell_height = 209;

if (abovepages === undefined) { abovepages = [] }
 
var map = L.map('map', {
        maxZoom: max_zoom || 9,
        minZoom: 0,
        zoom: 0,
        crs: L.CRS.Simple,
        center: new L.LatLng(0,0)
});


var markers = [
  {
    "id": "t1",
    "zoom": 8,
    "x": 0.707,
    "y": -2.477
  },
  {
    "id": "t2",
    "zoom": 8,
    "x": 235.688,
    "y": -2.512
  },
  {
    "id": "t3",
    "zoom": 8,
    "x": 3.805,
    "y": -2.563
  },
  {
    "id": "t4",
    "zoom": 8,
    "x": 4.313,
    "y": -2.512
  },
  {
    "id": "t5",
    "zoom": 8,
    "x": 233.848,
    "y": -2.531
  },
  {
    "id": "t6",
    "zoom": 8,
    "x": 231.82,
    "y": -2.496
  },
  {
    "id": "t7",
    "zoom": 8,
    "x": 230.74,
    "y": -2.93
  },
  {
    "id": "t8",
    "zoom": 8,
    "x": 6.23,
    "y": -2.488
  },
  {
    "id": "t10",
    "zoom": 8,
    "x": 6.832,
    "y": -2.492
  },
  {
    "id": "t14",
    "zoom": 8,
    "x": 11.258,
    "y": -2.445
  },
  {
    "id": "t15",
    "zoom": 8,
    "x": 13.766,
    "y": -2.52
  },
  {
    "id": "t16",
    "zoom": 8,
    "x": 18.781,
    "y": -2.434
  },
  {
    "id": "t17",
    "zoom": 8,
    "x": 26.273,
    "y": -2.473
  },
  {
    "id": "t18",
    "zoom": 8,
    "x": 28.797,
    "y": -2.465
  },
  {
    "id": "t19",
    "zoom": 8,
    "x": 33.75,
    "y": -2.469
  },
  {
    "id": "t20",
    "zoom": 8,
    "x": 38.766,
    "y": -2.465
  },
  {
    "id": "t21",
    "zoom": 8,
    "x": 46.332,
    "y": -2.52
  },
  {
    "id": "t22",
    "zoom": 8,
    "x": 48.746,
    "y": -2.52
  },
  {
    "id": "t23",
    "zoom": 8,
    "x": 51.293,
    "y": -2.48
  },
  {
    "id": "t25",
    "zoom": 10,
    "x": 53.9619,
    "y": -2.5205
  },
  {
    "id": "t26",
    "zoom": 8,
    "x": 58.75,
    "y": -2.473
  },
  {
    "id": "t27",
    "zoom": 8,
    "x": 63.73,
    "y": -2.484
  },
  {
    "id": "t28",
    "zoom": 8,
    "x": 66.316,
    "y": -2.477
  },
  {
    "id": "t29",
    "zoom": 8,
    "x": 68.719,
    "y": -2.457
  },
  {
    "id": "t30",
    "zoom": 8,
    "x": 71.266,
    "y": -2.473
  },
  {
    "id": "t31",
    "zoom": 8,
    "x": 76.32,
    "y": -2.48
  },
  {
    "id": "t32",
    "zoom": 8,
    "x": 78.707,
    "y": -2.516
  },
  {
    "id": "t33",
    "zoom": 8,
    "x": 81.234,
    "y": -2.492
  },
  {
    "id": "t34",
    "zoom": 8,
    "x": 83.813,
    "y": -2.508
  },
  {
    "id": "t35",
    "zoom": 8,
    "x": 86.246,
    "y": -2.469
  },
  {
    "id": "t36",
    "zoom": 8,
    "x": 88.766,
    "y": -2.457
  },
  {
    "id": "t38",
    "zoom": 8,
    "x": 91.254,
    "y": -2.477
  },
  {
    "id": "t39",
    "zoom": 8,
    "x": 93.738,
    "y": -2.473
  },
  {
    "id": "t40",
    "zoom": 8,
    "x": 96.27,
    "y": -2.5
  },
  {
    "id": "t41",
    "zoom": 8,
    "x": 101.285,
    "y": -2.5
  },
  {
    "id": "t42",
    "zoom": 8,
    "x": 101.785,
    "y": -2.477
  },
  {
    "id": "t43",
    "zoom": 8,
    "x": 106.223,
    "y": -2.414
  },
  {
    "id": "t44",
    "zoom": 8,
    "x": 108.754,
    "y": -2.422
  },
  {
    "id": "t45",
    "zoom": 8,
    "x": 111.23,
    "y": -2.441
  },
  {
    "id": "t46",
    "zoom": 8,
    "x": 113.77,
    "y": -2.492
  },
  {
    "id": "t47",
    "zoom": 8,
    "x": 116.266,
    "y": -2.512
  },
  {
    "id": "t49",
    "zoom": 8,
    "x": 118.793,
    "y": -2.492
  },
  {
    "id": "t50",
    "zoom": 8,
    "x": 118.816,
    "y": -0.859
  },
  {
    "id": "t51",
    "zoom": 8,
    "x": 121.199,
    "y": -2.387
  },
  {
    "id": "t52",
    "zoom": 8,
    "x": 123.75,
    "y": -2.469
  },
  {
    "id": "t53",
    "zoom": 8,
    "x": 126.258,
    "y": -2.441
  },
  {
    "id": "t54",
    "zoom": 8,
    "x": 141.266,
    "y": -2.484
  },
  {
    "id": "t56",
    "zoom": 8,
    "x": 143.805,
    "y": -2.473
  },
  {
    "id": "t57",
    "zoom": 8,
    "x": 146.273,
    "y": -2.477
  },
  {
    "id": "t58",
    "zoom": 8,
    "x": 148.773,
    "y": -2.445
  },
  {
    "id": "t59",
    "zoom": 8,
    "x": 151.297,
    "y": -2.461
  },
  {
    "id": "t60",
    "zoom": 8,
    "x": 153.75,
    "y": -2.492
  },
  {
    "id": "t61",
    "zoom": 8,
    "x": 156.27,
    "y": -2.473
  },
  {
    "id": "t62",
    "zoom": 8,
    "x": 158.789,
    "y": -2.465
  },
  {
    "id": "t63",
    "zoom": 8,
    "x": 161.262,
    "y": -2.5
  },
  {
    "id": "t64",
    "zoom": 8,
    "x": 163.809,
    "y": -2.504
  },
  {
    "id": "t65",
    "zoom": 8,
    "x": 166.172,
    "y": -2.512
  },
  {
    "id": "t66",
    "zoom": 8,
    "x": 168.758,
    "y": -2.465
  },
  {
    "id": "t67",
    "zoom": 8,
    "x": 171.27,
    "y": -2.445
  },
  {
    "id": "t68",
    "zoom": 8,
    "x": 173.766,
    "y": -2.414
  },
  {
    "id": "t69",
    "zoom": 8,
    "x": 176.273,
    "y": -2.449
  },
  {
    "id": "t73",
    "zoom": 9,
    "x": 175.752,
    "y": -2.1406
  },
  {
    "id": "t74",
    "zoom": 9,
    "x": 205.707,
    "y": -2.9727
  },
  {
    "id": "t76",
    "zoom": 9,
    "x": 175.752,
    "y": -2.1406
  },
  {
    "id": "t77",
    "zoom": 8,
    "x": 176.707,
    "y": -2.453
  },
  {
    "id": "t78",
    "zoom": 8,
    "x": 178.773,
    "y": -2.441
  },
  {
    "id": "t79",
    "zoom": 8,
    "x": 181.242,
    "y": -2.457
  },
  {
    "id": "t80",
    "zoom": 8,
    "x": 183.781,
    "y": -2.461
  },
  {
    "id": "t81",
    "zoom": 8,
    "x": 186.297,
    "y": -2.484
  },
  {
    "id": "t82",
    "zoom": 8,
    "x": 188.766,
    "y": -2.477
  },
  {
    "id": "t83",
    "zoom": 8,
    "x": 191.27,
    "y": -2.488
  },
  {
    "id": "t84",
    "zoom": 8,
    "x": 193.77,
    "y": -2.477
  },
  {
    "id": "t85",
    "zoom": 8,
    "x": 196.27,
    "y": -2.41
  },
  {
    "id": "t86",
    "zoom": 8,
    "x": 198.773,
    "y": -2.418
  },
  {
    "id": "t87",
    "zoom": 8,
    "x": 201.273,
    "y": -2.453
  },
  {
    "id": "t88",
    "zoom": 8,
    "x": 206.258,
    "y": -2.5
  },
  {
    "id": "t90",
    "zoom": 9,
    "x": 204.1035,
    "y": -3.0703
  },
  {
    "id": "t92",
    "zoom": 8,
    "x": 208.789,
    "y": -2.473
  },
  {
    "id": "t94",
    "zoom": 10,
    "x": 209.2344,
    "y": -3.0088
  },
  {
    "id": "t95",
    "zoom": 8,
    "x": 211.27,
    "y": -2.52
  },
  {
    "id": "t96",
    "zoom": 8,
    "x": 218.328,
    "y": -0.961
  },
  {
    "id": "t97",
    "zoom": 8,
    "x": 223.75,
    "y": -2.477
  },
  {
    "id": "t98",
    "zoom": 8,
    "x": 226.238,
    "y": -2.496
  },
  {
    "id": "t99",
    "zoom": 8,
    "x": 228.742,
    "y": -2.484
  },
  {
    "id": "t100",
    "zoom": 10,
    "x": 228.3154,
    "y": -2.1504
  },
  {
    "id": "t101",
    "zoom": 8,
    "x": 230.676,
    "y": -2.457
  }
];
// console.log("[map].markers", markers);
var allmarkers = [];
markers.forEach(function (m) {
    var marker = L.marker([m.y, m.x]).on("click", function (e) {
        //console.log("marker click", m.id, e, this)
        window.parent.postMessage({msg: "mapclick", id: m.id}, "*")
    });
    allmarkers.push(marker);
})
var markers_layer = L.layerGroup(allmarkers);

var request = new XMLHttpRequest();
request.open('GET', 'tiles.json', true);

var hash = new L.Hash(map);

function is_above_page (item) {
    var m = /^_DSC(\d+)\.png/.exec(item.name);
    if (m) {
        var pn = parseInt(m[1]),
            pni = abovepages.indexOf(pn);
        // console.log("AP", pn, pni);
        return (abovepages.indexOf(pn) != -1)
    }
}

request.onload = function() {
  if (request.status >= 200 && request.status < 400) {
    // Success!
    var data = JSON.parse(request.responseText);
    // console.log("data", data);
    var pelts = [];
    var above_spread = false;
    var x = 0,
        y = 0,
        items = data['@graph'];
    for (var i=0, l=items.length; i<l; i++) {
        var item = items[i],
            m = /^_DSC(\d+)\.png/.exec(item.name),
            itemname = m ? m[1] : '';

        if (layoutpages && layoutpages[itemname]) {
            x = layoutpages[itemname].x;
            y = layoutpages[itemname].y;
            // console.log("using layoutpages, placing", itemname, "at", x, y);
            pelts.push({x: x, y: y, item: items[i]});
            continue
        }
        if (is_above_page(items[i])) {
            // above_spread = true;
            y = 0;
            x -= 1;
        } else {
            y = 1;
        }
        // pelts.push({x: i, y: 0, item: items[i]});
        // console.log("placing", items[i].name, "at", x, y);
        pelts.push({x: x, y: y, item: items[i]});
        x += 1;
        // if (i == 1) break;
    }
    var vt = leafygal.layout(pelts, undefined, undefined, undefined, undefined, undefined, cell_width, cell_height),
        layer = leafygal.gridlayer(L, vt, {tileSize: L.point(cell_width, cell_height)});
    map.addLayer(layer);

    map.addLayer(markers_layer);
    L.control.layers(null, { "Show links": markers_layer }).addTo(map);


  } else {
    console.log("server ERROR");
  }
};
request.onerror = function() {
  console.log("connection ERROR");
};
request.send();


}
