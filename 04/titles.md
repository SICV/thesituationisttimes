# The Situationist Times #4

[0007#t=00:15:10.477]
[st04#8/-2.477/0.707]

So, International Labyrinth Edition... Why, I don't know. It is ina a way labyrinth and spiral. Of course again that (triskele).

[0007#t=00:15:23.190]
[st04#8/-2.512/235.688]
And this is a labyrinth on a Danish box. That's the outside,

[0007#t=00:15:31.572]
[st04#8/-2.563/3.805]
and on the inside you see the grey paper on which I put the brown and the green. So it is an expensive cover.
*It is a more expensive production than the previous issue.*  
Yes. It's the first one that tries to be more scholarly, you might way "well-done". I am not even sure if I agree on the "well done". It **is** well done in that it has a double cover. 

[0007#t=00:16:11.576]
[st04#8/-2.512/4.313]
And then you get to the contents, to the Index. The index you can read at the end of the index you see the opera I was talking about this morning. 

[0007#t=00:16:30.806]
[st04#8/-2.531/233.848]
And on the back of the inside, that's an artist, Romera, who was living in Copenhagen and I just invited him. I don't really know why.

[0007#t=00:16:48.413]
[st04#8/-2.496/231.820]

And then we have the coin with the triskele. A Norwegian coin. And of course the eternal "All reproduction ... etc. is permitted".

[0007#t=00:17:00.470]
[st04#8/-2.930/230.740]
Here we have the list of where the photos came from. Even **Daniel Spoerri**, I asked him to take pictures of the game on the ground. "Editor and publisher: Jacqueline de Jong. Sub-editors: **Gordon Fazakerley**" -- a colleague and friend in Copenhagen who helped me. His wife cooked food for me, which was very nice. And **Eduard Mazman**, who was a guy I knew in Paris. I didn't know him very well, but I admired him because he been in a circus. He was called Mazman Three Finger. He had only three fingers, and he was walking upside down on the street on his three fingers. And that was very impressive. I think I knew him through a friend of mine who was a juggler. He was also a writer and heroin addict, I think.  

[0007#t=00:18:31.988]
[st04#8/-2.488/6.230]
This everyone loves, being something iconic for the labyrinth.  

[0007#t=00:19:13.307]
*Did you take the photo?*  
Of course. I got a small camera which I still have. I made all the photos when I was in Copenhagen. I was taking pictures all the time. I didn't list myself as photographer, I think.  
*No, you didn't.*  
Allthough almost all the photos are mine. 

[0007#t=00:19:50.889]
[st04#8/-2.492/6.832]
And that's **Tomaszewski**, who sent this article.
*Did you invite him to contribute?*
I am not sure, I think he just sent it to me. I wrote the editor's text. No, I don't think I invited him. 
*He must have seen number 3.*

[0007#t=00:20:36.584]
Sure, I sent it of course to be people I knew that might be interested. It is a beautiful article. And he is a quite well-known sculptor, a scholar and a sculptor.
*And these are his drawings, right?*
Absolutely,the whole thing is his. It's his drawings and photos of the works. I knew him through the Stedelijk, he had an exhibition there.

[0007#t=00:21:21.115]
**cut**

[0007#t=00:23:21.270]
So he had a big show at the Stedelijk and I was very proud of having him in the Situationist Times. **omit**

[0007#t=00:23:51.359]
[st04#8/-2.445/11.258]

So this is an article which has some big importance. How it coincides with labyrinths I can't say but it does coincide with topology. 

[0007#t=00:24:13.142]
[st04#8/-2.520/13.766]

And then an article by **Simondo**, who is talking about the labyrinth. But I didn't translate it, I just took it the way it was.  
*So it is in Italian.*  
Yes, this part is in Italian, and this is in French. It talks about labyrinths, but you can't see any labyrinths. It is Simondo, and Simondo goes back to the early ... the founding of the Internationale Situationniste.

[0007#t=00:24:51.362]
[st04#8/-2.434/18.781]

And then you get **Bucaille** on the difference between spirals and labyrinths, which is very important because of the confusion... Remember I was teaching all the time on Gotland, "**that**'s not a labyrinth,  that's a spiral"... And then, him being a pataphysician, we get Le Roi Ubu. This text of Bucaille I illustrated, that's not with his drawings. I just put all these spirals. 

[0007#t=00:25:44.973]
[st04#8/-2.473/26.273]

And he made another text. And then another: "The Analysis of Labyrinths", in a topological way.

[0007#t=00:26:04.041]
[st04#8/-2.465/28.797]

And then we get to **Lietzmann**, which I just took out of the book, as you can see. And then I compared it with photos of the SISV.  
*From Normandie.*  
Yeah. Well, it is not important whether they are from Normandie or Spain or wherever; it is important that they have these images in them.

[0007#t=00:26:57.157]
[st04#8/-2.469/33.750]

Is this the text of Bachelard? No... Is it one of the stolen texts, or ... Do you remember? It's **Jaffé**. If it's (Hans C.L.) Jaffé, the text was made for the magazine. He was my professor of art history at the university here, when I was working at the Stedelijk, and he was the second director at the Stedelijk.  
*After Sandberg?*  
No, Sandberg was primary director and he was next to Sandberg. 

[0007#t=00:28:18.123]
[st04#8/-2.465/38.766]

And then these are Roman coins with labyrinths on them. And this is one of the texts I took out of a book. And then it goes on about spirals. And if you ask me why the images are put the way they are, well it's the same story as with the others. It's the way I made it.

[0007#t=00:28:52.390]
[st04#8/-2.520/46.332]

But then with this, practically, I really used skills as best as I could, putting the numbers in a nice way, to have the spaces in a nice way. This was a professional sort of book making, which of course was much more easy.

[0007#t=00:29:20.214]
[st04#8/-2.520/48.746]

*Then you have interspersed various quotations, from James Joyce, from Bachelard ...*
Yes, I used literature too. It's just a mixture.

[0007#t=00:29:35.425]
[st04#8/-2.480/51.293]

And then you get all the Val Camonica and all this Iron or Ice Age drawings on rocks, in Northern Italy, in Scandinavia, in China or wherever. 

What's this? Charles Olson, who is he?
*An American poet.*

[0007#t=00:30:03.365]
**cut** coughing

[0007#t=00:30:15.423]
[st04#10/-2.5205/53.9619]
And it says:
"The death in life (death itself)
is endless. Eternity
is a false cause

The knot is otherwise, each topological corner 
presents itself, and no sword
cuts it, each knot is itself its fire

Each knot of which the nets are made
is for the hands to untake
the knot's making, and touch alone
can turn the knot into its own flame"

I didn't recite it very well, but it is very interesting what he is writing. No idea how I got to that, but it is interesting. I took anything I found.

[0007#t=00:31:01.060]
[st04#8/-2.473/58.750]

So it just goes on and on ... Again **Bachelard**.  
*Did you have contact with him?*  
Jorn did. I met him once. 

[0007#t=00:31:15.403]
[st04#8/-2.484/63.730]
Here we have the whole **Val Camonica** with the labyrinth. I don't know what to say about all that. It's just images and images and images with animals and labyrinths...

[0007#t=00:31:28.771]
[st04#8/-2.477/66.316]
Animals and animals without labyrinths and labyrinths on them and ...

[0007#t=00:31:35.857]
[st04#8/-2.457/68.719]

And there is again the Trojaborg. Didn't we find some... Do you remember?  
*I think it's right here. (finding separatum)*  
That's it.  
*It's actually a different text.*  
It's not by Marstrander?  
*No, it's by Sundquist. But you have marked certain images so I guess you probably copied them or ...*

[0007#t=00:32:12.940]
[st04#8/-2.473/71.266]

And then we get another text by **Bucaille**. He was really doing a lot, in a quite funny way.  

[0007#t=00:32:23.875]
[st04#8/-2.480/76.320]

*Was he living in Paris?*  
Outside of Paris. I only saw him once or twice.  
*Really?*  
No, he came to me also. But mostly he gave the manuscripts to Noël Arnaud and then Arnaud gave them to me. All this work he did for nothing. Just for copies of the magazine. I have no idea what happened to him later. 

[0007#t=00:33:00.141]
[st04#8/-2.516/78.707]

So here starts the studying of the labyrinths, and that I did in the **Bibliotheque National**. I copied whatever I saw. There's a lot of Bachelard text.  

[0007#t=00:33:13.922]
[st04#8/-2.492/81.234]

*The layout is very clean, if you compare it with the previous issues.*
Absolutely, it's because it's a good printer. 

[0007#t=00:33:54.282]
[st04#8/-2.508/83.813]
And there at some point you get to Gotland, and then you have again text. It's either Bachelard or Shakespeare, or Joyce.

[0007#t=00:34:02.818]
[st04#8/-2.469/86.246]

You see that these little numbers are very cleanly cut out.
*So this is printed in offset...*
Yes, this is all very professional.

[0007#t=00:34:27.202]
[st04#8/-2.457/88.766]
That's Hampton Court, how to walk at Hampton Court. 

[0007#t=00:34:31.144]
**cut**

[0008#t=00:00:00.319]
[st04#8/-2.477/91.254]

The way you go through mazes. I never knew what the difference is between a maze and a labyrinth. There's no difference. "Amusements in mathematics", London. And then it says in German: 'der zahlreiche weitere Beispiele'. And that text is from ... Lietzmnan.

[0008#t=00:00:47.795]
[st04#8/-2.473/93.738]

And this is in the same park ... It says "All games and movement of chairs are prohibited in the labyrinth". **To play in the labyrinth is prohibited**. Just imagine. And that's the labyrinth of Versailles. 

[0008#t=00:01:59.412]
[st04#8/-2.500/96.270]

Then we go on with garden labyrinths, all the chic labyrinths.

[0008#t=00:02:09.148]
[st04#8/-2.500/101.285]
And then towns, like Lucca, being a labyrinth. Jerusalem is also a labyrinth, but I don't think I have Jerusalem here. 

[0008#t=00:02:35.202]
[st04#8/-2.477/101.785]

And this is the famous article by **Aldo van Eyck**. He wrote and defined the typography for his own article, for this issue. Of course I was very proud that he was writing for me, but also because he did this fantastic work on it. The manuscript was in color, telling me how to compose it. It is beautiful, a fantastic thing to look at. And apparently the text became very interesting in the 1980 and 1990s for architecture students.

[0008#t=00:03:25.675]
[st04#8/-2.414/106.223]
*Was he a friend of yours?*

Yes. It's like Jaffé. They were friends who wrote. That's Lucca, which of course is more of a spiral. And this is just a pavement. 

[0008#t=00:03:41.756]
[st04#8/-2.422/108.754]

And Van Eyck's text goes on: "Call it labyrynthian clarity". Which is of course a contradiction.

[0008#t=00:03:49.809]
[st04#8/-2.441/111.230]

Then we get all these three-dimensional labyrinths. We was the flat ones, the gardens, and then we get the vertical ones, like Stonehenge. I mean they are not labyrinths, but ... called everything labyrinths. 

[0008#t=00:04:09.953]
[st04#8/-2.492/113.770]

And then there are square labyrinths, in Asia. Chinese and Japanese labyrinths.

[0008#t=00:04:19.856]
[st04#8/-2.512/116.266]
Town labyrinths, again. There's Pisa, I don't know what is labyrinthine about it. Then suddenly, this is like when you are queuing at the airport. At some moment I start to call just about everything a labyrinth.  

[0008#t=00:04:42.416]
**cut** **Buzzer bell rings**

[0009#t=00:00:02.166]
[st04#8/-2.492/118.793]

What is this Finnish Ariadne story? Of course there is the thread of Ariadne. It's a Finnish fabel. No, it's "Lokes Gesang," Loke's song, which is Scandinavian. All these myths, the same stories about labyrinths.

[0009#t=00:00:49.388]
[st04#8/-0.859/118.816]
And here is the **best** page of the whole book. So here are the labyrinths, and I drew an extra page to show how to walk in and out of the labyrinths. That page made the book very expensive.

[0009#t=00:01:23.789]
[st04#8/-2.387/121.199]
The Loke song goes on. And Theseus. And the slashing of the dragon. And there is also the mythology of ... I think it's the light crown and the cross ... of course the myth about the slashing of the dragon has to do with labyrinths, but there's also something about candles on a crown. Perhaps I am condusing all the mythologies now. And then you have the Minotaure in the labyrinth.    

[0009#t=00:03:23.963]
[st04#8/-2.469/123.750]

And then you get the... Why all of a sudden the fertility of a woman is placed together with minotaures I don't know. I have no idea why there is a Virgin and a fertility sculpture in the middle of the labyrinth. Don't ask me! Let's look it up for a change: 284. "St. George and the Dragon" again. "Venus with drinking-horn". What has that to do with labyrinths?. I think it gets a little bit confused here. I mixed all the mythologies together. Here is again the Sacandinavian ...
*Sigurd, the dragon slaughterer.*

[0009#t=00:04:45.210]
[st04#8/-2.441/126.258]
And there, again, Bucaille. All of a sudden with oceanic drawings on the people in the "Nouvelles Hébrides". And he analyses those. Not really much to do with labyrinths, but with spirals, you might say, allthough ... These are the drawings as they were on the bodies of the people. Probably this was a hobby of his. And I never refused his articles. So they just jump in in a funny way. And it's not nothing! 


[0009#t=00:05:31.221]
[st04#8/-2.484/141.266]

Well, then we get back to ... the subject. Because that was a bit far off the subject! 

[0009#t=00:05:47.822]
Here I am saying:

"L'energie causale n'est pas localisée sur le front d'onde causale. La cause réclame des convenances organiques."   

*It is actually a quote from Bachelard.*  
Yes... Why doesn't it say so? It might be Bachelard.  
*I know it is.*  

[0009#t=00:06:23.135]
[st04#8/-2.473/143.805]
Then we have Ringbom, who is a very famous archaeologist from Norway, I think. And that is the dance of the Troja. And here I explain how the spiral becomes a labyrinth. And here you see how to make a spiral, in case we didn't know.

[0009#t=00:07:12.177]
[st04#8/-2.477/146.273]
Then we get to the double thing of a spiral and a labyrinth. A game of how to walk in a labyrinth. It is a little bit like what is in number 3. And then all of a sudden we have these nuns walking behind each other ... No idea why they are here! And here you have the spiral as a snake, walking in the shape of a snake. 307, what is this? "Procession in a Toledo." Well, of course it is a procession. You can't see anything. And I have absolutely no explanation why it is in the magazine.

[0009#t=00:08:09.589]
[st04#8/-2.445/148.773]
Again processions and processions ... I don't know why processions all of a sudden become labyrinthic. And that's how you walk in an underground train station. And that's how you walk if you are drawn on a rock in Val Camonica, or ... Is it Val Camonica? I am not sure about anything. Sahara. It's a battle. 

[0009#t=00:08:48.737]
[st04#8/-2.461/151.297]

And then ... I don't know why I put this in. The Egyptian Tarot. Is it the images? I don't remember why I included this. Probably it has something to do, mythologically, with labyrinths.

[0009#t=00:09:24.512]
[st04#8/-2.492/153.750]

It is **Doets**, who was a mathematician who sent this to me. And I just put it in, apparently. But I can't see any connection. 

[0009#t=00:09:37.908]
[st04#8/-2.473/156.270]

And there we go with games. Very old Assyrian games and the Dutch/German game of the game of the goose (Ganzenbord). And these are the games children make on the street and the photos are made by **Spoerri**. I asked him to make these photos, because I saw them and he had a camera.

[0009#t=00:10:01.337]
[st04#8/-2.465/158.789]
And here we have all sorts of games together, which are similar to the labyrinth walk and dance. The uterus mundi -- the cosmic mother body is in here. And then the heaven and hell game, which is spiralling.

[0009#t=00:10:46.665]
[st04#8/-2.500/161.262]
The children games are called 'heaven and hell'. And then you get churches with heaven and hell.

[0009#t=00:11:01.518]
[st04#8/-2.504/163.809]
And you get mini-golf with heaven and hell and Bachelard with heaven and hell. And you get all sorts of funny drawings which are not heaven and hell.

[0009#t=00:11:11.597]
[st04#8/-2.512/166.172]

And here we return to rock carvings and church drawings with similar forms.

[0009#t=00:11:21.167]
[st04#8/-2.465/168.758]
And getting back, sort of, to **pinball machines!** Don't you think? Round pinball machines. And magnetic pinball machines. They are like roulette.

[0009#t=00:11:42.176]
[st04#8/-2.445/171.270]

And these are graffitti from churches in Normandy, I think. 

[0009#t=00:11:53.269]
[st04#8/-2.414/173.766]
And then you get the **Musée labyrinthe** and the **Stedelijk Museum**, where I am telling what was going on at the Stedelijk Musuem in 1960. And somewhere I am saying that ... And of course the rat mazes. And this is le Corbusier. But where is the Situationist exhibition? Because this is the drawing of the Situationist project and where I say how it all went wrong.

[0009#t=00:12:36.368]
[st04#8/-2.449/176.273]

With *Dylaby*. And then, like in no. 1, you get the atomic fallout shelters.

*WHy did you put Dylaby in there? Was it a comment on ...?*  
Of course it was a comment.

[0009#t=00:13:02.357] **cut**

[0009#t=00:13:12.486]

Because it was the next thing that happened after the Situationist exhibition in 1960 was declined by Sandberg. And my theory is that he declined it because he didn't want to have it and he was already talking with Pontus Hultén about preparing *Dylaby*, 1,5 years later. That's why I put it in. 

[0009#t=00:13:48.918]
The Situationist exhibition was called labyrinth and they called it Dylaby, which means dynamic labyrinth. I thought I had to put it in. After all it had to do with my former comrades. And I put it very, very small, so it is hard to read. This was the exhibition, the Dylaby. But somewhere there is the comment on the ... We have to look it up. It was a consequence of this cheating of ... 

[0009#t=00:14:51.306]
[st04#9/-2.1406/175.7520]
Here it is, 357:

"The map of the 'Dylaby' exhibition in the Stedelijk Museum, 1962. See for this no. 413-422."

[0009#t=00:15:15.611]
[st04#9/-2.9727/205.7070]

Okay, we go to 413-422. Oh yeah, Constant. These two are by Constant.

[0009#t=00:15:38.690]
**cut**

[0009#t=00:15:42.650]
[st04#9/-2.1406/175.7520]

"And see the International Situationniste no. 4. After the discussion in 1960 on the idea of changing the museum and the town of Amsterdam into a labyrinth for the Situationist exhibition at this museum, the direction decided that it was not realisable. Nevertheless, the same direction, the same direction two years later accepted the 'Restany'-group to develop 'their' labyrinth in the same museum. It is an evidence that this Fake-labyrinth never penetrated as a modification of the town, not even temporary. The exhibition which is at this very moment at the ICA, London, is a very similar event only (not yet) using the word 'labyrinth' with urbanistic aspects of the early IS which these people were able to absorb. As the most clear of these might be considered the text 'Situation Gloop' (see '60 and '61 Alloway's Geometrical 'Situation'-School in the same ICA). The day these people will discover Topology (Situology) is not far, only this, of our aspects, they will not be able to be transformed into a Consumer-manipulation. It will be found too ambiguous for them (see no. 2 of Living Arts).

*Living Arts* is the bulletin or magazine of the ICA. So this text is mine, this is my comment.

*In the index of image captions ...* 

[0009#t=00:17:47.662]
[st04#8/-2.453/176.707]

We are at the shelters. So I hope that comment explains this.

[0009#t=00:17:57.864]
[st04#8/-2.441/178.773]

Then we get him again, **Bucaille**! He is really very present. And then we get to intestines, which are also labyrinths.

[0009#t=00:18:07.124]
[st04#8/-2.457/181.242]

In the ear there's a labyrinth. And then Henri Michaux with a poem about labyrinths. And then we have all these laboratory labyrinths, and hearts and interiors ... This is in the sky, collusions in the sky.

[0009#t=00:18:42.008]
[st04#8/-2.461/183.781]

And there we get all the Chinese labyrinths used for acupuncture.

[0009#t=00:18:51.926]
[st04#8/-2.484/186.297]
They get more and more labyrinthic the labyrinths. Inside of our brains, obviously. That's a computer, inside our brain, I think it is even a Chinese computer.

[0009#t=00:19:04.034]
[st04#8/-2.477/188.766]
Then we get the labyrinth in the ear. 

[0009#t=00:19:09.900]
[st04#8/-2.488/191.270]
Then we get something - I don't really know what it is - but is in on top of Chinese swords. It's an element that looks like some spiral labyrinths. And we go back to labyrinths!

[0009#t=00:19:25.305]
[st04#8/-2.477/193.770]

And then we get Arab labyrinths. Here we get to the Arab part. And this is (Gustav René) Hocke, a philosopher and mathematician, who says "Umwege führen zum Mittelpunkt" - detours lead to the middle. 

[0009#t=00:19:58.760]
[st04#8/-2.410/196.270]

Then there is a small text by Kafka and a small text by Bachelard. And there seems to be a labyrinthine game that was at some moment fashionable. And Orlando Furioso from 1532 and the Holy Forest which is also labyrintic. Don't ask me why and how! Orlando Furioso is an opera by Verdi.

[0009#t=00:20:45.731]
[st04#8/-2.418/198.773]

Here we have Bachelard with a text, and that's a portrait by **Jorn** of Bachelard. And that's a portrait by **Alechinsky** of Bachelard. Or is it of Bachelard? No, it's Stephen Dedalus. 

[0009#t=00:21:27.751]
[st04#8/-2.453/201.273]

Then we get **Mazman**: "A seeker of Labyrinths." And I put two prisons as illustrations of this guy with the three fingers. Walking on a tightrope.

[0009#t=00:22:03.623]
[st04#8/-2.500/206.258]

Then we have **Gordon Fazakerley**: "Labyrinth project." We have **Constant**, and Amsterdam. And I think this is the maquette for the Situationist exhibtion, 417 ... Constant. 418: Constrcution of thw town of Amsterdam. Staircase of the Sanatorium in the Fiat factories (415).

[0009#t=00:22:52.949]
**cut** coughing

[0009#t=00:23:02.282]
[st04#9/-3.0703/204.1035]
Because this is Constant too. And this is actually the maquette of the exhbition at the Stedelijk - 416.

[0009#t=00:23:13.351]
**cut**
*Did you have a lot of contact with Constant still?*  
Yes. 

[0009#t=00:23:43.037]
[st04#8/-2.473/208.789]

So Christmas 1962. That's Fazakerley's drawings. He was quite a draughtsman.

[0009#t=00:23:59.085]
**cut**

[0009#t=00:24:03.695]
[st04#10/-3.0088/209.2344]

And he made this, as a labyrinth.

"To our English admirers. We don't doubt that a consumer version of the Labyrinth will appear in the near future in 'LIVING ARTS', the English Peripheral Documentary magazine. We suggest that 'Living Arts', the French magazine 'Planète' and the German establishment magazine 'Das Labyrinth' amalgomate and Spirallike get lost in the Rat Race."

I am very friendly!
*Friendly and funny.*

[0009#t=00:24:40.400]
[st04#8/-2.520/211.270]

And then you get the letter from Zagreb, which **Peter Schat** wrote about the fact that he was going to call his opera *The Labyrinth*. Not saying that it was because of the magazine. As if they had decided that out of the blue. Instead of *Paradijsvogel*.

[0009#t=00:25:03.131]
[st04#8/-0.961/218.328]

And there is the complete score of the **Labyrinth opera**, with all the original images they used as decor and for costumes.

[0009#t=00:25:26.523]
[st04#8/-2.477/223.750]

And then we have the index, with Charlie Chaplin looking into a labyrinthine mirror.

[0009#t=00:25:34.417]
[st04#8/-2.496/226.238]
I dont know what this is ... 427: Map of the early Bronze age rock engravings in Spain. So that means all the rock engravings I am using in this issue, well not all of them ... 

[0009#t=00:26:17.294]
[st04#8/-2.484/228.742]
428: House of the Black snake: the heaven of warriors (sun + moon) from the old Mexican codex Borgia. And then we have a wood ceiling from Palazzo Ducale in Matua (429), Baden Powell and lovers' knot by Gordon Fazakerley.

[0009#t=00:27:07.017]
[st04#10/-2.1504/228.3154]
And that's the sources. And this text (note 357) is one of these texts I completely forgot about. It's quite interesting, it's 1963-64 and I am still furious at the **Stedelijk** doing the *Dylaby*. 
*For how long did you work there?*
2,5 years. Until the end of 1960. Then I left for Paris. 

[0009#t=00:27:44.186]
[st04#8/-2.457/230.676]

And then we have this, and that's the end of this story. It's complete nonsense, the last drawing.

[0009#t=00:27:52.617]
**cut**










[0004]: ../video/2017-12-15/MVI_0004.web.mp4 "video"
[0005]: ../video/2017-12-15/MVI_0005.web.mp4 "video"
[0006]: ../video/2017-12-15/MVI_0006.web.mp4 "video"
[0007]: ../video/2017-12-15/MVI_0007.web.mp4 "video"
[0008]: ../video/2017-12-15/MVI_0008.web.mp4 "video"
[0009]: ../video/2017-12-15/MVI_0009.web.mp4 "video"
[0010]: ../video/2017-12-15/MVI_0010.web.mp4 "video"

[st04]: ../04/scans.html "scans"
