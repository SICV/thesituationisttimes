# The Situationist Times #06


[0015#t=00:03:12.662]
This is **Walasse Ting**, a self-portrait. This is the cover of *1 cent Life*. He invited all his friends in the states. Everyone is in it. I don't know if Jorn was in it. That's Alechinsky.

*Jorn is in it.* 

Of course he is. Here you see the way it is made, it is very precious. And it is a very good idea, because it has all the artists around Walasse Ting. Sam Francis, Walasse Ting himself ... I think Oldenburg is in it, Rauschenberg, all the Americans are participating.

[0015#t=00:07:16.716]
That's another Jorn.

[0015#t=00:07:34.794]
It's a fantastic book. So this gives an impression ... 1962. There you see how much it costs now ... Which is not surprising given the artists that were involved. So that came out and I liked it very much. What I didn't like was that young people like us, my colleagues and me in Paris, were not in it. 

[0015#t=00:08:48.028]
It was a very up-to-date thing to make. But also very based on what was going on in the States, much more than in Paris, even though Walasse Ting was also living there. It was also huge, so it was very American. Like the croissants are much bigger, the book was much bigger. But it is of course a document of that moment. And then I thought I could make something small, in the size of my magazine, with my friends and the people I knew, my surroundings in Paris, which is very different from New York, or the States. And I could do it in an inexpensive way. To really make something inexpensive. 

[0015#t=00:10:01.531]
I talked it over with Jorn and he said it was a good idea. I said, let's find out how much it will cost and what the profit would be. The profit would then be used to make number seven. So I went to the bank Credit Lyonnais in Paris and proposed my project, which was very unusual at that moment. And they lent me money, which is amazing. I think they did it because my father paid my rent, which was peanuts compared to the cost of this project. But there was a regularity of income, it was at least something. 

[0015#t=00:11:07.288]
So I could at least start talking with **Bramsen**, the printer, about starting it. Then I spoke with **Rhodos** and said that all the surplus would go into no. seven and that's where The Situationist Times went wrong. So I started making lists of artist that I wanted to participate, and it was really concentrated on Paris. So I didn't ask SPUR peoplefor example. And I didn't ask Hans Haacke, who was a friend in New York. It had to be practical, because my idea was to have the first "Situationist Times" in color and have it printed with litho stone in four colors: red, yellow, blue and black. And that meant that the artists had to come back to the printshop for every color. So technically you couldn't really do it with someone not present in Paris. They had to come four times. Or you could only print in black, as some of them did. Some people refused and thought it was a silly idea. Like (Karel) Appel. Later, when it came out he asked me why I didn't invite him. 

[0015#t=00:13:12.448]
Also, like *1 Cent Life*, it's a moment. A moment when you have those friends, and not others. Later on I wondered why I didn't have Boltanski, for instance, but he was not really a painter at that moment. He was a painter without being a painter. I probably thought about people who had experience with graphics, who were able to draw on stone. You have to know how to use the colors.   

[0015#t=00:14:14.122]
I felt a little bit like a teacher. I can perhaps read this, where I explain the conditions. It's Paris, February 1967. 

"Dear friends. I have been talking about the number six of The Situationist Times, which we are going to make in litho form at Clot, Bramsen and Georges. This issue will be completely of lithos and with a printrun of 2,000. 320 will be hors commerce and for the participating artists. 32 artists will join. They all live in Paris, either permanently or temporary. The issue will be sold for 45 francs. The issue is meant to come out next spring. The five former Situationist Times are completely sold out. Here are the conditions: 1) ten copies will be reserved to each artist, 2) each litho will be signed on the stone and have a maximum of four colors; the colors are attached to this letter; the size is 19 by 25 cm; 3) I ask you to come with your maquette to the printer; the work will be much easier with a maquette; we propose four colors, but you can play with them in superpositions, and a big variety is made possible with these colors; 4) for the internal organization, the stones -- one for each color -- will be split into eight compartments, one for each artist, that means 32 compartments for each color; 5) the stones will be at your disposition (and then I give the address ...). If for any personal reason you will not be able to come you are free to do it at some other moment, but I would be happy if you could get in touch as soon as possible to let me know when and how you will be working there. As you can see, the success of this enterprise is quite complex and that is why I am insisting, once more, that you have to get in touch with me and let me know your intentions as fast as possible."

Very school teacher-like! It's all very formal. 

[0015#t=00:21:35.210]
And then we get the list. And of course this list is more recent and went to all the bookstores and interested people:

"The Situationist Times presents number sex, crossed out: six. The price is 60 Danish kroner + postage."

It was Rhodos who sent this out. 

[0015#t=00:22:52.980] 
Then what happened, when this went around, the gallery in Amsterdam and my friend who was distributing here, sold enough in advance to start paying the printer. That was very special. So I had it covered before starting.

[0015#t=00:23:55.856]
**cut**

[0016#t=00:00:22.490]
[06.html#7/-1.609/1.359]
OK then we start with the cover... 

I don't know why I put this mauve... It is red and blue together. It is printed on black paper, with white and these colors. 

[0016#t=00:00:56.077]
[06.html#7/-1.734/181.484]

This is **Ulf Trotsig** who made it (it looks like a potato actually). I don't know why he made it the way he made it. As usual, I let people do whatever they wanted. The cover is completely different from the inside in this case, which is strange.

[0016#t=00:01:27.601]
**cut**

[0016#t=00:01:46.388]
[06.html#7/-1.602/177.570]

I put the inside cover, left and right, completely black. I used this blue as a sneer but also homage to Yves Klein, and wrote: next page the poin of zero.

[0016#t=00:02:24.558]
[06.html#7/-1.805/8.633]

I wrote "The Parisian Edition". The Situationist Times no. six. Now the fact that the magazine copy is in the state that it is, is one of the reasons why I went sort of bankrupt with 'The Situationist Times*. Everything was perfect, and like I hoped it would be. But the bookbinder refused to bind it in the way I instructed him. Then I refused to pay him. Because it was falling apart the day it came out. 

[0016#t=00:03:31.757]
It was launched with a big exhibition party, it couldn't have been more glorious than it was, at **La Hune**, Boulevard St.Germain. With both vitrines covered completely with all the sheets from the printer. Not yet cut. They were selling the magazine but they were also showing it. And there are lots of photos.

[0016#t=00:04:10.933]
**cut**

[0016#t=00:04:38.303]
La Hune was *the* bookshop for art books until very recently. And at the same moment at Place St. Germain, at **Le Divan**, it was spread out. Here it it is together with the important intellectual reviews. So it had an enormous launch. But when people got the magazine, it fell apart. So I refused to pay the guy.

[0016#t=00:05:58.377]
For juridical reasons I was forced to pay him. Then I asked for money from Rhodos. First they refused to send money to the printer. Then we got into a fight. Then they refused to pay the bookbinder, not because of its quality, but for the simple reason that they had spent the money on a scientifc book. There was no money left for me. Then I had to get a lawyer and the whole thing started collapsing.

[0016#t=00:06:37.200]
In the end I got all the remaining copies of *The Situationist Times*, instead of money. And I had to pay the printer. End of story. So no number seven, as the idea was to make money for that with number six. So that I didn't have to depend on Jorn all the time. It would make me freer and self-supported, but I never got there. On the contrary, it is the end of the story. 

[0016#t=00:07:23.069]
[06.html#7/-1.672/173.477]

But before the end of the story is the beginning of the story. Getting the damn thing made. The little text there is, like this text, had to be printed too, at another printshop. It says it is "commissioned" by Rhodos. All the lithos are printed at Clos, Bramsen et Georges. And I say: 

"The editor thanks herewith the 33 artists (it is 33 with the cover) who were willing to make this experiment. This issue was completed furing the spring and autumn of 1967."

Because during the summer no one is in Paris. It took a long time to have it done.

[0016#t=00:08:55.688]
[06.html#7/-1.719/12.500]

So the we start with page 1, which is funny, because it's an A and it's **Pierre Alechinsky**. What he did, is explaining in the most basic way what I am talking about regarding the colors. You see the blue, yellow and red. I calculated this. It is a sort of lesson in how to do colors. 

[0016#t=00:09:54.344]
[06.html#7/-1.672/17.570]

Then we get **René Bertholo**, who apparently didn't understand much of the colors. You know the way artists are: they don't think about anything but their own work. It's not very nice what I am saying, but ... Bertholo and **Lourdes Castro** had a magazine together which I was very impressed by. Made in silkscreen. It was a wonderful magazine they made. They were a couple, Portuguese, and very Fluxus-like artists. Nowadays Lourdes Castro is very  well-known. She's one of the Fluxus women who gets big retrospective exhibitions all over the world. So Bertholo did what he did. It has nothing to do with the color idea of mine, but he is using these colors, obviously. It is a very conceptual thing.

[0016#t=00:11:30.250]
The whole selection of artists is very arbitrary, allthough you could say that they are just friends.

*It's like a snapshot of a scene, I think.*

Yes, in a way. And that is also what *1 cent life* was. 

[0016#t=00:11:44.357]
[06.html#7/-1.641/22.523]

So there we have **Guido Biasi**, who was an Italian in Paris.

[0016#t=00:11:55.294]
[06.html#7/-1.602/27.578]

This is a Swiss, obviously. **Samuel Buri**, a quite well-known artist, also. A Swiss in Paris. 

[0016#t=00:12:05.119]
[06.html#7/-1.641/32.578]

This is **Lourdes Castro**.

[0016#t=00:12:11.584]
[06.html#7/-1.570/37.547]

That's a Dutch guy, **Martin Engelman**, who became a professor. He had already lived in Paris for a long time. He's older, but they are all my age, it is very personal. But he is older. Later on he became a professor in Berlin.

[0016#t=00:12:33.683]
[06.html#7/-1.617/42.484]

**José Gamarra** who is Guatamalese, living in Paris. It's a very nice one, I think. I mean they are all nice in their own way. 

[0016#t=00:12:47.965]
[06.html#7/-1.719/47.563]

This one is not very nice. Another German one, I don't understand it at all. 

[0016#t=00:12:57.706]
[06.html#7/-1.563/52.492]

Another Swiss one, who really used the colors.

[0016#t=00:13:05.586]
[06.html#7/-1.563/57.539]

**Reinhoud d'Haese**, whose orange peels you saw in no. 3. And he was too lazy to come three times.  
*He only came once ...*

[0016#t=00:13:26.069]
[06.html#7/-1.602/62.547]
This was the wife of Jan Voss, **Hannelore**. She became a man later on, changed gender. She started making performances and then film, and at the same time she became a man. I don't know what she's called. 

[0016#t=00:13:39.994]
[06.html#7/-1.516/67.461]
This is a Mexican, **Hernandez**. I think it is very beautiful.

[0016#t=00:13:47.602]
[06.html#7/-1.633/72.570]
This one is quite amazing. He's a pataphysician/surrealist, **Maurice Henry**. Very well-known in these circles. And it is amazing what they do with the same colors! That's what is so fantastic about it. 

*Was he your age or ...*

No, he was Asger's age. And I knew him through Prévert and Bucaille. 

[0016#t=00:14:14.209]
[06.html#7/-1.648/77.531]

Well, that's poor me. *On fait ce qu'on peut* (You do what you can)

[0016#t=00:14:22.775]
[06.html#7/-1.656/82.617]

That's Jorn, who at least knows how to make lithos. 

[0016#t=00:14:28.375]
[06.html#7/-1.633/87.609]

That's **Peter Klasen**, very typical Peter Klasen, a very well-known German artist in Paris. 

[0016#t=00:14:35.536]
[06.html#7/-1.594/92.563]

Here we have **Wifredo Lam** who of course only came once, but he did it.

[0016#t=00:14:42.409]
[06.html#7/-1.563/97.531]

**Lea Lublin**, she's another one of the conceptual artists, who became very famous. Mainly  as a feminist conceptual artist, she's really world wide known. She's originally from ... I think she's Polish. And she was a little bit older. But she was within the group with Segui and so on. She is very famous for her feminism, performances and figures.

[0016#t=00:15:26.854]
[06.html#7/-1.578/102.602]

This is **Paul de Lussanet**, a Dutch friend who had been living a long time in Paris. Of my age. 

[0016#t=00:15:35.068]
[06.html#7/-1.586/107.523]

**Maglione**, wife of del Pezzo, who pretended to be a big artist. She was very feminine. Not feminist, but feminine.

[0016#t=00:15:48.742]
[06.html#7/-1.664/112.570]

There we have **Alejandro Marcos** who is still around. He is an Argentinian artist, with whom I made posters in 68.

[0016#t=00:16:03.876]
[06.html#7/-1.656/117.547]

He and she (Cristina Martinez) and Vanarsky were working for Vasarely, who had a factory of artists (today a very usual thing) working for him to earn money. They made his works. Now even very young artist have slaves. **Cristina Martinez**, she is still around in Paris, she was the the wife of Vanarsky and a very good artist. Also in a way a feminist. Most of the last ones are Argentinians.

[0016#t=00:17:00.666]
[06.html#7/-1.703/122.570]

Then we have **Matta**, who was very sweet to make the drawing, but didn't come back to do the colors. He knew very well how to make lithos. 

[0016#t=00:17:10.273]
[06.html#7/-1.703/127.602]

**Del Pezzo**, an Italian who was very well known at that moment.

[0016#t=00:17:15.670]
[06.html#7/-1.711/132.602]

That's a Dutch artist who actualy came three times from Amsterdam to do this. He was so proud to be in the magazine that he did that. At that time I thought he was a very good artist. Then he became a professor and he was less ...

[0016#t=00:17:32.966]
[06.html#7/-1.711/137.609]

Then we have **Saura**,  I think it is absolutely marvellous what he made. There is this generation and friends of Jorn and then of me.

*It's interesting the trans-generational aspect of this issue.*

[0016#t=00:17:46.498]
[06.html#7/-1.625/142.570]

That's **Antonio Segui**, the Argentinian. He was earning a lot of money. He had night clubs and things like that. He had an aeroplane hangar/studio and lots of artist working in his studio, for themselves. And we held parties there.

[0016#t=00:18:11.751]
[06.html#7/-1.563/147.586]

That's a Dane who was living in Paris, **Haugen Sørensen**. 

[0016#t=00:18:17.722]
[06.html#7/-1.508/152.555]

That's a Japanese living in Paris.

[0016#t=00:18:24.443]
[06.html#7/-1.523/157.664]

Then we have the famous **Topor**. 

[0016#t=00:18:28.006]
[06.html#7/-1.680/162.555]

And here again an Argentinian, with whom I made the ... I have some moving sculptures downstairs of his. He was a very close friend. 

[0016#t=00:18:40.404]
[06.html#7/-1.594/167.625]

**Jan Voss** was also a well-known artist at that moment. Ther are all *nouvelle figuration* more or less, the artists of my generation who are included. 

[0016#t=00:18:52.192]
[06.html#7/-1.656/173.453]
So that's it. End of story. Is that all? Sorry, there you have the end of *The Situationist Times*. I must say, when you look at it like that, we did it in four days. It's not so much! It's very sad that there is not more ...

[0016#t=00:19:22.986]
**cut**



[0011]: ../video/2017-12-17/MVI_0011.web.mp4 "video"
[0012]: ../video/2017-12-17/MVI_0012.web.mp4 "video"
[0013]: ../video/2017-12-17/MVI_0013.web.mp4 "video"
[0014]: ../video/2017-12-17/MVI_0014.web.mp4 "video"
[0015]: ../video/2017-12-17/MVI_0015.web.mp4 "video"
[0016]: ../video/2017-12-17/MVI_0016.web.mp4 "video"
[0017]: ../video/2017-12-17/MVI_0017.web.mp4 "video"
[0018]: ../video/2017-12-17/MVI_0018.web.mp4 "video"
[0019]: ../video/2017-12-17/MVI_0019.web.mp4 "video"
[0020]: ../video/2017-12-17/MVI_0020.web.mp4 "video"
[0021]: ../video/2017-12-17/MVI_0021.web.mp4 "video"
[0022]: ../video/2017-12-17/MVI_0022.web.mp4 "video"
[0023]: ../video/2017-12-17/MVI_0023.web.mp4 "video"

[06.html]: ../06/scans.html "scans"

