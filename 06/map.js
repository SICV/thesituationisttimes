function init_map (max_zoom, abovepages, layoutpages) {

var cell_width = 320,
    cell_height = 209;

if (abovepages === undefined) { abovepages = [] }
 
var map = L.map('map', {
        maxZoom: max_zoom || 9,
        minZoom: 0,
        zoom: 0,
        crs: L.CRS.Simple,
        center: new L.LatLng(0,0)
});


var markers = [
  {
    "y": -1.609,
    "id": "t12",
    "zoom": 7,
    "x": 1.359
  },
  {
    "y": -1.734,
    "id": "t13",
    "zoom": 7,
    "x": 181.484
  },
  {
    "y": -1.602,
    "id": "t15",
    "zoom": 7,
    "x": 177.57
  },
  {
    "y": -1.805,
    "id": "t16",
    "zoom": 7,
    "x": 8.633
  },
  {
    "y": -1.672,
    "id": "t22",
    "zoom": 7,
    "x": 173.477
  },
  {
    "y": -1.719,
    "id": "t23",
    "zoom": 7,
    "x": 12.5
  },
  {
    "y": -1.672,
    "id": "t24",
    "zoom": 7,
    "x": 17.57
  },
  {
    "y": -1.641,
    "id": "t26",
    "zoom": 7,
    "x": 22.523
  },
  {
    "y": -1.602,
    "id": "t27",
    "zoom": 7,
    "x": 27.578
  },
  {
    "y": -1.641,
    "id": "t28",
    "zoom": 7,
    "x": 32.578
  },
  {
    "y": -1.57,
    "id": "t29",
    "zoom": 7,
    "x": 37.547
  },
  {
    "y": -1.617,
    "id": "t30",
    "zoom": 7,
    "x": 42.484
  },
  {
    "y": -1.719,
    "id": "t31",
    "zoom": 7,
    "x": 47.563
  },
  {
    "y": -1.563,
    "id": "t32",
    "zoom": 7,
    "x": 52.492
  },
  {
    "y": -1.563,
    "id": "t33",
    "zoom": 7,
    "x": 57.539
  },
  {
    "y": -1.602,
    "id": "t34",
    "zoom": 7,
    "x": 62.547
  },
  {
    "y": -1.516,
    "id": "t35",
    "zoom": 7,
    "x": 67.461
  },
  {
    "y": -1.633,
    "id": "t36",
    "zoom": 7,
    "x": 72.57
  },
  {
    "y": -1.648,
    "id": "t37",
    "zoom": 7,
    "x": 77.531
  },
  {
    "y": -1.656,
    "id": "t38",
    "zoom": 7,
    "x": 82.617
  },
  {
    "y": -1.633,
    "id": "t39",
    "zoom": 7,
    "x": 87.609
  },
  {
    "y": -1.594,
    "id": "t40",
    "zoom": 7,
    "x": 92.563
  },
  {
    "y": -1.563,
    "id": "t41",
    "zoom": 7,
    "x": 97.531
  },
  {
    "y": -1.578,
    "id": "t42",
    "zoom": 7,
    "x": 102.602
  },
  {
    "y": -1.586,
    "id": "t43",
    "zoom": 7,
    "x": 107.523
  },
  {
    "y": -1.664,
    "id": "t44",
    "zoom": 7,
    "x": 112.57
  },
  {
    "y": -1.656,
    "id": "t45",
    "zoom": 7,
    "x": 117.547
  },
  {
    "y": -1.703,
    "id": "t46",
    "zoom": 7,
    "x": 122.57
  },
  {
    "y": -1.703,
    "id": "t47",
    "zoom": 7,
    "x": 127.602
  },
  {
    "y": -1.711,
    "id": "t48",
    "zoom": 7,
    "x": 132.602
  },
  {
    "y": -1.711,
    "id": "t49",
    "zoom": 7,
    "x": 137.609
  },
  {
    "y": -1.625,
    "id": "t50",
    "zoom": 7,
    "x": 142.57
  },
  {
    "y": -1.563,
    "id": "t51",
    "zoom": 7,
    "x": 147.586
  },
  {
    "y": -1.508,
    "id": "t52",
    "zoom": 7,
    "x": 152.555
  },
  {
    "y": -1.523,
    "id": "t53",
    "zoom": 7,
    "x": 157.664
  },
  {
    "y": -1.68,
    "id": "t54",
    "zoom": 7,
    "x": 162.555
  },
  {
    "y": -1.594,
    "id": "t55",
    "zoom": 7,
    "x": 167.625
  },
  {
    "y": -1.656,
    "id": "t56",
    "zoom": 7,
    "x": 173.453
  }
];
// console.log("[map].markers", markers);
var allmarkers = [];
markers.forEach(function (m) {
    var marker = L.marker([m.y, m.x]).on("click", function (e) {
        //console.log("marker click", m.id, e, this)
        window.parent.postMessage({msg: "mapclick", id: m.id}, "*")
    });
    allmarkers.push(marker);
})
var markers_layer = L.layerGroup(allmarkers);

var request = new XMLHttpRequest();
request.open('GET', 'tiles.json', true);

var hash = new L.Hash(map);

function is_above_page (item) {
    var m = /^_DSC(\d+)\.png/.exec(item.name);
    if (m) {
        var pn = parseInt(m[1]),
            pni = abovepages.indexOf(pn);
        // console.log("AP", pn, pni);
        return (abovepages.indexOf(pn) != -1)
    }
}

request.onload = function() {
  if (request.status >= 200 && request.status < 400) {
    // Success!
    var data = JSON.parse(request.responseText);
    // console.log("data", data);
    var pelts = [];
    var above_spread = false;
    var x = 0,
        y = 0,
        items = data['@graph'];
    for (var i=0, l=items.length; i<l; i++) {
        var item = items[i],
            m = /^_DSC(\d+)\.png/.exec(item.name),
            itemname = m ? m[1] : '';

        if (layoutpages && layoutpages[itemname]) {
            x = layoutpages[itemname].x;
            y = layoutpages[itemname].y;
            // console.log("using layoutpages, placing", itemname, "at", x, y);
            pelts.push({x: x, y: y, item: items[i]});
            continue
        }
        if (is_above_page(items[i])) {
            // above_spread = true;
            y = 0;
            x -= 1;
        } else {
            y = 1;
        }
        // pelts.push({x: i, y: 0, item: items[i]});
        // console.log("placing", items[i].name, "at", x, y);
        pelts.push({x: x, y: y, item: items[i]});
        x += 1;
        // if (i == 1) break;
    }
    var vt = leafygal.layout(pelts, undefined, undefined, undefined, undefined, undefined, cell_width, cell_height),
        layer = leafygal.gridlayer(L, vt, {tileSize: L.point(cell_width, cell_height)});
    map.addLayer(layer);

    map.addLayer(markers_layer);
    L.control.layers(null, { "Show links": markers_layer }).addTo(map);


  } else {
    console.log("server ERROR");
  }
};
request.onerror = function() {
  console.log("connection ERROR");
};
request.send();


}
