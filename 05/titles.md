# The Situationist Times #5

[0042#t=00:00:03.449]
[05.html#8/-0.859/0.750]

Number 5. While I was working on number 4, I was thinking about the fact that spirals and rings had not been really used in the... spirals had, but rings and chains, let's say, which was the next important topic to be done in the imagination of entrelac and topology. 

[0042#t=00:00:40.884]
So I put this, that's the Olympic rings, which are actually not in a topological way, because they are interclosed ring by ring by ring, rather than like that (gestures) it would. And then it's called *International Olympic Edition*.

[0042#t=00:01:02.591]
And then again, I start using this topological text from... what's his name..?
*Kurt Lewin.*
Exactly thank you. Which in the other issue is all over the place. Then I have of course, not Niki de Saint Phalle, or the Situationist in Odense... but a real ancient one, and here is a chain. And the chain is of course in metal, and this is in metal. And there you have a Val Camonica one.

[0042#t=00:01:45.571]
[05.html#8/-0.867/280.797]

Then on the back, I just put this, I found this somewhere in Denmark, when I was in Denmark for the other Issue,  I then I put "What means this mark?" and no idea... no answer.

[0042#t=00:02:04.246]
[05.html#8/-0.836/3.203]
And then on the inside of the cover, I put the chains of the Industrial Revolution, so very big chains, a man with a chain here, a man with a cigar. A very rich capitalist individual, and this (the silver seal) is just a practical thing but also a nice looking thing. And this is Mr. Brunel "in the middle of the (19th) century with a steamer, and the steamer is called the Great Eastern." With his anchor chain, and the big chain of the steamer.

[0042#t=00:02:54.109]
[05.html#8/-0.910/4.355]
Then we have the Eiffel tower together with a very big ring, and tumble rings.

[0042#t=00:03:10.422]
[05.html#8/-0.859/279.367]
We were talking about the cover, so here is the Capitalist part, and here is a little colored boy with one part of a chain, to have the difference from the one and the other.

[0042#t=00:03:29.773]
[05.html#8/-1.082/4.363]

Well, here's the Index, and here is the explanation, in English, from the dictionary of what is a ring and what is a chain. And in the index you can read who is participating in text and also in image.

There is one text which **Jorn** wrote for this issue, and the other one I took from ... I don't remember. *Art and orders*, and *Mind and sense*... Did I take it from somewhere or was it written especially for this issue? We can see that when we look through.

[0042#t=00:04:16.006]
**Max Bucaille** is participating with four articles. He's always participating a lot, I mean he's doing half the magazine in a way... Five articles I find! And a text from **Virtus Schade**, Danish writer and Art Critic, and ... six Bucailles!! The further I go down the more there is.

[0042#t=00:04:49.595]
Then I have quite some images from a Dutch book of children's games. Then **D. G. Emmerich**, who was a well known in France, and also in the UK/US, architectural theorist. He participated with text he made for the issue. And that's the difference, the one's I stole from other books, and the one's that were made for it.

[0042#t=00:05:27.395]
And, then there is a text by a Dutch art critic "Kreisen, Kreissegmenten und Wellenlinien usw." by **Van der Waals**. And again **Doets**, so Doets had two texts in it. And also Emmerich had also several ones. **Peter Schat** of course again from the Labyrinth Opera. Drawings by **Karl Pelgrom**, a Dutch artist with whom I had been exhibiting in Holland at that moment. And then again our friend **Mazman**, of whom we only know he had three fingers and was walking on a string (tightrope).

[0042#t=00:06:22.5]
[05.html#8/-0.879/5.941]


Then we start of course with the ring, well the ring around a town. And with an excerpt from a letter from **Luc d'Heusch** to **Pierre Alechinsky**. And I don't know... Ah he says "my ring is not in movement but is transformed into a chain," so that's how the letter starts, it's a sort of meta-physical sort of statement he is making to Pierre Alechinsky. And why he writes to him about this, I don't know, probably he was asked.

[0042#t=00:07:10]
[05.html#8/-0.887/6.848]

This article was made by Jorn, on the nordic "husdrapa", which is the Flag. Or what is "husdrapa"?    
*Husdrapa is a kind of drawing that you would have inside a house. And then Bards, poets, would use the image as the kind of theme on which they would create... they would look at the image and use it to generate stories and poetry.*

[0042#t=00:07:44.861]

And where can you find them? Because, this is not a Husdrapa...  
*No, there are no Husdrapa here.*  
So there's no illustration of the text.  
*It's a tradition where stories and texts would come from the image. In the beginning was the image.*
"In the beginning was the image", that's it! And then we have again the interlaced rings, which is not really topological. Which is funny as I took it from the former issue, and I mention it (look at number 3). That (symbol) Jorn has drawn, and also made the letters.

[0042#t=00:08:46.197]
[05.html#8/-0.852/8.836]

So this is an essay by Jorn.  
*Did you translate it?*  
No, he wrote it in English. I'm sure he wrote it in English. And then someone corrected it. I don't remember.  
*The text was also published in Danish in a Swedish magazine, *Paletten*.*  
That is why he says, "I was asked to what is Nordic about art", that's the *Paletten* text. I should have mentioned it. That's a lapsus. Thank you for mentioning it. I had it translated probably in Denmark. That's a pity it's not mentioned. And *Paletten* was at that moment a very good magazine, the only one I think.

[0042#t=00:10:14.911]
[05.html#8/-0.848/11.297]

This is a funny text about a bride next to an image which is the *Last Judgement*. There is a poem here about the bride, and one about the boat. And there are also all these games and text and poems.

And there we have the Jorn triolectical scheme, which is also used for the triolectical football game. There we go again. That's the Tarot disposition (layout). I don't know what I had with that, because I never really had anything with that.

Then we have this comparison sort of image of Sengaï and of Alechinsky with a triangle and an O which is Owing **?**

[0042#t=00:11:23.825]
[05.html#8/-0.871/13.828]

*This* has not been published anywhere, by Jorn. No, I think this one is made for the magazine. No? Am I wrong? 

*I know that it was published in Danish as well, but maybe it was published here first.*

He made it for me, for *The Situationist Times*, and there you have the color scheme -- that's the basis for the triolectical scheme.

[0042#t=00:12:09.592]
**cut** 
If you wish I could look it up ... 

[0042#t=00:12:59.543]
**cut**
It is quite a big book on actually quite a basic knowledge of color. These are the spiritual colors, this part, an dthen the physical. And what is very funny is nowhere the triolecical, well the drawing, of the colors.Jorns (puren?) form is base a lot on this. 

[0042#t=00:15:15.469]

There you see that I'm not very consequent (consistent) in my exact spaces of the way a page should be. Although I tried, but it doesn't always work out as I would like too. There are many mistakes...

[0042#t=00:15:44.820]
[05.html#8/-0.883/18.820]

There we have our **Tomaszewski** again who participated again wiht a new article. Which was also quite amazing.
And this I don't remember. That's tapestries, there are of course arabesque abstract rings and chains in arabic interlaces.

[0042#t=00:16:14.630]
[05.html#8/-0.887/21.297]

And here you go on with the interlaced ring, and of course I put this into his text, well *next* to his text, and made a kind of visual comparison to his forms. With again, a little poem from Danish 16th Century, "Sigvard and Brynhild".

[0042#t=00:16:47.018]
[05.html#8/-0.910/23.813]

And then a text about artificial rings. And there we go to all this, they are in the UK. There are lots of them in Ireland, this is in Cornwall. And then you get bones with rings, or holes. A hole is very often a ring. And it just goes on. What is a hole, and what is a ring? And these are shaman drums from Lappland. I mean it is again this comparative research on what is a hole and what is a ring.

[0042#t=00:17:27.189]
[05.html#8/-0.852/26.301]

... or what are rings. And they are from churches, and they are from bones. And they are from Ortohpedic rings... and rings around the sun and in the sky. I don't remember what this text is. The funny thing is it's in Dutch, and hardly ever have any Dutch texts. But I can't because it starts in German. So this text comes out of the blue! Aha... it's the past of East Europe by **Dr. Z.R. Dietrich**. Who the hell he is, I don't know. But he's talking of Eastern Europe. But these illustrations have nothing to do with that text, obviously. I think you ought to read that text (although it's in Dutch) to find out.

[0042#t=00:19:10.887]
[05.html#8/-0.867/28.805]

Funny things about slaves from Elba. And there was a big slavic, so not slaves like colonies, but Slavs and they had this sort of ways of living in a ring form, which well even Boy scouts there are tents in a round. There you can centralize. So the structure of roundabouts are in Lagoons, ways of constructing a town. The are atolls, Corelian atolls. This doesn't explain very much. And then it says it's about 5 or 6 km, diameter. So they are Lagerplaatsen (military compounds), where the soldiers were fenced in.

[0042#t=00:20:58.958]
[05.html#8/-0.871/29.379]

And then the next page, we go over to a mathematic analysis of a ring a spheric ring, which also is the eye. So there we go to the sphere and make a ring out of it. Then here we have illustrations of early rings, and with chains.

[0042#t=00:21:27.117]
[05.html#8/-0.898/30.887]

He's holding a ring, and there we get again, hey there the Homotopy comes again. And the homotopy of a sphere. So we still haven't really found out...shouldn't we find out what the homotopy is. Yes, the text is from Patterson, it's probably the same text I was using before? And then we have the Mexican ring, and suddently here is ... the Merovingian ring.

[0042#t=00:22:07.852]
[05.html#8/-0.895/31.805]

And then we have *Cosmogonies Annulaires* which is of course is the Cosmos. The yearly cosmos. Where you have a ring. But of course you have a ring everywhere. But then the ring is made by Jambû, Jambûdvîpa. and all sorts of other rings in the And it's from the Antiquity world by Mr. Pierre Gordon, written in 1949.

[0042#t=00:22:41.694]
[05.html#8/-0.906/33.340]
And here we get again the analysis of Jorn, this time on Imagination, Truth and Reality as oppositions. Always anti-dialectic, it's triolectic. And then is how to wear a ring, in the antiquities. In the Greek and Roman antique times, and the later in the Middle Age.

[0042#t=00:23:18.519]
[05.html#8/-0.914/34.328]
And then you get examples of rings, and then I illustrate with photos and here come little illustrations made by Max Bucaille.

[0042#t=00:23:29.013]
[05.html#8/-0.895/36.332]

So this text is by **Max Bucaille**, and it is of course an analysis in topology. And I chose the illustrations, partly it is his, and everything that is photos is just my illustration. And here we have a shower curtain ring in the middle of all this middle ages. So that's a bit the idea to have it mixed. The ring and its combinations.

[0042#t=00:24:09.235]
[05.html#8/-0.898/38.770]
Well here we get big rings seen from the side.

A ring around (Lee) Oswald when he's about to be shot by ... The ring in a newspaper around him. And here we have usual rings, but also elements. And here is the start of making a Moebius band, from Lietzmann.

[0042#t=00:25:17.782]
[05.html#8/-0.844/41.328]
So there we get more sort of art. Bram van Velde. Well whatever is around suddenly becomes a ring. Saint Francis, god knows why this should be in. And a circular Pollock, I mean everything is getting in. Pollock in retrospective, no idea why. A little Klee drawing. No idea.

[0042#t=00:25:59.287]
[05.html#8/-0.863/43.820]
And this is the origine and geneology of the ring, written by whom... Loeffler-Delachaux, "Le CERCLE, un symbole." One thing I never want to do, was have symbolic explanations. So I'm surprised that this is in it.

Then we have a German song from 1788, called "The broken small ring," very sad song.

[0042#t=00:26:38.411]
There is the Moebius, I just showed before the starting of the moebius. This I don't know... Well it might also be the moebius. It starts here, there, and here it comes. So these are rings. This one I still have downstairs. Don't know the reason why I kept it. At least there's one.

[0042#t=00:27:17.665]
[05.html#8/-0.836/46.293]
This is of course a story called in French, Peau d'Âne (Donkey skin), next to a ring ring.

[0042#t=00:27:38.054]
[05.html#8/-0.828/48.824]
That's the hopscotch, which we saw in the other issue with the photos of Spoerri. I this is called the "Forgotten Story of the Hopscotch," which is of course is made with chalk on the street. And that's a German investigation looking at a thousand years of hopscoth. And there you have the heaven and hell of the hopscotch. And then you have *Rock engravings* and *Rehabillitation*, and *Bridges*.

[0042#t=00:28:20.158]
[05.html#8/-0.855/51.313]
And here we go again, oh that's **Virtus Schade** who wrote this. And this is the counting of the hopscotch which is not completely made as a hopscotch. And this article at least says who translated it: Ella Bach Sørensen.

[0042#t=00:28:48.044]
[05.html#8/-0.887/53.781]
So here get to the rings around swords. Japanese swords have these rings around which are very beautifully engraved and designed. Then we get rings that are not completely closed, which is of course another form than the closed ring.

[0042#t=00:29:14.623]
[05.html#8/-0.848/56.328]

So there we get the closed ring again. And the ring road. And they have rings around their heads. And another of these images, it looks like a bottle...
*It's a Klein bottle.*

"A mathematician named Klein  
thought the Moebius band was divine.  
Said he, "If you glue  
The edges of two  
You'll get a weird botttle like mine."
That's afunny poem.

[0042#t=00:29:56.463]
And these are bracelets and rings.

This is from a film, famous film at that moment, *The Ten Commandments*. Don't know why.

[0042#t=00:30:10.579]
[05.html#8/-0.852/58.777]

Well, rings. rings. Not closed rings.

[0042#t=00:30:14.808]
[05.html#8/-0.883/61.305]

Two rings, they are put together, in handcuffs.

Footcuffs. Porno footcuffs. Rings chains, lots of rings, rings, rings, rings, even more rings. Really lots of rings.

[0042#t=00:30:42.233]
[05.html#8/-0.906/63.848]

Rings that make ears become rings. 

Then we have ringlegs. Ringleg is a ring play. Leg is Danish for play.

And chains, colliers.

[0042#t=00:31:08.725]
[05.html#8/-0.906/66.324]

Well it just goes on and on, I can't explain very much about it, look at all these funny images which have to do with rings. And they're getting more and more funny.

[0042#t=00:31:21.822]
[05.html#8/-0.902/68.793]

This is probably a ring where you... (gestures punch). It's interesting that I am making rings out of potatoes, I didn't know then.

[0042#t=00:31:34.213]
[05.html#8/-0.891/71.324]

And here it says the 100 lockings, the opening and closing of a key is probably also a ring.

[0042#t=00:32:05.814]
[05.html#8/-0.879/73.398]
And then we have again the "Small Ring". That's a famous song by Mahler, but that's long before Mahler. You can see rings wherever you look.

[0042#t=00:32:34.155]
[05.html#8/-0.879/74.309]
And this is the text by Bucaille, which is called "The infinite on the finger."

[0042#t=00:32:46.803]
[05.html#8/-0.898/76.313]

Then you have the van Koch, not van Gogh, curve, which is something mathematic.

[0042#t=00:33:01.493]
[05.html#8/-0.910/81.301]

And then we go on with rings. I mean if they are not on the arms then they are in the ears. Or around the neck.

[0042#t=00:33:22.964]
[05.html#8/-0.910/83.785]

And the bull has of course the ring in his nose. 

There we have a text from Kashmir. An Indian text called the "Refound Ring."

These statues have rings in their ears. All of them.

[0042#t=00:34:00.946]
[05.html#8/-0.891/86.305]

Well she has no rings, but funny tears in her eyes. Among rings, it's called.

[0042#t=00:34:19.019]
[05.html#8/-0.898/88.828]
There we have king ring, a kingdom for a ring... Boats with tires. Tires are rings.

It's not surprising that I wanted to make a wheel book after, because it's the same story. But I happy I didn't do it.

[0042#t=00:34:45]
**cut**

[0043#t=00:00:00.000]
[05.html#8/-0.906/91.324]

There at least is starts being a little bit different as a ring. And you might question me, why do you put all of this inside. And I don't remember, because it to me had the association with rings. Not with chains, obviously.

[0043#t=00:00:22.894]
[05.html#8/-0.887/93.840]

A circle around your waist is a ring too. Apparaently it's all these waist parts I used. Then you have the ring snake.

[0043#t=00:00:48.611]
[05.html#8/-0.879/96.305]

You're very quiet... Why don't you ask anything? Because it's just going on: Rings, rings, rings. Around your head, rings high in the sky, rings that became crowns.

[0043#t=00:01:05.696]
[05.html#8/-0.863/98.789]

Rings that become crowns in films. Like Gina Lollobrigida in *Tosca*, a Mexican statue with a ring on his head. And Orlando and his wife, the queen, apparently in a film, or perhaps a film of an opera. A lamp in a Gottland church. Well then all these lamps with candles (chandeliers).

[0043#t=00:01:42.868]
[05.html#8/-0.879/101.301]

Either they're on your head or in the sky.


[0043#t=00:01:48.343]
[05.html#8/-0.902/103.813]

And there I made the comparison between Christ and a Peruvian statue. This statue and that statue.

[0043#t=00:02:02.834]
[05.html#8/-0.902/106.316]


And that's Mr. Alf Lombard in Lund who's having a ring on his head. And they are have rings on their head. And he has a ring on his head and so they all have rings. And he's making rings by smoking and these stone age peopple with rings on their head. He's smoking, he is actually Smith, the then English prime minister. When smoking was still in you could (blow) smoke rings.


[0043#t=00:02:46.146]
[05.html#8/-0.902/108.820]
Glorie is smoking (blowing) lots of rings, and people are getting happy because it's Gloire, even the painter's happy! The rings around, in the graveyards, the same symbol that you have at Christmas (wreaths).

[0043#t=00:03:15.536]
[05.html#8/-0.867/111.301]
And then the dog here is sad because he also sees a sausage. It's quite nice this acquarel: The Commune-wall of all the people who were killed in Paris.

[0043#t=00:03:53.937]
[05.html#8/-0.906/113.805]
Of course there we have, what do you call them, the garlands that are around your head. And of course a cross with a ring.

[0043#t=00:04:12.926]
[05.html#8/-0.809/116.344]
And there they are playing with a ring. This is a text about Mysterious Rings and Circles (faery rings). Well this is also of course a ring, she puts a ring around herself. I don't know if I have hula-hoop, but probably it comes.

[0043#t=00:04:41.940]
[05.html#8/-0.813/118.797]
There we have cosmetic rings. There we have P.V. Glob  who's putting a ring around himself. She's making a ring around herself. I don't know what de Chirico's ring is, it's called "Melancholy".

[0043#t=00:05:01.217]
[05.html#8/-0.820/121.332]
There they are jumping through rings. They are playing with rings.

[0043#t=00:05:07.336]
[05.html#8/-0.828/123.801]

It's all the comparative games. THere's hula-hoop. Ididn't forget.Well lots of jugglers with rings.

[0043#t=00:05:18.975]
[05.html#8/-0.824/126.328]

Hula hoop. Games. Little statue which I know after the image I saw.

[0043#t=00:05:35.419]
[05.html#8/-0.816/128.801]
Here's a big game, the football game with rings. That's not a game with rings, it's just drawn with rings, that's interesting.

So there we go on that's from Yule River, from Australia, an erotic ring circle and phallus. Of course, it's also very much phallic, well not phallic it's very much a circle, the female sex.

[0043#t=00:06:14.096]
[05.html#8/-0.820/131.320]
Well there they have to go through this ring on horses. Pass through the ring. It's all the same: games, games, games. Ring riding and putting the ring with a spear.

[0043#t=00:06:45.903]
[05.html#8/-0.848/133.793]
I don't know what they're doing here. They're throwing a ball in a the ring, but that's a funny way, putting a ring like that and then getting the ball through. And here you can see here how they do it. And the King of Herodes flight to Egypt through a ring, that's a bit surprising. And that is Kandinsky, *The Spiritual in the Art*. And these are Norwegian wood carvings (rock engravings) on ring games.

[0043#t=00:07:35.336]
[05.html#8/-0.859/136.313]

There are lots of stone engravings, and some wood engravings. There you see again going through the hole. And here there are ring people with crosses and erections. And boats I imagine, but also with rings. Probably whatever I saw that was around, I just put in... that was circle-like.

[0043#t=00:08:11.787]
[05.html#8/-0.867/138.840]
There of course we have the snakes who are... The Lingam is the Indian and Nepalese fertility symbol, which is always an erection with a ring around. And then of course I make the comparison with the things that we have for the boats. Here are little lingams and these are lingams too. It's a statue.

[0043#t=00:09:05.031]
[05.html#8/-0.898/141.328]
All these are lingams, lingams in a temple in Kerala. And this guy is doing this, I don't know why he is going that. And again donkeys and rings. Funny thing is I can't make a round with this finger because this one is broken... just in case people are interested in actuality.

[0043#t=00:09:46.600]
[05.html#8/-0.895/143.844]
So what do we get here. Of course the Unicorn, which is in a way an erotic symbol, the erection and she has a ring in her hand. Then we have the fore finger which is burned in this case. And the Devil in a witch ring circle. Here the skaters are making rings. Here the fisher is making a ring of the fish he has been catching in the little hole which is probably also a circle. Here they're dancing the round which is also a ring.

[0043#t=00:10:36.287]
[05.html#8/-0.891/146.309]
There we get mountain tribesman hamlets ... "For Vietnamese mountain tribesmen, new strategic hamlets." So that's not even against the Americans, it's just originally. And that's a circle grave which we saw in Gotland and which are all over Scandinavia. There of course we get the towns and the circles. And here are the worker bees around the royal cell of the queen. These are muchroom rings, of course the poisonous muchrooms, red with white speckles, make rings. A ring mountain. A Finnish ring grave.

[0043#t=00:11:47.500]
[05.html#8/-0.906/148.871]

Now it goes on with graves. And I don't know where Pirlanda is, Bohuslän, must be in Sweden somewhere. That's all these grave rings. And this of course the military rings. And this is the literary, aha yes, that's so-called funny. The dog is not allowed in this grass ring so the policeman sends him out. Ha ha ha. And these are all round graves. And the Synkrotron is obviously a ring. These are towns in ring form, many medieval towns are in circle form.

[0043#t=00:12:56.439]
[05.html#8/-0.918/151.285]

This text, where is it from? It's from **Emmerich**. So he is trying to analyze the Triskele but then at once when he starts analyzing this he's making his own ying and yang. I had big discussions with him why ... he couldn't as an architect, or mainly as a theoretical architect, stick to the 3 faced (phased?) form, he had to have the opposition, the thesis / anti-thesis dielectic, all the time to get symmetry. I mean you can't get symmetry out of 3. So that was of course the theory and we had big discussions on that. 

[0043#t=00:13:53.941]
[05.html#8/-0.891/153.809]

Well there we get Lucca. We get again the towns which are in ring form. There is a sort of repetition from one to the other, but also not completely. Then we have the circus, the posters of the circus, Cirque du Montmartre. This is an image by Pol Bury but you can't really see it, a deformation of it.

[0043#t=00:14:25.215]
[05.html#8/-0.867/156.320]
Then this is the Oswald theory from the famous Oswald from van Gallen, Sankt Oswald but who was also Scandinavian.

[0043#t=00:14:42.751]
**cut**
in 70 Jorn made a book ... Diederich. Why did Oswald change into Diedrich? Because it started with Oswald. That's strange, it's Diederich. And I confused them because it was in Sant Galen that he started doing his research. What has Oswald have to do with Diedrich? Well read the book and we'll know probably.

[0043#t=00:15:34.838]
[05.html#8/-0.910/158.863]

Here is just one ring in all sorts of religious poses. Here is of course the Indian ring but very different from the ring around the Lingam, the phallus. 

[0043#t=00:15:56.988]
[05.html#8/-0.883/161.328]
So he's holding rings. They're all holding rings, all these guys. I don't know why, the ring has power apparently.

[0043#t=00:16:06.956]
[05.html#8/-0.883/163.867]
But as I don't want to explain, I can't explain. Even if I would have know, I wouldn't... 

[0043#t=00:16:15.473]
[05.html#8/-0.879/166.355]

You see all the time they are holding rings.
And his problem is that he has a very small ring for a very fat lady, obviously. He's holding a ring. I have to point out all the rings because sometimes you can hardly see them. There's a ring, there's a ring.

[0043#t=00:16:38.127]
[05.html#8/-0.875/168.836]
I just wanted to say, that one we have seen, but where are the rings? You remember in the museum in Gotland.  
*Well there are shields...*  
Yes there are shields but not ... this are all shields but he has a ring. See that's a shield, that's a ring. So if we look, well we can find the rings. That's a ring. That's a ring. A shield is closed. Yes there is something down there, rings on top of the churches. Something like that.

[0043#t=00:17:23.894]
[05.html#8/-0.887/171.359]

This Dubuffet has a tiny little ring there. That's Sherlock Holmes... looking through a (magnifying glass)... I think, I don't know, might be. Something ringish around here.

[0043#t=00:17:42.692]
[05.html#8/-0.883/173.836]

Then of course he's eating a ring. Doughnut. Ring cakes. Baking pancakes... they have to put a hole in it otherwise it's not allowed to be in here. Ring chains, Chain rings. Léger, what's that Ubac, yes. Engineering ring.

[0043#t=00:18:22.056]
[05.html#8/-0.883/176.316]

Ring ring ring ring ring, attachment ring. You have to put your head in a ring. Why is this here? It says that he is ridiculous? Ahhhhh, you see you have to think further, because these poor people will be put in these rings, that's the hanging ring (nooses) and there you see the hanging. Aha, it's not that foolish, sometimes.

[0043#t=00:19:09.883]
[05.html#8/-0.875/178.855]

Well... And there we have Galileo showing the moon, the way of the moon, and suddenly the moon becomes a ring which of course the moon sometimes does. And these are letters (de)formed by people in the form of an O. And then of course we have the Japanese or Chinese rings. And this is Edgar Tytgat. And we are at the O's. Even the eggs become O's.

[0043#t=00:19:49.884]
[05.html#8/-0.871/181.387]
O.

[0043#t=00:20:01.739]
[05.html#8/-0.902/183.766]
Soto, the artist Soto we put it. Then we have all the different deformations of rings, concentric circles, that Pol Bury changes. Deformation of rings. (Cinétisation)

[0043#t=00:20:21.029]
[05.html#8/-0.863/186.359]
Also, deformation by **Pol Bury**. Interesting... no idea. 

[0043#t=00:20:30.651]
He was a friend of yours... **cut**

[0043#t=00:21:08.601]
[05.html#8/-0.879/188.789]
These are the tree rings.

[0043#t=00:21:21.815]
[05.html#8/-0.871/191.324]
You have the temple ring. This is called circle (Il y a cercle et cercle). He's gambling money and then drowning. This might be a hole by gunshot. This is magnetic fields that make rings. 

[0043#t=00:22:04.543]
[05.html#8/-0.891/193.820]

These people are holding discs. And these are of course upper hell and lower hell. Rings, rings everywhere.

[0043#t=00:22:20.847]
[05.html#8/-0.879/196.301]

Shooting rings, Painter Maryan I put it because he had this... and again not Nikki de Saint-Phalle and not the situationists.

[0043#t=00:22:37.299]
[05.html#8/-0.887/198.828]

Ah that's the **Van der Waals**. He got very angry with me about this. That's the text he made and apparently I didn't do it OK, or the illustrations were not to his wish (liking). He sent me a very nasty letter saying that he was going to sue me because of having printed the text the way I had. And actually, you might Christine Koonigs the other day, and he's married to her aunt, so his wife is also a Koonigs. And this Var der Waals was an art critic. ...

[0043#t=00:23:39.503]
[05.html#8/-0.887/201.305]

Gets more and more weird. I don't remember what the text was about but I remember the fight we had.

[0043#t=00:23:48.725]
[05.html#8/-0.910/203.785]

*And he wrote it for you?*  
Yes, that's why he was angry probably. And he wrote about my work at some moment.

[0043#t=00:24:09.844]
Now there's not much about chains. Here is the Van der Waals, and the bibliography for his text. So probably these images are chosen by him, I'm sure he didn't like the way I put them. It must have been something like that.

[0043#t=00:24:37.997]
[05.html#8/-0.875/206.203]
There we get a little more chain like. Because I'm missing chains until now.

[0043#t=00:24:42.673]
[05.html#8/-0.910/208.746]
Are we getting chains? Yes, sort of. Who's text is this. It's an English one.

[0043#t=00:25:00.126]
[05.html#8/-0.879/211.211]
There it becomes German, and there topological.
On Danish graveyards, you very often have three interlaced chains. You see? very very often, I found out when I was in Copenhagen making this.

[0043#t=00:25:35.849]
[05.html#8/-0.883/213.742]
And this is a very beautiful text by **Nathan der Weise**, about the three chains of religion and interlacing. So I just published the whole text.

[0043#t=00:26:07.009]
[05.html#8/-0.883/216.258]
And there is the Saga of the Gunnlaug Serpant who had a double tongue.

And here are all sorts of worms, of course they have a chain structure.

[0043#t=00:26:34.210]
[05.html#8/-0.887/218.766]
Then we get interlaced patterns on different things, with interlaced chains -- not one round but different rounds interlaced together.

[0043#t=00:26:55.796]
[05.html#8/-0.859/221.293]
So, apparently this is a very much interlaced brain. And here's also, all the time it's not just one round, but the many rounds which are interlacing patterns.

[0043#t=00:27:12.820]
[05.html#8/-0.859/223.754]
Again, hardly visible, with of course small topological texts.

[0043#t=00:27:23.722]
[05.html#8/-0.871/226.246]
This is a labyrinth and a chain, is that not from Gotland, we have seen this. I think that one was also, that's **Franceschi** photos I used.

[0043#t=00:27:43.875]
[05.html#8/-0.879/228.340]

And then we get chains. I mean there's not so much. Of course you could say chain reaction. Postmark with olympic chains. There's this Indian goddess.

[0043#t=00:28:17.791]
[05.html#8/-0.840/229.199]
And then we have "Some mathematical aspects." Who is this? **Doets?** So that's very serious. 
*Did you invite him to write this?*
Yes.
*Was he living in Amsterdam?*
Yes. He still is, or in Utrecht now.

[0043#t=00:28:51.950]
[05.html#8/-0.859/236.273]

Here the chains come from Léger. Doets became really a professor of Mathematics, he wasn't a student then. He doesn't want anything to do with this, it's the past he says. 
*How did you get to know him?* 
I really don't remember.

[0043#t=00:29:24.665]
Well there you see the guy is ... chains. And then of course you have the chain saw.

[0043#t=00:29:37.406]
[05.html#8/-0.859/238.727]

These are very chained women. And this guy has a cup with a chain.

[0043#t=00:29:55.991]
[05.html#8/-0.867/241.219]
And there the Catholic apostolate is chained. Chains in the middle ages. And the swastika and cross on a chain. From a film.

[0043#t=00:30:33.512]
[05.html#8/-0.848/243.777]

Here's comes an analysis by **Emmerich**.

[0043#t=00:30:39.411]
[05.html#8/-0.852/248.820]

There you get the moebius and the chains in the models he is using.  
*And this is also made for the magazine?*  
It's made for the magaine, yes.

[0043#t=00:30:49.390]
[05.html#8/-0.883/251.250]
And of course then you get getting out of a chain, the jugglers, the prisoners.

[0043#t=00:31:06.033]
[05.html#8/-0.898/253.809]
Not getting out of a chain...

[0043#t=00:31:15.284]
[05.html#8/-0.875/256.266]
There you have the Uppland stone. Ubu, pataphysics unchained.

[0043#t=00:31:34.679]
[05.html#8/-0.875/258.746]
Well, Frankenstein chained. Chained people. He starts with a small chain, then he becomes corrupt and gets a bigger chain, then he ends up chained, very symbolic.

[0043#t=00:31:57.586]
[05.html#8/-0.871/261.242]

And then there's apparently a story "La Vénus d'Ille." I don't know by whom, about very interesting .... no idea. 

[0043#t=00:32:14.196]
[05.html#8/-0.891/266.875]
There you have a complicated chain... and another...

[0043#t=00:32:19.695]
[05.html#8/-0.859/268.719]
And then it ends with a tiny little bit of music by **Schat** (just a bit, I got fed up after the opera)... but still there is something in it. 

[0043#t=00:32:36.269]
[05.html#8/-0.863/271.273]
And the drawings by **Karl Pelgrom** which I think is quite funny.

[0043#t=00:32:41.723]
[05.html#8/-0.848/273.820]

And this is by ... well we look in the back. That's a Fontana. Of course, this has not very much to do with the chain.

[0043#t=00:32:55.046]
[05.html#8/-0.824/276.254]

And this is **Mazman**, "No Happy returns for me." Probably he got chained. And this is of course a tractor with a chain.

[0043#t=00:33:04.682]
[05.html#8/-0.789/278.234]

Now we have a look on the last part...

[0043#t=00:33:11.425]
[05.html#10/-0.6201/4.1875]
No Happy Return, oh **Jim Ryan**. "Forms and drawings." That's again the sort of thing... Mérimée, it's the Vénus d'Ille, it's a very well known story. The Nathan story is by Lessing. These are famous stories I took in to have literature I guess.

And these are again what I mentioned before, just arbitrary friends who did things, proposed and then I couldn't say no.

[0043#t=00:34:01.326]

And then, the most important thing of the whole issue is the Editor's Note. And then I put a real statement. That's the first and the last time. Should I read it?

[0043#t=00:34:23.103]
[05.html#10/-1.3232/278.2451]

"In this number of the Situationist Times (5) do we try to open up the problem of the ring, interlaced rings and consequently, chains."

[0043#t=00:34:32.722]
**cut**

[0044#t=00:00:17.121]

"This happens on the base of the three patterns of European culture which are introduced in Jorn's article on "Meaning and Sense". (So that article was written for the magazine.) 
No. 3 of the Situationist Times dealt with the "interlaced" pattern of "overlapping situations".  
No. 4 deals with the "ring and or chain" pattern, which is most clearly found in the Olympic rings and in Lenin's statement (see page 3-5) (which is in our opinion the Byzanthine and Russian pattern (see also page 3-5)).  
We do not want to make any statements about the three patterns neither in a so called "symbolistic" way nor in a scientific one, even though some of the contributions might show a certain tendency in one way or another.  
All we try to show here in an artistic way is a certain connection of the three patterns with topological aspects. It is up to the reader if he wants so, to make his conclusions."

[0044#t=00:01:44.784]

Now there is a funny thing. I don't know if you remarked it, I'm talking about number 4 deals with rings and chains, it supposed to be number 5.

[0044#t=00:01:55.530]
**cut** Also this has never been corrected.  I say here: "All we try to show here in an artistic way is a certain connection of the three patterns with topological aspects. It is up to the reader if he wants so, to make his conclusions" But this is in a way a little non-sensic, the way that I put it. No? 

[0044#t=00:02:59.587]
*This is almost like a manifesto.*  
Yes, it is a manifesto.


[0044#t=00:03:06.372]
**cut**

[0042]: ../video/2017-12-16/MVI_0042.web.mp4 "video"
[0043]: ../video/2017-12-16/MVI_0043.web.mp4 "video"

[0044]: ../video/2017-12-16/MVI_0044.web.mp4 "video"
[0045]: ../video/2017-12-16/MVI_0045.web.mp4 "video"

[05.html]: ../05/scans.html "scans"

> Letter: Luc d'Heusch  
Mind and sense -- Asger Jorn  
Der kleine Bootsmann (from Danish poem 17th cent)  
Art and order -- Asger Jorn  
Regular forms of closed non-orientable surfaces -- Lech Tomaszewski  
From: "Het verleden van Oost-Europa"  Dr. Z. R. Diettrich  
From: Patterson -- Topology  
Cosmogonie annulaires -- Max Bucaille  
Port d'Anneau -- Max Bucaille  
Structure d'Anneau -- Max Bucaille  
Von den Polyeder zu den gekrümmten Flächen -- Prof. W. Lietzmann  
Origines et généologie d'Anneau -- Max Bucaille  
Forgotten knowledge of the Universe in the children's hopscotch  
Virtus Schade  
L'infini du doigt -- Max Bucaille  
L'Anneau retrouvé (conte indient)  
Cercels mysterieux -- Max Bucaille  
Ringsteken en Ringrijden (Kinderspelen)  
Noeds et dénouements -- D. G. Emmerich  
Die Legende des Heiligen Oswalk  
"Cinétisations" -- Pol Bury  
What goes up still goes down -- Dr. Narlikar & Prof. Fred Hoyle  
Kreisen, Kreissegmenten und Wellenlinien usw. -- F. van der Waals  
Die Parabel der 3 Ringe (Nathan der Weise) -- Lessing  
Some mathematical aspects -- H. C. Doets  
Tournures -- D. G. Emmerich  
Venus de l'île -- Mérimée  
From: Opera "The Labyrinth" -- Peter Schat  
Drawing -- Karl Pelgrom  
Poems and drawings -- Jim Ryan  
No happy returns for me -- E. Mazman
