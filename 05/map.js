function init_map (max_zoom, abovepages, layoutpages) {

var cell_width = 320,
    cell_height = 209;

if (abovepages === undefined) { abovepages = [] }
 
var map = L.map('map', {
        maxZoom: max_zoom || 9,
        minZoom: 0,
        zoom: 0,
        crs: L.CRS.Simple,
        center: new L.LatLng(0,0)
});


var markers = [
  {
    "y": -0.859,
    "id": "t1",
    "x": 0.75,
    "zoom": 8
  },
  {
    "y": -0.867,
    "id": "t4",
    "x": 280.797,
    "zoom": 8
  },
  {
    "y": -0.836,
    "id": "t5",
    "x": 3.203,
    "zoom": 8
  },
  {
    "y": -0.91,
    "id": "t6",
    "x": 4.355,
    "zoom": 8
  },
  {
    "y": -0.859,
    "id": "t7",
    "x": 279.367,
    "zoom": 8
  },
  {
    "y": -1.082,
    "id": "t8",
    "x": 4.363,
    "zoom": 8
  },
  {
    "y": -0.879,
    "id": "t12",
    "x": 5.941,
    "zoom": 8
  },
  {
    "y": -0.887,
    "id": "t13",
    "x": 6.848,
    "zoom": 8
  },
  {
    "y": -0.852,
    "id": "t15",
    "x": 8.836,
    "zoom": 8
  },
  {
    "y": -0.848,
    "id": "t16",
    "x": 11.297,
    "zoom": 8
  },
  {
    "y": -0.871,
    "id": "t17",
    "x": 13.828,
    "zoom": 8
  },
  {
    "y": -0.883,
    "id": "t21",
    "x": 18.82,
    "zoom": 8
  },
  {
    "y": -0.887,
    "id": "t22",
    "x": 21.297,
    "zoom": 8
  },
  {
    "y": -0.91,
    "id": "t23",
    "x": 23.813,
    "zoom": 8
  },
  {
    "y": -0.852,
    "id": "t24",
    "x": 26.301,
    "zoom": 8
  },
  {
    "y": -0.867,
    "id": "t25",
    "x": 28.805,
    "zoom": 8
  },
  {
    "y": -0.871,
    "id": "t26",
    "x": 29.379,
    "zoom": 8
  },
  {
    "y": -0.898,
    "id": "t27",
    "x": 30.887,
    "zoom": 8
  },
  {
    "y": -0.895,
    "id": "t28",
    "x": 31.805,
    "zoom": 8
  },
  {
    "y": -0.906,
    "id": "t29",
    "x": 33.34,
    "zoom": 8
  },
  {
    "y": -0.914,
    "id": "t30",
    "x": 34.328,
    "zoom": 8
  },
  {
    "y": -0.895,
    "id": "t31",
    "x": 36.332,
    "zoom": 8
  },
  {
    "y": -0.898,
    "id": "t32",
    "x": 38.77,
    "zoom": 8
  },
  {
    "y": -0.844,
    "id": "t33",
    "x": 41.328,
    "zoom": 8
  },
  {
    "y": -0.863,
    "id": "t34",
    "x": 43.82,
    "zoom": 8
  },
  {
    "y": -0.836,
    "id": "t36",
    "x": 46.293,
    "zoom": 8
  },
  {
    "y": -0.828,
    "id": "t37",
    "x": 48.824,
    "zoom": 8
  },
  {
    "y": -0.855,
    "id": "t38",
    "x": 51.313,
    "zoom": 8
  },
  {
    "y": -0.887,
    "id": "t39",
    "x": 53.781,
    "zoom": 8
  },
  {
    "y": -0.848,
    "id": "t40",
    "x": 56.328,
    "zoom": 8
  },
  {
    "y": -0.852,
    "id": "t42",
    "x": 58.777,
    "zoom": 8
  },
  {
    "y": -0.883,
    "id": "t43",
    "x": 61.305,
    "zoom": 8
  },
  {
    "y": -0.906,
    "id": "t44",
    "x": 63.848,
    "zoom": 8
  },
  {
    "y": -0.906,
    "id": "t45",
    "x": 66.324,
    "zoom": 8
  },
  {
    "y": -0.902,
    "id": "t46",
    "x": 68.793,
    "zoom": 8
  },
  {
    "y": -0.891,
    "id": "t47",
    "x": 71.324,
    "zoom": 8
  },
  {
    "y": -0.879,
    "id": "t48",
    "x": 73.398,
    "zoom": 8
  },
  {
    "y": -0.879,
    "id": "t49",
    "x": 74.309,
    "zoom": 8
  },
  {
    "y": -0.898,
    "id": "t50",
    "x": 76.313,
    "zoom": 8
  },
  {
    "y": -0.91,
    "id": "t51",
    "x": 81.301,
    "zoom": 8
  },
  {
    "y": -0.91,
    "id": "t52",
    "x": 83.785,
    "zoom": 8
  },
  {
    "y": -0.891,
    "id": "t53",
    "x": 86.305,
    "zoom": 8
  },
  {
    "y": -0.898,
    "id": "t54",
    "x": 88.828,
    "zoom": 8
  },
  {
    "y": -0.906,
    "id": "t56",
    "x": 91.324,
    "zoom": 8
  },
  {
    "y": -0.887,
    "id": "t57",
    "x": 93.84,
    "zoom": 8
  },
  {
    "y": -0.879,
    "id": "t58",
    "x": 96.305,
    "zoom": 8
  },
  {
    "y": -0.863,
    "id": "t59",
    "x": 98.789,
    "zoom": 8
  },
  {
    "y": -0.879,
    "id": "t60",
    "x": 101.301,
    "zoom": 8
  },
  {
    "y": -0.902,
    "id": "t61",
    "x": 103.813,
    "zoom": 8
  },
  {
    "y": -0.902,
    "id": "t62",
    "x": 106.316,
    "zoom": 8
  },
  {
    "y": -0.902,
    "id": "t63",
    "x": 108.82,
    "zoom": 8
  },
  {
    "y": -0.867,
    "id": "t64",
    "x": 111.301,
    "zoom": 8
  },
  {
    "y": -0.906,
    "id": "t65",
    "x": 113.805,
    "zoom": 8
  },
  {
    "y": -0.809,
    "id": "t66",
    "x": 116.344,
    "zoom": 8
  },
  {
    "y": -0.813,
    "id": "t67",
    "x": 118.797,
    "zoom": 8
  },
  {
    "y": -0.82,
    "id": "t68",
    "x": 121.332,
    "zoom": 8
  },
  {
    "y": -0.828,
    "id": "t69",
    "x": 123.801,
    "zoom": 8
  },
  {
    "y": -0.824,
    "id": "t70",
    "x": 126.328,
    "zoom": 8
  },
  {
    "y": -0.816,
    "id": "t71",
    "x": 128.801,
    "zoom": 8
  },
  {
    "y": -0.82,
    "id": "t72",
    "x": 131.32,
    "zoom": 8
  },
  {
    "y": -0.848,
    "id": "t73",
    "x": 133.793,
    "zoom": 8
  },
  {
    "y": -0.859,
    "id": "t74",
    "x": 136.313,
    "zoom": 8
  },
  {
    "y": -0.867,
    "id": "t75",
    "x": 138.84,
    "zoom": 8
  },
  {
    "y": -0.898,
    "id": "t76",
    "x": 141.328,
    "zoom": 8
  },
  {
    "y": -0.895,
    "id": "t77",
    "x": 143.844,
    "zoom": 8
  },
  {
    "y": -0.891,
    "id": "t78",
    "x": 146.309,
    "zoom": 8
  },
  {
    "y": -0.906,
    "id": "t79",
    "x": 148.871,
    "zoom": 8
  },
  {
    "y": -0.918,
    "id": "t80",
    "x": 151.285,
    "zoom": 8
  },
  {
    "y": -0.891,
    "id": "t81",
    "x": 153.809,
    "zoom": 8
  },
  {
    "y": -0.867,
    "id": "t82",
    "x": 156.32,
    "zoom": 8
  },
  {
    "y": -0.91,
    "id": "t84",
    "x": 158.863,
    "zoom": 8
  },
  {
    "y": -0.883,
    "id": "t85",
    "x": 161.328,
    "zoom": 8
  },
  {
    "y": -0.883,
    "id": "t86",
    "x": 163.867,
    "zoom": 8
  },
  {
    "y": -0.879,
    "id": "t87",
    "x": 166.355,
    "zoom": 8
  },
  {
    "y": -0.875,
    "id": "t88",
    "x": 168.836,
    "zoom": 8
  },
  {
    "y": -0.887,
    "id": "t89",
    "x": 171.359,
    "zoom": 8
  },
  {
    "y": -0.883,
    "id": "t90",
    "x": 173.836,
    "zoom": 8
  },
  {
    "y": -0.883,
    "id": "t91",
    "x": 176.316,
    "zoom": 8
  },
  {
    "y": -0.875,
    "id": "t92",
    "x": 178.855,
    "zoom": 8
  },
  {
    "y": -0.871,
    "id": "t93",
    "x": 181.387,
    "zoom": 8
  },
  {
    "y": -0.902,
    "id": "t94",
    "x": 183.766,
    "zoom": 8
  },
  {
    "y": -0.863,
    "id": "t95",
    "x": 186.359,
    "zoom": 8
  },
  {
    "y": -0.879,
    "id": "t97",
    "x": 188.789,
    "zoom": 8
  },
  {
    "y": -0.871,
    "id": "t98",
    "x": 191.324,
    "zoom": 8
  },
  {
    "y": -0.891,
    "id": "t99",
    "x": 193.82,
    "zoom": 8
  },
  {
    "y": -0.879,
    "id": "t100",
    "x": 196.301,
    "zoom": 8
  },
  {
    "y": -0.887,
    "id": "t101",
    "x": 198.828,
    "zoom": 8
  },
  {
    "y": -0.887,
    "id": "t102",
    "x": 201.305,
    "zoom": 8
  },
  {
    "y": -0.91,
    "id": "t103",
    "x": 203.785,
    "zoom": 8
  },
  {
    "y": -0.875,
    "id": "t105",
    "x": 206.203,
    "zoom": 8
  },
  {
    "y": -0.91,
    "id": "t106",
    "x": 208.746,
    "zoom": 8
  },
  {
    "y": -0.879,
    "id": "t107",
    "x": 211.211,
    "zoom": 8
  },
  {
    "y": -0.883,
    "id": "t108",
    "x": 213.742,
    "zoom": 8
  },
  {
    "y": -0.883,
    "id": "t109",
    "x": 216.258,
    "zoom": 8
  },
  {
    "y": -0.887,
    "id": "t110",
    "x": 218.766,
    "zoom": 8
  },
  {
    "y": -0.859,
    "id": "t111",
    "x": 221.293,
    "zoom": 8
  },
  {
    "y": -0.859,
    "id": "t112",
    "x": 223.754,
    "zoom": 8
  },
  {
    "y": -0.871,
    "id": "t113",
    "x": 226.246,
    "zoom": 8
  },
  {
    "y": -0.879,
    "id": "t114",
    "x": 228.34,
    "zoom": 8
  },
  {
    "y": -0.84,
    "id": "t115",
    "x": 229.199,
    "zoom": 8
  },
  {
    "y": -0.859,
    "id": "t116",
    "x": 236.273,
    "zoom": 8
  },
  {
    "y": -0.859,
    "id": "t118",
    "x": 238.727,
    "zoom": 8
  },
  {
    "y": -0.867,
    "id": "t119",
    "x": 241.219,
    "zoom": 8
  },
  {
    "y": -0.848,
    "id": "t120",
    "x": 243.777,
    "zoom": 8
  },
  {
    "y": -0.852,
    "id": "t121",
    "x": 248.82,
    "zoom": 8
  },
  {
    "y": -0.883,
    "id": "t122",
    "x": 251.25,
    "zoom": 8
  },
  {
    "y": -0.898,
    "id": "t123",
    "x": 253.809,
    "zoom": 8
  },
  {
    "y": -0.875,
    "id": "t124",
    "x": 256.266,
    "zoom": 8
  },
  {
    "y": -0.875,
    "id": "t125",
    "x": 258.746,
    "zoom": 8
  },
  {
    "y": -0.871,
    "id": "t126",
    "x": 261.242,
    "zoom": 8
  },
  {
    "y": -0.891,
    "id": "t127",
    "x": 266.875,
    "zoom": 8
  },
  {
    "y": -0.859,
    "id": "t128",
    "x": 268.719,
    "zoom": 8
  },
  {
    "y": -0.863,
    "id": "t129",
    "x": 271.273,
    "zoom": 8
  },
  {
    "y": -0.848,
    "id": "t130",
    "x": 273.82,
    "zoom": 8
  },
  {
    "y": -0.824,
    "id": "t131",
    "x": 276.254,
    "zoom": 8
  },
  {
    "y": -0.789,
    "id": "t132",
    "x": 278.234,
    "zoom": 8
  },
  {
    "y": -0.6201,
    "id": "t133",
    "x": 4.1875,
    "zoom": 10
  },
  {
    "y": -1.3232,
    "id": "t135",
    "x": 278.2451,
    "zoom": 10
  }
];
// console.log("[map].markers", markers);
var allmarkers = [];
markers.forEach(function (m) {
    var marker = L.marker([m.y, m.x]).on("click", function (e) {
        //console.log("marker click", m.id, e, this)
        window.parent.postMessage({msg: "mapclick", id: m.id}, "*")
    });
    allmarkers.push(marker);
})
var markers_layer = L.layerGroup(allmarkers);

var request = new XMLHttpRequest();
request.open('GET', 'tiles.json', true);

var hash = new L.Hash(map);

function is_above_page (item) {
    var m = /^_DSC(\d+)\.png/.exec(item.name);
    if (m) {
        var pn = parseInt(m[1]),
            pni = abovepages.indexOf(pn);
        // console.log("AP", pn, pni);
        return (abovepages.indexOf(pn) != -1)
    }
}

request.onload = function() {
  if (request.status >= 200 && request.status < 400) {
    // Success!
    var data = JSON.parse(request.responseText);
    // console.log("data", data);
    var pelts = [];
    var above_spread = false;
    var x = 0,
        y = 0,
        items = data['@graph'];
    for (var i=0, l=items.length; i<l; i++) {
        var item = items[i],
            m = /^_DSC(\d+)\.png/.exec(item.name),
            itemname = m ? m[1] : '';

        if (layoutpages && layoutpages[itemname]) {
            x = layoutpages[itemname].x;
            y = layoutpages[itemname].y;
            // console.log("using layoutpages, placing", itemname, "at", x, y);
            pelts.push({x: x, y: y, item: items[i]});
            continue
        }
        if (is_above_page(items[i])) {
            // above_spread = true;
            y = 0;
            x -= 1;
        } else {
            y = 1;
        }
        // pelts.push({x: i, y: 0, item: items[i]});
        // console.log("placing", items[i].name, "at", x, y);
        pelts.push({x: x, y: y, item: items[i]});
        x += 1;
        // if (i == 1) break;
    }
    var vt = leafygal.layout(pelts, undefined, undefined, undefined, undefined, undefined, cell_width, cell_height),
        layer = leafygal.gridlayer(L, vt, {tileSize: L.point(cell_width, cell_height)});
    map.addLayer(layer);

    map.addLayer(markers_layer);
    L.control.layers(null, { "Show links": markers_layer }).addTo(map);


  } else {
    console.log("server ERROR");
  }
};
request.onerror = function() {
  console.log("connection ERROR");
};
request.send();


}
