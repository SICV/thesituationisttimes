1
00:00:01,695 --> 00:00:08,605
An industrialist from Twente surrounds himself after the war with young, living art.

2
00:00:08,605 --> 00:00:12,175
With fierce, rebellious artists.

3
00:00:12,175 --> 00:00:17,578
Such as Karel Appel, Willem de Kooning and the Dane Asger Jorn.

4
00:00:17,578 --> 00:00:21,977
A daughter grows up in the midst of all this.

5
00:00:21,977 --> 00:00:24,808
Still a child at the foundation of Cobra in 1948.

6
00:00:24,808 --> 00:00:30,465
Only 20 when she herself flees the tame Netherlands for Paris, bitten by Cobra.

7
00:00:30,465 --> 00:00:42,744
A painter, and a close friend of Jorn in particular.

8
00:00:42,744 --> 00:01:11,395
Jacqueline and the Situationists

9
00:01:11,395 --> 00:01:12,897
Yes, but I used to play rather in the garden.

10
00:01:12,897 --> 00:01:15,846
I didn’t particularly run.

11
00:01:15,846 --> 00:01:18,573
Digging up treasures and such which we went looking for in the morning.

12
00:01:18,573 --> 00:01:25,610
We buried huge treasures. And I had a friend who lived next door and we did crazy things in the garden.

13
00:01:25,610 --> 00:01:28,174
That’s not really running. It’s more like playing and stuff.

14
00:01:28,174 --> 00:01:32,227
This made my father terribly angry because the lawn this and the lawn that …

15
00:01:32,227 --> 00:01:35,744
But of course we didn’t bother about that at all.

16
00:01:35,744 --> 00:01:37,268
When I was ten or so …

17
00:01:37,268 --> 00:01:43,900
Now look, in my case I just didn’t see my father from my third year

18
00:01:43,900 --> 00:01:54,000
until after after the war, when I was six or seven – no, seven.

19
00:01:54,000 --> 00:01:57,653
And I had a kind of insane ideal of my father.

20
00:01:57,653 --> 00:02:00,785
I found him sweet and terribly authoritarian,

21
00:02:00,785 --> 00:02:03,825
but I thought that was part of it, that’s how fathers were.

22
00:02:03,825 --> 00:02:09,147
But I believe that I hardly had any contact with him until later,

23
00:02:09,147 --> 00:02:12,953
until my fifteenth birthday more or less.

24
00:02:12,953 --> 00:02:17,256
Not that I can really remember. It was just a part of it.

25
00:02:17,256 --> 00:02:20,632
Fathers and mothers, that was just part of it.

26
00:02:20,632 --> 00:02:19,944
Like a garden you play in.

27
00:02:19,944 --> 00:02:23,265
It wasn’t anything important or impressive.

28
00:02:23,265 --> 00:02:30,796
He also slapped me an awful lot.

29
00:02:30,796 --> 00:02:34,976
Really a lot, and very probably rightly so.

30
00:02:34,976 --> 00:02:37,159
But that doesn’t matter. He just did it.

31
00:02:37,159 --> 00:02:40,315
And I see now that it was a part of him,

32
00:02:40,315 --> 00:02:45,487
that it was a manifestation of him.

33
00:02:45,487 --> 00:02:48,736
Father, say something.

34
00:02:48,736 --> 00:02:53,553
The father sees the daughter as an adolescent,

35
00:02:53,553 --> 00:02:57,326
as the child with the unavoidable situations

36
00:02:57,326 --> 00:03:00,560
such as they are here then.

37
00:03:00,560 --> 00:03:02,662
Going to school, coming home, going to school.

38
00:03:02,662 --> 00:03:05,749
At which the father, because of his occupations,

39
00:03:05,749 --> 00:03:08,506
is only ever present briefly, and always asks the same questions.

40
00:03:08,506 --> 00:03:12,158
How was it? That’s school for you.

41
00:03:12,158 --> 00:03:14,431
What grades did you get?

42
00:03:14,431 --> 00:03:18,412
And in fact always those simplistic questions that were asked.

43
00:03:18,412 --> 00:03:25,958
And in fact later, when Jacqueline no longer went to school,

44
00:03:25,958 --> 00:03:32,143
when she went abroad, thanks to the distance,

45
00:03:32,143 --> 00:03:33,506
a very different relationship in fact automatically came about.

46
00:03:33,506 --> 00:03:39,902
With Jorn, Armando and Constant we arrive at the Situationists.

47
00:03:39,902 --> 00:03:45,832
At age 20 you were already on the central committee of an international artist group,

48
00:03:45,832 --> 00:03:51,900
which connects the teachings of Marx and Lenin with that of the playful creative person.

49
00:03:51,900 --> 00:03:53,295
A continuation of surrealism.

50
00:03:53,295 --> 00:03:56,423
A precursor of Provo, Kabouter,

51
00:03:56,423 --> 00:04:25,377
reflected in Constant’s city of the future, New Babylon.

52
00:04:25,377 --> 00:04:29,941
Jacqueline, that seat you just sat down on, what is it called?

53
00:04:29,941 --> 00:04:33,752
This one is called a ‘Topozak’.

54
00:04:33,752 --> 00:04:37,750
It was made by Hans Brinkman in reaction to the normal beanbag

55
00:04:37,750 --> 00:04:45,845
that is opaque, made of leather or imitation leather and such,

56
00:04:45,845 --> 00:04:48,688
so where you don’t see what is happening in it

57
00:04:48,688 --> 00:04:51,811
when you sit or lie down on it or whatever you do with it.

58
00:04:51,811 --> 00:04:54,622
And because with this one you see exactly what is happening inside it,

59
00:04:54,622 --> 00:05:03,318
around it, he called it ‘Topozak’ (Topo bag),

60
00:05:03,318 --> 00:05:05,239
by analogy with ‘topology’.

61
00:05:05,239 --> 00:05:08,968
Because it is a permanent form that changes constantly.

62
00:05:08,968 --> 00:05:13,877
And which you experience during the change

63
00:05:13,877 --> 00:05:15,860
and which you can change back to other forms and so on.

64
00:05:15,860 --> 00:05:20,401
But it remains a single existing form. Changeable.

65
00:05:20,401 --> 00:05:23,719
So that is an aspect of what you call ‘topology’.

66
00:05:23,719 --> 00:05:27,899
How would you describe ‘topology’ and how did you arrive at it?

67
00:05:27,899 --> 00:05:31,837
Well, in the first place, how I arrived at it …

68
00:05:31,837 --> 00:05:35,470
It just exists. It is simply an existing science.

69
00:05:35,470 --> 00:05:42,978
But at one point, during the Situationist movement,

70
00:05:42,978 --> 00:05:47,470
Jorn discovered topology.

71
00:05:47,470 --> 00:05:51,328
So we’ve now arrived at the Situationists,

72
00:05:51,328 --> 00:05:55,583
and it seems then that the concept of Situationism came about

73
00:05:55,583 --> 00:05:58,404
through contact with topology.

74
00:05:58,404 --> 00:06:02,380
Do you see a transition from ‘topos’ (place)

75
00:06:02,380 --> 00:06:08,879
to the notion of ‘situation’?

76
00:06:08,879 --> 00:06:12,221
How did you arrive at the Situationists?

77
00:06:12,221 --> 00:06:16,226
How old were you?

78
00:06:16,226 --> 00:06:19,927
20, so that was in 1959.

79
00:06:19,927 --> 00:06:24,693
It happened … First of all, I had met Jorn.

80
00:06:24,693 --> 00:06:36,511
I really admired his ideas and what he did, wrote, etc.

81
00:06:36,511 --> 00:06:42,840
I also met, in rather deep isolation at that time in Amsterdam,

82
00:06:42,840 --> 00:06:46,286
a group of German artists, called Spur,

83
00:06:46,286 --> 00:06:48,434
who have more or less dropped out of the picture since then,

84
00:06:48,434 --> 00:06:52,652
but who in fact wanted the same thing I wanted.

85
00:06:52,652 --> 00:06:55,211
I came in contact with them, talked to them and

86
00:06:55,211 --> 00:06:56,747
learned that they were part of an international group

87
00:06:56,747 --> 00:07:00,485
that was called Internationale Situationiste

88
00:07:00,485 --> 00:07:03,600
and was in fact based in Paris.

89
00:07:03,600 --> 00:07:05,410
And I asked them whether there were any Dutch people involved

90
00:07:05,410 --> 00:07:09,826
and I was told that Constant, Armando and Har Oudejans

91
00:07:09,826 --> 00:07:12,127
were the Dutch people in the group.

92
00:07:12,127 --> 00:07:16,251
And so I got in touch with them and talked a lot but,

93
00:07:16,251 --> 00:07:20,540
six months after I had got in touch with them,

94
00:07:20,540 --> 00:07:23,491
they were thrown out of the group. Because Har Oudejans had built a church

95
00:07:23,491 --> 00:07:29,951
which the other two agreed with and which did not fit in the dogma of the Situationists.

96
00:07:29,951 --> 00:07:37,185
No, building a church does not fit in a total upheaval of society of course.

97
00:07:37,185 --> 00:07:45,238
So Situationism was a movement advocating the total upheaval of society.

98
00:07:45,238 --> 00:07:50,322
What were their basic thoughts, their foundations?

99
00:07:50,322 --> 00:07:53,997
Well, I have my vision on that.

100
00:07:53,997 --> 00:08:02,158
The foundations that I saw were a creative playful society,

101
00:08:02,158 --> 00:08:05,532
which is now a bromide as a term.

102
00:08:05,532 --> 00:08:10,625
At the time it was really a concept.

103
00:08:10,625 --> 00:08:15,911
A society where every instant, every situation and every place

104
00:08:15,911 --> 00:08:19,848
can constantly be changed.

105
00:08:19,848 --> 00:08:24,749
As a result of which mankind acquires the freedom to act

106
00:08:24,749 --> 00:08:27,307
that lies entirely within his own power and

107
00:08:27,307 --> 00:08:31,100
he can express all his powers.

108
00:08:31,100 --> 00:08:33,530
Every human being therefore.

109
00:08:33,530 --> 00:08:39,000
And who can use in society all his creativity, which is originally in him,

110
00:08:39,000 --> 00:08:42,288
instead of in a specific isolation, of a field, or who knows what.

111
00:08:42,288 --> 00:08:47,641
How did the Situationists want to achieve that?

112
00:08:47,641 --> 00:08:51,323
Yes, I ask myself that too.

113
00:08:51,323 --> 00:08:57,975
It is terribly difficult, because they wrote and wrote,

114
00:08:57,975 --> 00:09:01,345
and they made films.

115
00:09:01,345 --> 00:09:10,204
But theorizing doesn’t signify,

116
00:09:10,204 --> 00:09:14,381
so that is one of the main reproaches you can make against them,

117
00:09:14,381 --> 00:09:09,930
or against us, that we theorized an awful lot,

118
00:09:09,930 --> 00:09:14,553
and in practice Situationism was only practised by themselves,

119
00:09:14,553 --> 00:09:17,904
if you can already call it that.

120
00:09:17,904 --> 00:09:20,924
By making ‘dérives’.

121
00:09:20,924 --> 00:09:28,812
So ‘dérives’ were walks from one place to an unknown place

122
00:09:28,812 --> 00:09:30,914
and writing down these walks.

123
00:09:30,914 --> 00:09:33,596
But of course that won’t change society.

124
00:09:33,596 --> 00:09:37,725
So I do find that a reproach, but it is also terribly difficult,

125
00:09:37,725 --> 00:09:39,999
and especially in those days it was even more difficult

126
00:09:39,999 --> 00:09:41,397
because the climate was lacking.

127
00:09:41,397 --> 00:09:45,873
What lines of thought or political orientations underlie Situationism?

128
00:09:45,873 --> 00:09:49,681
It didn’t just fall out of the sky, it came from somewhere.

129
00:09:49,681 --> 00:09:53,940
It didn’t just fall out of the sky.

130
00:09:53,940 --> 00:10:00,855
It is of course heavily influenced by Marxism, Leninism, Maoism.

131
00:10:00,855 --> 00:10:03,324
That is at bottom the foundation.

132
00:10:03,324 --> 00:10:08,182
For instance, the division, with the Comité Central and that kind of stuff.

133
00:10:08,182 --> 00:10:11,944
Clearly party-oriented initially. I mean, in thoughts.

134
00:10:11,944 --> 00:10:19,744
But just as the Surréalistes Révolutionaires were already against the party,

135
00:10:19,744 --> 00:10:25,694
because the party had become too indoctrinated

136
00:10:25,694 --> 00:10:30,530
with the whole civil servant system which came with it and so on.

137
00:10:30,530 --> 00:10:33,611
A technocratic system.

138
00:10:33,611 --> 00:10:37,875
Trying to find something new which would indeed free humanity from all problems.

139
00:10:37,875 --> 00:10:41,100
In fact a bit like what is happening in Cuba and in China,

140
00:10:41,100 --> 00:10:57,521
but in any case that is my vision of it.

141
00:10:57,521 --> 00:11:07,194
Well I believe that I was Provo my whole life, and now of course Kabouter,

142
00:11:07,194 --> 00:11:13,416
although I don’t agree with everything, but it is of course unique that this happened.

143
00:11:13,416 --> 00:11:17,265
Now I have to tell you honestly, in terms of the group it is of course the young

144
00:11:17,265 --> 00:11:21,776
which I don’t even belong to any more. But age doesn’t count.

145
00:11:21,776 --> 00:11:24,823
But the whole youth has become so incredibly good.

146
00:11:24,823 --> 00:11:28,556
While we were in fact crazy, except for a couple.

147
00:11:28,556 --> 00:11:33,720
And it is now the whole, I really find it just general.

148
00:11:33,720 --> 00:11:40,105
It’s OK to talk to people from age 10, which in the past just wasn’t possible.

149
00:11:40,105 --> 00:11:45,655
They are bursting with ideas and want things.

150
00:11:45,655 --> 00:11:51,604
In fact they don’t want specific things, but simply to express their own freedom.

151
00:11:51,604 --> 00:11:52,332
That is of course fantastic.

152
00:11:52,332 --> 00:12:00,279
I find it fascinating that a little ball can do things in a specific space

153
00:12:00,279 --> 00:12:07,705
that you yourself do not control, or only partly control.

154
00:12:07,705 --> 00:12:08,890
And that you yourself can change it again.

155
00:12:08,890 --> 00:12:10,946
That you stand opposite a machine.

156
00:12:10,946 --> 00:12:14,316
I don’t at all like gambling machines or such.

157
00:12:14,316 --> 00:12:21,461
But a machine you’re engaging with, that thing does something itself.

158
00:12:21,461 --> 00:12:27,112
So fantastic.

159
00:12:27,112 --> 00:12:31,842
Jacqueline, I want to talk about your own paintings now.

160
00:12:31,842 --> 00:12:35,147
We haven’t talked about them yet. This one here is very recent.

161
00:12:35,147 --> 00:12:37,881
But first let’s move the books and so, you know.

162
00:12:37,881 --> 00:12:42,995
What is dominant there? Sex, aggression, sport.

163
00:12:42,995 --> 00:12:47,368
What is it in fact?

164
00:12:47,368 --> 00:12:53,622
What for me is dominant is that everything is gathering

165
00:12:53,622 --> 00:12:56,206
at a point of concentration which is not there.

166
00:12:56,206 --> 00:13:04,669
Thanks to a general movement.

167
00:13:04,669 --> 00:13:09,220
But it is moving, at least I hope so.

168
00:13:09,220 --> 00:13:16,886
Why there is sport in it? In the first place I must say that I hate sport.

169
00:13:16,886 --> 00:13:21,731
Mind if I light a cigarette?

170
00:13:21,731 --> 00:13:31,130
In terms of movement and of the visual aspect,

171
00:13:31,130 --> 00:13:38,652
a human body, with an object, it’s generally that type of thing,

172
00:13:38,652 --> 00:13:45,427
I find it so beautiful.

173
00:13:45,427 --> 00:13:55,481
These are Vietcong fighters.

174
00:13:55,481 --> 00:14:03,364
So I don’t see all that much of a difference between sport and militarism.

175
00:14:03,364 --> 00:14:06,775
And I hate militarism and it fascinates me too of course.

176
00:14:06,775 --> 00:14:14,286
I find war films and such fantastic. Which is logic of course.

177
00:14:14,286 --> 00:14:20,508
But I can imagine that a sportsman dreams of a liberation army,

178
00:14:20,508 --> 00:14:26,362
or appallingly mean marines from Den Helder.

179
00:14:26,362 --> 00:14:33,154
But it could also be a sportsman … This is an ice-hockey player …

180
00:14:33,154 --> 00:14:34,985
Society sports ….

181
00:14:34,985 --> 00:14:41,515
So you go from skating to sex, which plays a big role.

182
00:14:41,515 --> 00:14:44,882
Yes, sorry, but sport and sex go together.

183
00:14:44,882 --> 00:14:46,690
Can you explain that more?

184
00:14:46,690 --> 00:14:51,778
How do you see that relation between sex and aggressiveness?

185
00:14:51,778 --> 00:14:57,149
Well, I can tell you exactly. You mean this lady?

186
00:14:57,149 --> 00:15:01,724
Well, this is an image of an ad for Pepsi Cola.

187
00:15:01,724 --> 00:15:07,367
Where she holds a bottle in one hand and a mic in the other.

188
00:15:07,367 --> 00:15:14,923
So the message is: you can just as well sing as drink Pepsi.

189
00:15:14,923 --> 00:15:22,548
And I find that holding a microphone in your hand is therefore the same as this.

190
00:15:22,548 --> 00:15:30,998
A female longing for a sexual object.

191
00:15:30,998 --> 00:15:35,829
And a bottle too: why do I lift a bottle to my mouth, hm?

192
00:15:35,829 --> 00:15:41,997
Or female … human … animal or whatever.

193
00:15:41,997 --> 00:15:47,888
And that’s why that’s it. What’s more, I find it the most beautiful object there is.

194
00:15:47,888 --> 00:15:54,750
I don’t know as a woman. I can well imagine that men also find it the to be the nicest object.

195
00:15:54,750 --> 00:15:54,468
I find it great how you manage to make such a thing? It’s unbelievable.

196
00:15:54,468 --> 00:16:03,692
On a whole body, there are a whole bunch of wonderful mechanical things.

197
00:16:03,692 --> 00:16:07,127
Eyes, tongues, you name it.

198
00:16:07,127 --> 00:16:11,880
But I find this so beautiful. Hence, very topological.

199
00:16:11,880 --> 00:16:18,755
I mean: the volume changes. The thing remains the same,

200
00:16:18,755 --> 00:16:28,433
but adopts other positions, other situations and also receives another expression.

201
00:16:28,433 --> 00:16:32,370
That’s quite fantastic. Which is also the case with breasts and such,

202
00:16:32,370 --> 00:16:54,483
but not to that extent.

203
00:16:54,483 --> 00:16:57,610
There are 40,000 visual artists living in Paris.

204
00:16:57,610 --> 00:17:00,457
Most in poor primitive popular areas.

205
00:17:00,457 --> 00:17:05,164
Your ‘quartier’ is behind the Bastille, an area of small craftsmen,

206
00:17:05,164 --> 00:17:08,696
furniture-makers. Like every area in Paris, a village.

207
00:17:08,696 --> 00:17:14,224
With small village shops, and with village cafés which still sell coal and wood.

208
00:17:14,224 --> 00:17:19,580
Like yours with its regulars and with pinball machines.

209
00:17:19,580 --> 00:17:21,120
This is where, in a small apartment,

210
00:17:21,120 --> 00:17:27,611
as the only editor, publisher and layout designer, you made the Situationist Times,

211
00:17:27,611 --> 00:17:36,000
which has become legendary. You paid the expenses with your own paintings.

212
00:17:36,000 --> 00:17:49,714
Ha ha ha, what did you put in that cigarette?

213
00:17:49,714 --> 00:18:09,756
I think I’ll have a sip after all. You’re looking at me so sweetly.

214
00:18:09,756 --> 00:18:16,279
Some examples of what you find typically situationist, except this.

215
00:18:16,279 --> 00:18:29,882
Examples. Well, this. This seems to me like a serious example.

216
00:18:29,882 --> 00:18:34,146
Wandering through the city. Things that happen.

217
00:18:34,146 --> 00:18:38,979
That you accept, which you act on.

218
00:18:38,979 --> 00:18:43,503
As a result of which your entire route changes, plus your thoughts,

219
00:18:43,503 --> 00:18:47,443
plus everything, plus the people or the things you come in contact with.

220
00:18:47,443 --> 00:18:54,176
Change again. Through other things again. Assimilation of things.

221
00:18:54,176 --> 00:19:00,451
That is very situationist, I find.

222
00:19:00,451 --> 00:19:04,992
And your life here in Paris, for instance your life here in Café La Palette,

223
00:19:04,992 --> 00:19:09,264
and your local cafés, does that also have something to do with it?

224
00:19:09,264 --> 00:19:16,198
Well, I do indeed believe that you can say that, if through these thoughts,

225
00:19:16,198 --> 00:19:22,950
which you are also displaying through all these remarks of yours, influence of thoughts, by the way you act –

226
00:19:22,950 --> 00:19:28,310
that everything you do has to do with it.

227
00:19:28,310 --> 00:19:34,533
And I find that good. You can pass it on to others.

228
00:19:34,533 --> 00:19:42,330
By acting that way. So in fact rather without known consequences.

229
00:19:42,330 --> 00:19:45,633
But it is not only action because then everyone would be a Situationist.

230
00:19:45,633 --> 00:19:47,902
In a certain sense that is the origin …

231
00:19:47,902 --> 00:19:48,662
In fact that’s the point, yes.

232
00:19:48,662 --> 00:19:54,305
… but it remains a specific view on action as such, on everything.

233
00:19:54,305 --> 00:19:58,894
Well, it is in fact simply everything that happens.

234
00:19:58,894 --> 00:20:03,594
Using, turning around, letting others …

235
00:20:03,594 --> 00:20:08,115
I mean … those with whom it then happens.

236
00:20:08,115 --> 00:20:12,814
And by letting the thing being used then changing again, so that whole environment.

237
00:20:12,814 --> 00:20:18,400
One thing leads to another.

238
00:20:18,400 --> 00:20:26,779
I believe that I feel drawn to the Situationist idea because I had that need.

239
00:20:26,779 --> 00:20:33,421
But I believe that there are a lot of people who have never heard it

240
00:20:33,421 --> 00:20:40,154
who act and think and live 100 per cent in a Situationist manner.

241
00:20:40,154 --> 00:20:43,456
Does this have to do with the fact that you travel a lot,

242
00:20:43,456 --> 00:20:46,789
with the fact that there is nowhere where you are in fact completely at home?

243
00:20:46,789 --> 00:20:55,824
Yes, certainly. Indeed the Gypsies, or nomads, are the best or most brilliant Situationists which you can imagine.

244
00:20:55,824 --> 00:21:03,669
It did not just fall out of the sky, but well-known ethnically in fact.

245
00:21:03,669 --> 00:21:07,954
So as a Jewish woman you are closer to the Gypsies than to Israel, I understand.

246
00:21:07,954 --> 00:21:13,399
Yes, certainly. If we’re going to talk about that.

247
00:21:13,399 --> 00:21:18,577
That is very painful to a lot of people, but I don’t feel at all concerned by Israel.

248
00:21:18,577 --> 00:21:31,507
It’s very annoying. If you want to hear my political thoughts on the subject, I don’t know.

249
00:21:31,507 --> 00:21:35,778
But I find it terribly dangerous what is happening there.

250
00:21:35,778 --> 00:21:40,512
I believe that a people have a right to life. No matter what people.

251
00:21:40,512 --> 00:21:44,169
But then they shouldn’t mess it up themselves.

252
00:21:44,169 --> 00:21:48,398
And I find that really ghastly. A kind of suicide action.

253
00:21:48,398 --> 00:21:53,151
By living militaristically. It may be out of self-defence,

254
00:21:53,151 --> 00:21:59,815
but the defence can become so appallingly big that it becomes suicide.

255
00:21:59,815 --> 00:22:03,953
And your relation to men is in fact also Situationist?

256
00:22:03,953 --> 00:22:09,331
I don’t know whether you can call it Situationist.

257
00:22:09,331 --> 00:22:25,166
No, I believe that it is simply a rather natural need that many women have.

258
00:22:25,166 --> 00:22:27,150
What need?

259
00:22:27,150 --> 00:22:27,474
To have a man.

260
00:22:27,474 --> 00:22:32,796
I don’t mean that, but the way in which you take up a position with regard to that theme.

261
00:22:32,796 --> 00:22:39,269
Yes, certainly. With a life partner it has to be so that the other …

262
00:22:39,269 --> 00:22:44,254
life partner is also such a terribly difficult word …

263
00:22:44,254 --> 00:22:51,765
an unacceptable term in fact since what is life ... Situationist, of course.

264
00:22:51,765 --> 00:23:00,542
But in any case, it has to be someone who lives and can live the same way.

265
00:23:00,542 --> 00:23:04,623
And also who can accept and live with the whims, the changes, the bizarreness

266
00:23:04,623 --> 00:23:09,226
and the chaos that must be created if you wish to live a Situationist life.

267
00:23:09,226 --> 00:23:17,531
And does so himself. Otherwise it is of course impossible for me.

268
00:23:17,531 --> 00:23:19,461
So that has consequences for you.

269
00:23:19,461 --> 00:23:24,540
Yes, of course. You always pay for what you do.

270
00:23:24,540 --> 00:23:32,801
What is the price you pay then?

271
00:23:32,801 --> 00:23:37,222
Well, among others by never having a fixed point where I live, at least so far I’ve never had one.

272
00:23:37,222 --> 00:23:50,900
Where we are now is temporary. Everything is always temporary with me.

273
00:23:50,900 --> 00:23:57,162
By, for instance, not being able to have had children so far.

274
00:23:57,162 --> 00:24:02,303
I don’t think that you can impose it on adolescent people

275
00:24:02,303 --> 00:24:09,590
who haven’t yet chosen to lead this sort of life.

276
00:24:09,590 --> 00:24:15,911
So it was really a conscious decision until now. That I have none.

277
00:24:15,911 --> 00:24:27,572
Although I do of course have the need. That might still change.

278
00:24:27,572 --> 00:24:31,168
What did those eyes see in May 1968?

279
00:24:31,168 --> 00:24:32,345
The power of the people.

280
00:24:32,345 --> 00:24:34,573
The imagination in power.

281
00:24:34,573 --> 00:24:38,593
The artists anonymously at the service of the revolution.

282
00:24:38,593 --> 00:24:42,484
Yourself anonymously among them with posters.

283
00:24:42,484 --> 00:24:47,771
No distinction between art and science, between students and workers.

284
00:24:47,771 --> 00:24:50,707
When your Situationism became reality.

285
00:24:50,707 --> 00:24:54,365
And your eyes saw that it didn’t last long.

286
00:24:54,365 --> 00:25:04,638
And wasn’t real.

287
00:25:04,638 --> 00:25:10,568
All the products that were in the group didn’t count as Situationist products.

288
00:25:10,568 --> 00:25:18,514
Because that is a contradiction in terms. That’s impossible.

289
00:25:18,514 --> 00:25:23,874
You can’t consider a painting a Situationist painting.

290
00:25:23,874 --> 00:25:27,240


291
00:25:27,240 --> 00:25:34,200
Or a book or whatever. Name any product.  Because it is a definite product, while such a chair is naturally an object that is transformable.

292
00:25:34,200 --> 00:25:38,992
You can of course say that images and objects can be made that are transformable

293
00:25:38,992 --> 00:25:48,564
but that is then a product of the Situationist or situlogic way of thinking.

294
00:25:48,564 --> 00:25:54,570
Topology is of course simply one of the aspects of Situationism.

295
00:25:54,570 --> 00:25:58,913
Which, besides, was only carried out by Jorn and me.

296
00:25:58,913 --> 00:26:03,422
While the rest were totally uninterested in topology.

297
00:26:03,422 --> 00:26:07,443
And didn’t see it as so serious either, certainly not Debord.

298
00:26:07,443 --> 00:26:10,281
Unfortunately enough.

299
00:26:10,281 --> 00:26:16,772
But for the Situationists the artist was simply a random person.

300
00:26:16,772 --> 00:26:23,231
Rightly so, I find. For what is an artist?

301
00:26:23,231 --> 00:26:31,495
Who is an artist? That is not exactly, I believe at least, a unique position.

302
00:26:31,495 --> 00:26:38,195
At least, it shouldn’t be. It is simply that you make a craft product and that’s the end of it.

303
00:26:38,195 --> 00:26:46,187
You have certain possibilities to do things differently than another as a result of which it becomes an individual product but that’s all.

304
00:26:46,187 --> 00:26:50,930
You have now published seven issues of the Situationist Times ....

305
00:26:50,930 --> 00:26:55,152
Six, I’m working on the seventh.

306
00:26:55,152 --> 00:26:59,985
And you always extracted specific motifs from

307
00:26:59,985 --> 00:27:01,879
the whole of history of humanity, from all cultures and ages.

308
00:27:01,879 --> 00:27:08,999
Could you give me a couple of examples of what you find typically situationist?

309
00:27:08,999 --> 00:27:13,839
This is the little room where I came to sit when my bother was born.

310
00:27:13,839 --> 00:27:21,186
And that was indeed my school room. That means that I spent my secondary years here.

311
00:27:21,186 --> 00:27:29,673
For 90 per cent because I never left the room because it felt really nice being alone in the little room.

312
00:27:29,673 --> 00:27:57,783
A kind of ivory tower.

313
00:27:57,783 --> 00:28:04,722
We went with you to your parents’ home and that was a farewell 11 years ago.

314
00:28:04,722 --> 00:28:07,840
What do you think about what we did at the time?

315
00:28:07,840 --> 00:28:13,207
Well that was a wonderful day. It was simply wonderful.

316
00:28:13,207 --> 00:28:21,123
For me it was also a sort of return. At last the confrontation with my father,

317
00:28:21,123 --> 00:28:31,469
and I found that whole conversation moving. While our relation is not exactly moving but simply very real.

318
00:28:31,469 --> 00:28:39,215
For years on end I saw them a week or so per year. The last year a bit more because I was more in the Netherlands.

319
00:28:39,215 --> 00:28:49,367
The connection has been rather intellectual these past years.

320
00:28:49,367 --> 00:28:56,428
That whole house, which I find really nice, at least nice,

321
00:28:56,428 --> 00:29:01,692
I find it really nice there, which they really don’t understand in fact.

322
00:29:01,692 --> 00:29:06,485
And also saying, you never used to find that. How did you suddenly arrive at that?

323
00:29:06,485 --> 00:29:12,928
Why are you never there? So that was one thing, but that whole distance that I have with regard to it.

324
00:29:12,928 --> 00:29:18,604
I just left at age 17 or 18. Suddenly on that one day, everything came back.

325
00:29:18,604 --> 00:29:23,686
And that is also a conclusion, I find. I had a terribly good day with you at the time.

326
00:29:23,686 --> 00:29:33,686
And it is a conclusion. A bit spooky, but that’s what it really is.

Transcription: Peter Westenberg Translation: Patrick Lennon

