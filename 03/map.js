function init_map (max_zoom, abovepages, layoutpages) {

var cell_width = 320,
    cell_height = 209;

if (abovepages === undefined) { abovepages = [] }
 
var map = L.map('map', {
        maxZoom: max_zoom || 9,
        minZoom: 0,
        zoom: 0,
        crs: L.CRS.Simple,
        center: new L.LatLng(0,0)
});


var markers = [
  {
    "x": 1.508,
    "y": -1.672,
    "id": "t3",
    "zoom": 7
  },
  {
    "x": 7.523,
    "y": -1.836,
    "id": "t5",
    "zoom": 7
  },
  {
    "x": 1.625,
    "y": -1.805,
    "id": "t6",
    "zoom": 7
  },
  {
    "x": 12.508,
    "y": -1.82,
    "id": "t8",
    "zoom": 7
  },
  {
    "x": 17.563,
    "y": -1.773,
    "id": "t9",
    "zoom": 7
  },
  {
    "x": 22.555,
    "y": -1.789,
    "id": "t10",
    "zoom": 7
  },
  {
    "x": 23.563,
    "y": -1.742,
    "id": "t11",
    "zoom": 7
  },
  {
    "x": 251.734,
    "y": -2.078,
    "id": "t14",
    "zoom": 6
  },
  {
    "x": 248.375,
    "y": -1.742,
    "id": "t15",
    "zoom": 7
  },
  {
    "x": 1.508,
    "y": -1.773,
    "id": "t27",
    "zoom": 7
  },
  {
    "x": 6.586,
    "y": -1.805,
    "id": "t30",
    "zoom": 7
  },
  {
    "x": 12.617,
    "y": -1.875,
    "id": "t31",
    "zoom": 7
  },
  {
    "x": 17.656,
    "y": -1.891,
    "id": "t32",
    "zoom": 7
  },
  {
    "x": 22.688,
    "y": -1.961,
    "id": "t33",
    "zoom": 7
  },
  {
    "x": 23.57,
    "y": -1.898,
    "id": "t34",
    "zoom": 7
  },
  {
    "x": 27.484,
    "y": -1.898,
    "id": "t35",
    "zoom": 7
  },
  {
    "x": 8.133,
    "y": -1.813,
    "id": "t37",
    "zoom": 7
  },
  {
    "x": 27.602,
    "y": -1.805,
    "id": "t38",
    "zoom": 7
  },
  {
    "x": 31.453,
    "y": -1.695,
    "id": "t39",
    "zoom": 7
  },
  {
    "x": 33.547,
    "y": -1.75,
    "id": "t40",
    "zoom": 7
  },
  {
    "x": 37.508,
    "y": -1.773,
    "id": "t41",
    "zoom": 7
  },
  {
    "x": 35.844,
    "y": -2.34,
    "id": "t42",
    "zoom": 8
  },
  {
    "x": 42.609,
    "y": -1.828,
    "id": "t43",
    "zoom": 7
  },
  {
    "x": 47.563,
    "y": -1.766,
    "id": "t44",
    "zoom": 7
  },
  {
    "x": 52.563,
    "y": -1.781,
    "id": "t45",
    "zoom": 7
  },
  {
    "x": 57.625,
    "y": -1.789,
    "id": "t49",
    "zoom": 7
  },
  {
    "x": 62.555,
    "y": -1.766,
    "id": "t50",
    "zoom": 7
  },
  {
    "x": 67.516,
    "y": -1.742,
    "id": "t51",
    "zoom": 7
  },
  {
    "x": 72.563,
    "y": -1.805,
    "id": "t52",
    "zoom": 7
  },
  {
    "x": 77.586,
    "y": -1.828,
    "id": "t53",
    "zoom": 7
  },
  {
    "x": 82.539,
    "y": -1.805,
    "id": "t54",
    "zoom": 7
  },
  {
    "x": 87.586,
    "y": -1.75,
    "id": "t56",
    "zoom": 7
  },
  {
    "x": 92.563,
    "y": -1.766,
    "id": "t57",
    "zoom": 7
  },
  {
    "x": 97.609,
    "y": -1.766,
    "id": "t58",
    "zoom": 7
  },
  {
    "x": 102.531,
    "y": -1.766,
    "id": "t59",
    "zoom": 7
  },
  {
    "x": 107.586,
    "y": -1.766,
    "id": "t60",
    "zoom": 7
  },
  {
    "x": 112.453,
    "y": -1.813,
    "id": "t61",
    "zoom": 7
  },
  {
    "x": 117.57,
    "y": -1.883,
    "id": "t62",
    "zoom": 7
  },
  {
    "x": 122.531,
    "y": -1.82,
    "id": "t63",
    "zoom": 7
  },
  {
    "x": 127.641,
    "y": -1.867,
    "id": "t65",
    "zoom": 7
  },
  {
    "x": 132.578,
    "y": -1.781,
    "id": "t66",
    "zoom": 7
  },
  {
    "x": 137.555,
    "y": -1.758,
    "id": "t67",
    "zoom": 7
  },
  {
    "x": 142.539,
    "y": -1.766,
    "id": "t68",
    "zoom": 7
  },
  {
    "x": 147.547,
    "y": -1.813,
    "id": "t69",
    "zoom": 7
  },
  {
    "x": 152.633,
    "y": -1.852,
    "id": "t70",
    "zoom": 7
  },
  {
    "x": 157.531,
    "y": -1.758,
    "id": "t71",
    "zoom": 7
  },
  {
    "x": 162.438,
    "y": -1.75,
    "id": "t72",
    "zoom": 7
  },
  {
    "x": 167.578,
    "y": -1.781,
    "id": "t73",
    "zoom": 7
  },
  {
    "x": 172.617,
    "y": -1.805,
    "id": "t74",
    "zoom": 7
  },
  {
    "x": 177.508,
    "y": -1.797,
    "id": "t76",
    "zoom": 7
  },
  {
    "x": 182.523,
    "y": -1.766,
    "id": "t77",
    "zoom": 7
  },
  {
    "x": 187.523,
    "y": -1.773,
    "id": "t78",
    "zoom": 7
  },
  {
    "x": 192.609,
    "y": -1.836,
    "id": "t80",
    "zoom": 7
  },
  {
    "x": 197.477,
    "y": -1.805,
    "id": "t81",
    "zoom": 7
  },
  {
    "x": 202.609,
    "y": -1.82,
    "id": "t82",
    "zoom": 7
  },
  {
    "x": 207.43,
    "y": -1.852,
    "id": "t83",
    "zoom": 7
  },
  {
    "x": 217.625,
    "y": -1.695,
    "id": "t84",
    "zoom": 7
  },
  {
    "x": 232.5,
    "y": -1.656,
    "id": "t85",
    "zoom": 7
  },
  {
    "x": 237.609,
    "y": -1.719,
    "id": "t88",
    "zoom": 7
  },
  {
    "x": 238.617,
    "y": -1.656,
    "id": "t90",
    "zoom": 7
  },
  {
    "x": 242.531,
    "y": -1.758,
    "id": "t92",
    "zoom": 7
  },
  {
    "x": 247.547,
    "y": -1.766,
    "id": "t93",
    "zoom": 7
  },
  {
    "x": 211.758,
    "y": -1.797,
    "id": "t100",
    "zoom": 7
  },
  {
    "x": 213.586,
    "y": -1.789,
    "id": "t101",
    "zoom": 7
  },
  {
    "x": 213.6973,
    "y": -2.2168,
    "id": "t102",
    "zoom": 9
  }
];
// console.log("[map].markers", markers);
var allmarkers = [];
markers.forEach(function (m) {
    var marker = L.marker([m.y, m.x]).on("click", function (e) {
        //console.log("marker click", m.id, e, this)
        window.parent.postMessage({msg: "mapclick", id: m.id}, "*")
    });
    allmarkers.push(marker);
})
var markers_layer = L.layerGroup(allmarkers);

var request = new XMLHttpRequest();
request.open('GET', 'tiles.json', true);

var hash = new L.Hash(map);

function is_above_page (item) {
    var m = /^_DSC(\d+)\.png/.exec(item.name);
    if (m) {
        var pn = parseInt(m[1]),
            pni = abovepages.indexOf(pn);
        // console.log("AP", pn, pni);
        return (abovepages.indexOf(pn) != -1)
    }
}

request.onload = function() {
  if (request.status >= 200 && request.status < 400) {
    // Success!
    var data = JSON.parse(request.responseText);
    // console.log("data", data);
    var pelts = [];
    var above_spread = false;
    var x = 0,
        y = 0,
        items = data['@graph'];
    for (var i=0, l=items.length; i<l; i++) {
        var item = items[i],
            m = /^_DSC(\d+)\.png/.exec(item.name),
            itemname = m ? m[1] : '';

        if (layoutpages && layoutpages[itemname]) {
            x = layoutpages[itemname].x;
            y = layoutpages[itemname].y;
            // console.log("using layoutpages, placing", itemname, "at", x, y);
            pelts.push({x: x, y: y, item: items[i]});
            continue
        }
        if (is_above_page(items[i])) {
            // above_spread = true;
            y = 0;
            x -= 1;
        } else {
            y = 1;
        }
        // pelts.push({x: i, y: 0, item: items[i]});
        // console.log("placing", items[i].name, "at", x, y);
        pelts.push({x: x, y: y, item: items[i]});
        x += 1;
        // if (i == 1) break;
    }
    var vt = leafygal.layout(pelts, undefined, undefined, undefined, undefined, undefined, cell_width, cell_height),
        layer = leafygal.gridlayer(L, vt, {tileSize: L.point(cell_width, cell_height)});
    map.addLayer(layer);

    map.addLayer(markers_layer);
    L.control.layers(null, { "Show links": markers_layer }).addTo(map);


  } else {
    console.log("server ERROR");
  }
};
request.onerror = function() {
  console.log("connection ERROR");
};
request.send();


}
