# The Situationist Times #3

[0039#t=00:04:41.580]
I said I would like to make an issue in London, with my friends like Peter Blake and Hockney etc., with the people I know of my age in London and have it in English. But how should I explain that to Noël Arnaud? It gets far off from the Parisian surrounding. He was never travelling, as far as I know. 

[0039#t=00:05:23.289]
Then I said if we get each time all his friends in it it becomes a pataphysical magazine instead of my magazine. So I went to Arnaud and I said to him, "Thank you so much for having helped me so much, we had a wonderful number made together. I think I am ready to do it on my own. Would you accept and resign?" And he said, "Of course, just go to England and let's see what comes out of it if you do it on your own." 

[0039#t=00:06:08.023]
[st03#7/-1.672/1.508]
So then I went to London and thought I am going to use my connections. And that is why the #3 is called **The British Edition**. I said to Jorn, "I want to do much more about topology." And in the meantime, I don't know when this one came out. Does it say anywhere? January 1963, my god I was speedy. 

[0039#t=00:06:44.756]
In the meantime I was sort of studying topology. Books Asger gave me or which we bought. And I went to two or three lectures on topology. And I said that what I would like to do is getting the most basic topology in the British issue. To have this base on which we can go on. So I was looking up all these books and then I started. 

[0039#t=00:07:22.465]
[st03#7/-1.836/7.523]

Then I was in London and met with **Anton Ehrenzweig** and I met with **Jasia Reichard**, yes I went to the ICA, the Institute of Comtemporary Art, which I knew from before, when I was staying in London. Then I asked Ehrenzweig and **George Hay**, who I have no idea who it was, I don't remember anything, to be collaborators. And started to have a big contribution on topological issues again (from Bucaille) and a text that he made, "The Dog's Curve", which was a really mathematical text. And I also asked people like **Alechinsky and Haese**, having nothing to do with the British issue, but just to have some of my friends in it. And I started interlacing and had these two Middle Age bulls interlacing with their... A sort of bull's fight.

[0039#t=00:08:55.293]
[st03#7/-1.805/1.625]

This, of course, which today is very well known, is the topological side of psychology. Here they are weaving, and of course weaving is interlacing. 

[0039#t=00:09:18.030]
**cut**

[0039#t=00:09:21.385]
[st03#7/-1.820/12.508]
I had this Ehrenzweig essay. I think I met Ehrenzweig through Jasia Reichard, well through my small network of people at the ICA.

[0039#t=00:09:40.802]
[st03#7/-1.773/17.563]
And then of course it started with the photos of Franceschi. I have no idea why this is used as the first really big photo.
**cut**

[0039#t=00:09:54.846]
[st03#7/-1.789/22.555]

And then with Ann Hagen we get again art which is awful, it shouldn't be in here, but is. But that was my own fault. Did you read this? It is completely ridiculous. But it is to explain at least why it is in it. And she got into it because she had all these connections. She was married to a fantastic man, a very funny man, Bode Hagen. He had written about WW2, he was Jewish and German, living in Norway. She was from a very rich Norwegian family. He made a lot of books about WW2, and films. And she was also at Tate. So this is the same story as in #2, this nepotism I did myself too. **cut**

[0039#t=00:11:33.811]
[st03#7/-1.742/23.563]

*But then it starts with Bucaille, right? **The Pattern of Situological Applications**. **cut**

[0039#t=00:11:39.292]
**cut** (speaks about Bucaille, but doesn't recognize as such, or is this funny to keep?) 
JDJ: Yeah, of course I had to find a reason to have this ridiculous... So I made up a text to give myself a reason why it is in it. No?
EP: No, I think this is the title page for the entire "Pattern of Situological Applications". This is definitely Bucaille.
JDJ: This is not Bucaille.
EP: It is, I am positive.
JDJ: 'All reproduction permitted', I put there. Is it Bucaille?
EP: Yes, look at the index.
JDJ: Okay, sorry! I am happy you know my magazine...
But this is my hand writing, that's for sure. And that's probably him. And then I put 'All reproduction is permitted.' A good thing.

[0039#t=00:13:02.462]
**cut**
And this, what is this?
EP: This is Ann Hagen. 
JDJ: And this, 'Text by Anton Ehrenzweig'. Which text? That text. And what's this? Do we tell? Here we get Mutant again! 
EP: It is the same text. 
JDJ: Apparently I am insisting. But then we see the same thing as the front. And this is made by me, obviously. 

[0039#t=00:14:11.011]
[st03#6/-2.078/251.734]
Then this, interlacing is in Dutch 'vlechtwerk', and 'vlecht' is when the hair is done like that... And here is the famous... Which I wonder was it also used by the Internationale Situationniste like this, or did I make the "deformation, derivation, transformation"?  
*I think you modified it.*  
And SPUR never used it, I think. They didn't have an anti... And that's interesting. This, until today, made life much easier, because everything is permitted. I mean it also led to the terrible facsimile, fucksimile, fakesimile -- whatever you call it.

[0039#t=00:15:18.879]
[st03#7/-1.742/248.375]
But this (Mutant) I find very strange, putting it in again. I probably thought it was very important. **cut** 

[0039#t=00:15:32.753]

*Why did you insist on the anti-copyright stance?*  
I am sure it was somewhere in the Internationale Situationniste, that there was this anti-copyright.  
*Yes.*
Where is it? I thought it was absolutely essential, that you don't have to have...  
*"Tous les textes publiés dans 'Internationale Situationniste" (peuvent être librement reproduits, traduits ou adaptés même sans indication d'origine)..."*  
That I didn't write, with mentioning the original. Why didn't I put that? Because I didn't think about it. I thought it was absolutely necessary that it would be open to everyone to use it and to participate or to change or do whatever. 

[0039#t=00:16:37.384]
**cut**
And then I start, after reading the books on topology... That I can show you, they are here. 
**looking for books in shelves CUT**

[0039#t=00:20:08.335]
Do you see any topology?  
*Yes, Lietzmann.*  
That's the one I was looking for.  
*And Patterson as well. A basic primer.*  
It was Lietzmann I was looking for. What's this? Someone from Göttingen. It is Mr. Lietzmann. 
*You wrote to him?*  
That's very interesting. So we found... Mr. Lietzmann, from Göttingen.  
*What does it say?*  
(Reading letter in German). Oh you don't speak German? That's very practical.

[0039#t=00:21:43.909]
Mrs. Lietzmann is writing me in 1964: Unluckily I have to tell you... Thank you very much for your letter from Ascona. July 1964, Göttingen. Your letter to my husband, dr. professor Walther Lietzmann I got, and I have to tell, unhappily, that my husband has died and he is no longer on our earth. (I don't know where he is though.)

[0039#t=00:22:34.659]
Your interest and your way of writing he would have very much liked and loved when he was still alive. And he would have been very happy about your interest in his mathematical knowledge, his big love until he passed away.

[0039#t=00:23:15.655]
He was also able to help a lot of people with their human problems and to explain in a very clear form about topology. I myself am not a mathematician, but I know that your name, De Jong, is very famous. Herr professor dr. Richter aus Marburg Universitat, Fakultat Marburg /Lahm  could be someone who could help you instead of my husband.

(Why does she think about my name? Everyone is called de Jong.)

[0039#t=00:24:30.670]
Herr Riechert could be interested to work with your magazine. (But it is strange, because it is from 1964.) Perhaps it's an idea to write to him. He was in America for two semesters and had all sorts of talks there. Try to get in touch with him. I wish you good luck with your magazine and thank you very much for writing to me, Käthe Lietzmann.

Of course it doesn't say anything, but it's funny. It's a document.

[0039#t=00:26:04.111]

Oh yeah, that's the charming thing I do and always did. Someone said about Jorn, "Never give him a book, because you get back half a book."  
*Labyrinths.*  

[0039#t=00:26:41.563]
I took literally everything from him (Lietzmann). Labyrinths is just one aspect of *The Situationist Times.* It is perhaps the most charming, but not the most interesting. I always say that this issue (#3) is the most interesting of all the issues of the Situationist Times. 

[0039#t=00:27:06.526]
(looking at drawing in margin) Now, I don't know if this is me or Jorn, I think it's me. These are very elementary. The knots are the most interesting, because they are the most complex. And he calls it Linientopologie. 

[0039#t=00:27:45.273]
OUTBURST! **cut**


[0001#t=00:02:52.936]
[st03#7/-1.773/1.508]
Well it's very simple... It's the psychology/topology part, which I used in *The Situationist Times*. And I put it in front, and I put this... interlaced weaving... These are images from the Val Camonica. Weaving in the Bronze age, stone drawings. And this is the eternal thing, that was used in Drakabygget, which I re-used and which is not Hoppe's curve. 

[0001#t=00:03:37.666]
As this is the first issue I am making alone, without Noël Arnaud, I started getting more deeply into the topological part. That is what is important with number 3, it is the explanation of topology.

[0001#t=00:03:59.926]
I'm not going to repeat.... **cut**

[0001#t=00:04:14.543]
[st03#7/-1.805/6.586]
Here we have, "the number 3, printed in 1000 examples January, 1963'. So the third issue is ten months after the first issue. Then we have errata here. This is from Reinhoud d'Haese. I don't know why it's there.

[0001#t=00:04:54.590]
[st03#7/-1.875/12.617]
This is a text by **Ehrenzweig** about... Probably about topology, no ... About colors... **"Meditations on the Future of Art".**

[0001#t=00:05:19.626]
[st03#7/-1.891/17.656]
Oh, and it's also about **Comparative Vandalism.** This is one of the photos **Franceschi** made of the graffiti in Normandie, and I probably put it there as an illustration.  
*Yes, there's a reference to it in the text.*

[0001#t=00:05:44.099]
[st03#7/-1.961/22.688]
Then we have **Ann Hagen**, who is a friend of mine. This is absolutely nonsense in a way. But it is about trolls. She was a Norwegian artist who was also at Atelier 17 in Paris and became a friend. And I just put her things in. And I made this, talking about her trolls, they are sort of an illustration of Ehrenzweig's text. How I met Ehrenzweig I don't remember if it was through her or through Jasia Reichard at the ICA. As it is the British edition it has very much to do with my friends in London and Great Britain. 

[0001#t=00:06:39.850]
[st03#7/-1.898/23.570]

This is by **Bucaille**, and here I put **"all reproduction permitted",** which is also on the backside in the more explainable form, 'derivation, modification, transformation of *The Situationist Times* is permitted'. The non-copyright sentence of the *Internationale Situationniste* which I took over. This is Bucaille's **"Pattern of Situological Applications"**. This I think is written by me and not by him, but I am not sure. 

[0001#t=00:07:27.249]
[st03#7/-1.898/27.484]
Then we have the first explanation/study of the Hoppe knot, which you can say is the most elementary image of non-Euclidian mathematics which you find everywhere in patterns all over the world, repeated like it was by *Drakabygget*. Triskele. In Scandinavia it's very much used, the Hoppe curve. The study is about the Hoppe curve, the knot, and the Moebius ribbon. 

[0001#t=00:08:22.644]
And here is the algrebraic or mathematic, abstract way of how the Hoppe curve works, which I of course took from these two books. And here you can see that the book has been completely cut into pieces and used in here, and later on also in the Labyrinth issue. The normal thing was to have two books, one that was kept ok, and one that was used to make the magazine or other purposes, like Jorn did. This one by Patterson, with lots of handwriting by Jorn, in Danish. The same book is in the Museum in Silkeborg, apparently, and that is because we bought one to use and one to...   

[0001#t=00:10:27.171]
[st03#7/-1.813/8.133]

So this one starts with the index, which is quite usual: "Meditations by Ehrenzweig", "A Pattern of Situological Applications" which you just saw, Introduction by Max Bucaille, "A Study in the Morphology of Orange Peels", by Alechinsky. He was peeling oranges and making drawings of oranges and I thought that somehow you could call it curves which are topological. He was doing it together with the Belgian sculptor Reinhoud d'Haese. So I put that in, just to have some contemporary art in this issue. An then there is an enigmatic story by George Hay, and I don't remember at all who he was and how I got to him, so that's an enigma to me too. The editor's note is the non-copyright sentence. Apparently I found that very important. There you have the errata, and then we go on.

[0001#t=00:12:13.059]
[st03#7/-1.805/27.602]

Here it is Hoppe and Moebius and the knot and these explanations I took out of the books.

[0001#t=00:12:22.482]
[st03#7/-1.695/31.453]

Here we have a text by Bucaille.

[0001#t=00:12:31.555]
[st03#7/-1.750/33.547]
Here I start making, these are all my drawings, which I took out of the books. In typewriting I am explaining what they are. This is very basic, the most elementary of topology. There's the Moebius, there's the triskele or the Hoppe, there are applications of Hoppe, the three rings, which of course is not the same as this. These are entrelacs, this are patterns that are very much used in all sorts of cultures and all sorts of ages. 

[0001#t=00:13:20.530]
[st03#7/-1.773/37.508]
And there I start illustrating how the elements were used. And here I copied several of them by hand. How they are used, what they are used for. So you find all sorts of explanations from churches and from objects, mainly in England here, but also in Scandinavia. Brecknockshire, I don't know where it is. St. Fillan... I took mostly English ones. They are all over the world, but it was nice, as this is the British edition, to have English ones.

[0001#t=00:14:05.720]
[st03#8/-2.340/35.844]
And there you have the "relative positions of two regions". So there it starts the geographical and psychological use of topology. And here I try to explain a little bit how the shapes are made and the rings are made. But all of this of course is very basic.

[0001#t=00:14:36.646]
[st03#7/-1.828/42.609]
And then it goes on. And all these are hand-drawn copies from images I found. And of course there are copies which are photographed. But all these badly drawn things are the ones I just copied by hand. 

[0001#t=00:15:06.954]
[st03#7/-1.766/47.563]

I can go on, but it will be the same with other issues. I am not going to explain each image. All I can do is to tell that you get images from the Amazon, a runestone in Sweden and a snake by Walasse Ting. So it is this mixture of different aspects of the snake, ring, whatever, which is the topological thing that makes the magazine. And then of course it is my interpretation, without any explanation. And here you have the "topology of the person", which is again from the psychological topology. And here you just have these different forms of snakes.

[0001#t=00:16:08.256]
[st03#7/-1.781/52.563]
And the snakes are, of course, topological. You have the Indian ones, the Nepalese ones. And here you get again the psychology, it is all the time interfering in different ways. And it is very badly printed, so sometimes it is very difficult to see.
*It is still Rotaprint?*  
Yes, this one is still Rotaprint.

[0001#t=00:16:36.188]
**cut**

[0002#t=00:00:00.000]
The whole idea of the Situationist Times is **not** to explain. And what you want me to do is to explain, which is in contradiction with the whole magazine... I can go on image for image telling what they are, but people have to look themselves. The whole concept is so against the idea of *The Situationist Times*, because explanation is not what I ever wanted with it. It is just showing the things and then people themselves make their combinations.

[0002#t=00:00:49.600]
**cut**
So I don't really understand what we are doing.  
*We can go through them without explaining I think.*  
But then go through and do what? I mean people can read what is written here. And they can see, so what am I doing?  
*You are explaining how you produced it...*  
Explaining! You are saying "explaining" ... Ellef! Do you really want me to explain a page like this?  
*You are kind of explaining it by saying that you don't want to explain.*

[0002#t=00:01:38.614]
[st03#7/-1.789/57.625]

Well, it goes from one thing over to the next, and that's the way I made it. There is no explanation **why** I made it. I just put the things together with the the things I put them together with. And don't forget that it's a hell of a long time ago that I did it. But the pattern is that all the time the topological psychology parts are coming into it. While these are from all over the world. It's the same here. That's the way you walk through a maze, a little bit comparable to the situationist exhibition in the Stedelijk that never took place, you could say. But also it is the association with this. And this Japanese snake dance, if you could see it, you would see that it has the same movement. And these of course are things again that I copied. I am showing simply associative images. And then also the topological ones. 

[0002#t=00:02:59.444]
[st03#7/-1.766/62.555]
*Did you lay them out in a maquette in Paris? How did you create these collages?*  
Of course. I made collages. What is left of them is at Yale. The whole stuff is cut out of books or copied by photo. This is a Danish dance. Well, you have to find out how it works. And then you get over to the curve and the *vlechtwerk*, the interlace. But all the time it's interlace. I mean the dances are interlaced. The interlace is in the strings that are put all the way through. And almost on every page there is a little... having to do with geography or psychology of people. There's all the time one little part interfering. That's just for fun, and here I say "dérives". Whatever it is... These are knots of boy scouts.   

[0002#t=00:04:34.535]
[st03#7/-1.742/67.516]

It goes on and on and on. Of course you have again the images from the Fromm book.  
*Kurt Lewin is his name.*  
Yes. And here arethe woven ones. But again I have to say, it's nothing but my interpretation that counts.

[0002#t=00:05:05.555]
[st03#7/-1.805/72.563]
It's just what I thought was working well together on a page, and explaining the enormous richness of the weaving and *vlechtwerk* interlacing. This is a Jorn. This is I think Japanese or Lebanese, I don't remember.  
*Did you work in libraries, going systematically through books?*

[0002#t=00:05:42.997]
[st03#7/-1.828/77.586]
Yes. I didn't yet work much in the Bibliotheque National, but I got permission for number 4, through Jaffé, but I'll tell that we get to number 4. But I collected books and then cut them into pieces. And things like that I just copied from the books and wrote with my handwriting the different stages. I definitely got most of it simply from books which I sampled and got together. But then of course I had to type all the text and number all the texts, once the maquettes were made. That was the most work, in a way. And to make it nicely, in my way.  

[0002#t=00:06:47.549]
[st03#7/-1.805/82.539]

And there we get the association of photos which I had seen in the collection of Jorn. Actually when we were making photos I of course saw, already before the photo itself existed, that it might go very well together with one of the things, the aspects I was going to use.

[0002#t=00:07:13.986]
And this I found very important. (Looks for Undercover book /potato writing **cut**

[0002#t=00:11:45.824]
[st03#7/-1.750/87.586]
So here we have the Boy Scouts, and here I start mixing it together with photos of the 10,000 Years of Vandalism.

[0002#t=00:11:57.316]
[st03#7/-1.766/92.563]
Then we get the more abstract mathematical patterns, which are of course not always very topological like these are. And some labyrinths.

[0002#t=00:12:16.520]
[st03#7/-1.766/97.609]
It is evident that labyrinths come in, because it is about topology and form. 

[0002#t=00:12:24.250]
[st03#7/-1.766/102.531]
It is all associative images which I could find.

[0002#t=00:12:34.550]
[st03#7/-1.766/107.586]
And sometimes, again, I draw them, because I can't get a photo of them.

[0002#t=00:12:44.634]
[st03#7/-1.813/112.453]

[0002#t=00:12:49.512]
[st03#7/-1.883/117.570]

But in a way, it is completely personal...

[0002#t=00:12:52.052]
[st03#7/-1.820/122.531]

... what I put together. And sometimes you have to find what in it that has to do with that, and so on. And here you have explained...  
*The topology of the prison.* 

[0002#t=00:13:11.550]
**cut** Any more questions?

[0002#t=00:13:15.188]
[st03#7/-1.867/127.641]

*Did you ask people to send you images as well?* 
How could I? To make a photo and then ask if they had anything similar? Well, people around me, perhaps, but not that I remember. Then this, you might ask why is this in it? I mean this is obvious, these are interlaced things, but here it is because these are like weaving, etching is like weaving, by making crosses.

[0002#t=00:13:53.263]
[st03#7/-1.781/132.578]
And here we get more to the line...  These are interlaced drawings, that's very much language and topology in my context. And sometimes it says Klee and I can't see where the Klee is. "Abstract writing", where is that?  
*It's this one, I guess.*  
Oh, yeah. This is sort of Dada, *cadavre exquis* without being exquis. 

[0002#t=00:14:39.841]
[st03#7/-1.758/137.555]

This is a big Japanese fighting scheme, calligraphic, which is very beautiful. That I got from Alechinsky, so you are right, I did ask people.

[0002#t=00:14:53.579]
[st03#7/-1.766/142.539]
And sometimes they came up with things I didn't like.   
This is Dotrement writing which I thought was similar to the fighters. And there again you have..."inaccessability of a goal". And these are similar, topologically, there's no difference between these movements. Then I think there's another attempt for the same movement. This is a little bit like the football game which is being played now in England.  
*Triolectic football.*  
Exactly. Here I think the association is obvious.

[0002#t=00:15:52.871]
[st03#7/-1.813/147.547]
Then we go on with Emmerich, who made these cupolas, which of course were made by Louis Kahn and also by other architects, like le Corbusier, and here I get into the three dimensional cupola-like architecture.  

[0002#t=00:16:24.122]
[st03#7/-1.852/152.633]

And because we were in England, through Jasia Reichard I met **Stefan Themerson and his wife Alicia**. They had just made an opera, this opera, and they gave me the manuscript to publish it for the first time. And I thought it was funny with her drawings. They were not at that moment very well known, but later they were, also in Holland. And then I took this Danish music image which again is an entrelac.

[0002#t=00:17:22.012]
[st03#7/-1.758/157.531]
This is one of my favorites. I found this as a poster, I have no idea who he is. And I knew this photo from Asger, from the Franceschi photos. And then I thought it is absolutely magnificent to have them together. It is amazing. I found this in London. 

[0002#t=00:17:47.276]
[st03#7/-1.750/162.438]
And there you go again with all the more complicated entrelacs and weavings.

[0002#t=00:17:58.951]
[st03#7/-1.781/167.578]
That's all the ornaments in the spiral/entrelac...

[0002#t=00:18:09.168]
[st03#7/-1.805/172.617]
There's not much to tell, there's much to see. But the association which is made is very personal.

[0002#t=00:18:19.225]
Here I think I stop a bit to put in the psychological part into it.

[0002#t=00:18:33.227]
[st03#7/-1.797/177.508]
There we have hair, which of course is very entrelacs. Here it comes again, here we get these images again from the patterns (Lewin). Then of course spaghetti is very much ... or pasta in this case ...

[0002#t=00:18:50.971]
[st03#7/-1.766/182.523]
This is also one of the big things I found. That is the covered train, a war train. And I had the luck that the images were the same size. I put it together with a Pollock. But that, again, is just my interpretation of images that I see. So in a way it is a very egocentric way of making a magazine. And then I have this Arp, which I associate with these images, which are again topology.

But this blinded German train with Pollock is fantastic I think. 

[0002#t=00:19:43.323]
[st03#7/-1.773/187.523]

There it gets more and more abstract, but also very associative, with the scientific images. 

[0002#t=00:19:59.384]
**cut** mini break

[0002#t=00:20:15.000]
[st03#7/-1.836/192.609]

These are the lithos Jorn made for the cover of the catalogue of the Luxury paintings, which were made with threads that went into ink, and he just let them fall, more or less, on the stone (I don't know if it was a plate or a stone). All sorts of coincidences ... like dadaist or surrealist art. But to him they were entrelacs and if you look at them they are. They are all different. They form one image that changes because the threads are real physical threads.   

[0002#t=00:21:14.173]
[st03#7/-1.805/197.477]
These are the peelings. The peelings and the threads went very well together, of course. They were made by **Reinhoud d'Haese and Alechinsky** after peelings they dropped, and then drew. 

[0002#t=00:21:36.419]
[st03#7/-1.820/202.609]

I like d'Haese quite much. He made bread sculptures of which I have one. 

[0002#t=00:21:43.204]
[st03#7/-1.852/207.430]

And there we have, at last, "The Dog's Curve" by Max Bucaille, after all *the* mathematician in the issue, who made a text about the curve of the dog.  

[0002#t=00:21:59.259]
[st03#7/-1.695/217.625]

And this is this enigmatic text which is completely out of space and time. It's not understandable why ... I don't know. I put some images in there.

[0002#t=00:22:30.236]
[st03#7/-1.656/232.500]

Then we have a text about **"Gog and Magog"**. I can read it:

"The Scandinavian review Drakabygget no. 2-3 has published an article which they called Gog and Magog, signed with my name. This is an obvious detournement of an article called 'Luxor or Martyr' which I wrote indeed and sent to them to get published. I would like to make a reflection on another détournement, and well the manifest which declare the Situcratie being sent to us by Drakabygget and which appeared in No. 2 of The Situationist Times (pages 60-62), which was evidently not the real form of our declaration on the Situcratic society. We therefore published it as their ('their' I wrote wrong ('there')) contribution without further connection to The Situationist Times, in a rather illisible way. 

That's it!
[0002#t=00:23:58.964]
"Happy to be at last able to print the changed points. We with all our force sign here the real manifest for a Situcratic Society.
Drakabygget's détournement capacities might be considered as an answer to the poor attack made by Isidor Isou on the lack of this détournement-spirit in the Situationistic derive blood. We are grateful to Drakabygget to add these points." 

[0002#t=00:24:26.679]
And then again, of course:

"As the translation of the definite declaration for the Situcraty did not arrive in time, we will only be able to publish it in the coming number of the Situationist Times." 

So you get rid of a problem in that way, by time. 

[0002#t=00:24:48.796]
[st03#7/-1.719/237.609]

Here comes... "Au de la de..." qui? De Eisenstein. 'Une thèse sóvietique'.
*It's 'Au de la d'Einstein' I think.*  
Yes.  

"Kozyrev crée la mécanique causale." 

[0002#t=00:25:18.264]
Who is Kozyrev?  
*He is the Soviet physicist...*  
Who is against Einstein.  
*Yes, or...*  
I don't remember...  
*Jorn was interested in...*

[0002#t=00:25:38.517]
[st03#7/-1.656/238.617]

There we have Jorn and his triolectic economy.

"The French magazine Planète which published the following lines on the recent discovery of the Russian Nicolai Alexandrovitch Kozyrev. We feel an obligation to remind our readers of the essay written in 1958 by Asger Oluf Jorn, *Pour la Forme*. We only show here one of these remarkable parts where Jorn several years ahead of Kozyrev explains us the neccessary. In Jorn's recent book *Naturens orden* he goes even more deeply into the problem and well considering it not only from a scientific and semantic point of view but too from a deeply studied aesthetical one."    

[0002#t=00:26:39.648]
*This is a page from* Pour la Forme.
That's *Pour la Forme* and this is a page from *Planète*, from this horrible magazine *Planète*. It is a bit confusing. 

[0002#t=00:26:54.297]
[st03#7/-1.758/242.531]

This if find very confusing. Two pages before I have explained "Gog and Magog" and "Luxury or Martyrdom". And then I put in the wrong text with my name, and the right text without my name. That's the *Drakabygget* version and my own version. 

[0002#t=00:27:18.840]
[st03#7/-1.766/247.547]

Then we have again an anti-bomb announcement. Moral armement against nuclear armement, but I turned it around, of course:

"Artistic rearmement against moral rearmement." 

And then again, Mutant.

[0002#t=00:27:48.817]

That's it! **cut**

[0002#t=00:27:54.458]
*In terms of **distribution**, after the magazines had been printed, how would you get them out to the public?*  
The way I explained in number 2, through the same bookshops.  
*And you did all by yourself at this point?* 
Absolutely.  
*You didn't have a professional distributor?*  
I never had up to this point. I was doing it myself, sending them to America, and apparently they arrived, becasue they were sold there. And then they were in Paris and Holland.

[0002#t=00:28:41.028]
*Did you already have a plan for the coming issues when you made #3, that you wanted to do a labyrinth issue, for instance?*  
Of course. There were two things. There was the material I could take from the **SISV**, the **Franceschi** material, the photo material. The sheets were made in Paris so I could look through them to see if there was anything I could use. In this issue I used a lot, and it was very associative.  

[0002#t=00:29:23.219]
*Especially from Spain, there were a lot of images from Spain.* 
Yes, because we had only been to Normandy and Spain by then. There had been no other travels. This is January 1963. I think the trip to Spain was in December 1962. It was in the winter of 1962. That's how I could use the photos.

[0002#t=00:30:14.820]
*Did you have any reactions from the Internationale Situationniste?*  
No, never. From *the* ..? It could only be Debord.  
*Did you have any response from Debord?*  
No, no. I don't know if they saw it, if he saw it. I had absolutely no contact with Kotanyi and Vaneigem.

[0002#t=00:30:46.518]
Why didn't we talk about this?
*That's the list of sources.* **cut**

[0002#t=00:31:03.000]
[st03#7/-1.797/211.758]
I was wondering what the numbers correspond with, and it's this. I made it professionaly, you could almost say. And I say here:

"Photos which are not from one of the books are kindly given to us by the Scandinavian Institute of Comparative Vandalism." 

[0002#t=00:31:28.539]
[st03#7/-1.789/213.586]

Here is the story about Hay. It is a bit funny what I am saying here. I confirm this strange text by Hay, in saying:

"This story, as said by the author **George Hay**, to be made in a topological manner, coincides strangelt enough with a work realized in the 15th century and completely unknown to the author. Among the church chalk paintings in Denmark happen to live the very same images as used by George Hay in his story 'A Short Trip to Chaos'. The deformed creatures and moving smells are happily put together and mixed up on the bare walls of these small white churches."    

This is also my interpretation, of course. It is fiction, you could say.

"How did these images appear on the walls made by a great Renaissance master-and, and how did they change place 500 years later to renew their plastic capacities on the other side of the Channel in the story of te new-Renaissance artist George Hay? As said the author had never seen or never heard of these pro-topological church images in far away Denmark. There is no doubt that until the very moment of this printing of his text, all possibility of any similarity was quite unknown to him. We can only conclude that it might happen to be due to the great faculty of deformation and derivation of the topological plasticities which forces their products to penetrate anywhere at any time in any form. Where and when and how will the next realization be? Therefore any deformation, reproduction, modification, derivation and transformation of the times is permitted." 


[0002#t=00:33:45.660]
[st03#9/-2.2168/213.6973]
And here I put the devil, who does all these deformations. 

[0002#t=00:33:53.570]
**cut**
https://en.wikipedia.org/wiki/Anton_Ehrenzweig
https://en.wikipedia.org/wiki/Jasia_Reichardt


[0038]: ../video/2017-12-14/MVI_0038.web.mp4 "video"
[0039]: ../video/2017-12-14/MVI_0039.web.mp4 "video"
[0040]: ../video/2017-12-14/MVI_0040.web.mp4 "video"
[0041]: ../video/2017-12-14/MVI_0041.web.mp4 "video"


[0001]: ../video/2017-12-15/MVI_0001.web.mp4 "video"
[0002]: ../video/2017-12-15/MVI_0002.web.mp4 "video"
[0003]: ../video/2017-12-15/MVI_0003.web.mp4 "video"
[0004]: ../video/2017-12-15/MVI_0004.web.mp4 "video"
[0005]: ../video/2017-12-15/MVI_0005.web.mp4 "video"
[0006]: ../video/2017-12-15/MVI_0006.web.mp4 "video"
[0007]: ../video/2017-12-15/MVI_0007.web.mp4 "video"
[0008]: ../video/2017-12-15/MVI_0008.web.mp4 "video"
[0009]: ../video/2017-12-15/MVI_0009.web.mp4 "video"
[0010]: ../video/2017-12-15/MVI_0010.web.mp4 "video"

[st03]: ../03/scans.html "scans"

